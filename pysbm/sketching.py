from copy import deepcopy
from pylowstroke.sketch_io import SketchSerializer as skio
from pylowstroke.sketch_core import Stroke
from pylowstroke.sketch_hooks import cut_sketch_hooks
import pylowstroke.sketch_shapely as Shapely 
import os
import numpy as np
from pysbm.clustering import cluster_straight_lines, cluster_curves
from pysbm.intersections import remove_unlikely_intersections, compute_tangential_intersections
import pysbm.fitCurves as fitCurves
import pysbm.bezier as bezier
from skimage.measure import EllipseModel

def load_sketch(file_name):
    sketch = skio.load(file_name, keep_duplicate_points=True)
    for i, s in enumerate(sketch.strokes):
        s.original_id = [i]
        s.original_points = deepcopy(s.points_list)
        s.original_coords = deepcopy(s.points_list)
        # compute original stroke speed
        if s.has_data("time"):
            s.average_speed = s.speed()
        else:
            s.average_speed = 0.0
        if s.has_data("pressure"):
            s.mean_pressure = s.get_mean_data("pressure")
        else:
            s.mean_pressure = 0.5
    sketch.sketch_folder = os.path.dirname(file_name)
    sketch.scale = 1
    return sketch

def sketch_clean(sketch, with_scale=True):

    sketch.trim_strokes()
    sketch.remove_hooks()
    sketch.set_is_curved_parameters("pca_weighted", -1)
    mean_line_length = np.mean([s.length() for s in sketch.strokes])
    sketch.shave_empty_strokes(maxlen=0.01*mean_line_length)
    sketch.update_stroke_indices()
    for i, s in enumerate(sketch.strokes):
        if s.is_curved():
            s.axis_label = 5
            continue
        ## fit straight line
        #a, b = s.resample_ideal_line()
        #sketch.strokes[i].from_array([a, b])
        #sketch.strokes[i].add_avail_data("pressure")
        #sketch.strokes[i].add_avail_data("time")
        #sketch.strokes[i].points_list[0].add_data("pressure", 1.0)
        ##sketch.strokes[i].points_list[0].add_data("p", 1.0)
        #sketch.strokes[i].points_list[0].add_data("time", s.original_points[0].get_data("time"))
        ##sketch.strokes[i].points_list[0].add_data("t", s.original_points[0].get_data("time"))
        #sketch.strokes[i].points_list[1].add_data("pressure", 1.0)
        ##sketch.strokes[i].points_list[1].add_data("p", 1.0)
        #sketch.strokes[i].points_list[1].add_data("time", s.original_points[-1].get_data("time"))
        ##sketch.strokes[i].points_list[1].add_data("t", s.original_points[-1].get_data("time"))
        #sketch.strokes[i].resample_linestring(s.length()/10)
    if with_scale:
        scale_center_sketch(sketch)
    compute_accuracy_radius(sketch)

# this function reimplements the scale-center function from Lifting3D
# should be called after remove_hooks
def scale_center_sketch(sketch, new_dim=512):
    height = sketch.height
    width = sketch.width

    all_points = np.array([p.coords for s in sketch.strokes for p in s.points_list])
    bbox = [np.min(all_points[:, 0]), np.max(all_points[:, 0]), np.min(all_points[:, 1]), np.max(all_points[:, 1])]
    bbox[0] = (max(np.floor(bbox[0]-1), 1.0))
    bbox[1] = (min(np.ceil(bbox[1]+1), height))
    bbox[2] = (max(np.floor(bbox[2]-1), 1.0))
    bbox[3] = (min(np.ceil(bbox[3]+1), width))

    new_max_dim = new_dim - np.ceil((new_dim - 0.8*new_dim)*0.5)*2
    
    max_dim = max([(bbox[1] - bbox[0]+1), (bbox[3] - bbox[2]+1)])
    scale = new_max_dim/max_dim
    img_size = [(1+bbox[1]-bbox[0])*scale, (1+bbox[3]-bbox[2])*scale]
    off_x = np.floor(0.5*(new_dim-img_size[0]))
    off_y = np.floor(0.5*(new_dim-img_size[1]))
    sketch.scaling_information = [scale, bbox, off_x, off_y]
    sketch.scale = scale
    #print("scale", scale)
    #print(off_x, off_y, scale, bbox)
    #exit()
    for s_id, s in enumerate(sketch.strokes):
        for p_id in range(len(s.points_list)):
            s.points_list[p_id].coords[0] = (s.points_list[p_id].coords[0]-bbox[0])*scale + off_x
            s.points_list[p_id].coords[1] = (s.points_list[p_id].coords[1]-bbox[2])*scale + off_y
        s.linestring = Shapely.ShapelyStroke(s.points_list)
        # scale original points
        for p_id in range(len(s.original_points)):
            s.original_points[p_id].coords[0] = (s.original_points[p_id].coords[0]-bbox[0])*scale + off_x
            s.original_points[p_id].coords[1] = (s.original_points[p_id].coords[1]-bbox[2])*scale + off_y

    sketch.height = new_dim
    sketch.width = new_dim
    return sketch

def copy_scaling(sketch, scaling_information):
    for s_id, s in enumerate(sketch.strokes):
        for p_id in range(len(s.points_list)):
            s.points_list[p_id].coords[0] = (s.points_list[p_id].coords[0]-scaling_information[1][0])*scaling_information[0] + scaling_information[2]
            s.points_list[p_id].coords[1] = (s.points_list[p_id].coords[1]-scaling_information[1][2])*scaling_information[0] + scaling_information[3]
        s.linestring = Shapely.ShapelyStroke(s.points_list)

# Call this function after scale_center_sketch
def compute_accuracy_radius(sketch):

    scale = sketch.scale
    MIN_MERGE_THR = 3*scale
    MAX_MERGE_THR = 10*scale

    # Values from the OpenSketch dataset
    min_s = 0.3929
    max_s = 1.7940e+03

    for s_id in range(len(sketch.strokes)):
        if sketch.strokes[s_id].has_data("time"):
            sketch.strokes[s_id].acc_radius = MIN_MERGE_THR + \
                sketch.strokes[s_id].average_speed*(MAX_MERGE_THR - MIN_MERGE_THR)/(max_s - min_s)
        else:
            sketch.strokes[s_id].acc_radius = MIN_MERGE_THR + 0.5*(MAX_MERGE_THR - MIN_MERGE_THR)

# fit beziers to curves
def bezier_fitting(sketch):
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5 or s.is_ellipse():
            continue
        points = np.array([p.coords for p in s.points_list])
        bez = np.array(fitCurves.generate_bezier_without_tangents(points))
        c_points = []
        for c_1_i in range(int(len(bez)/4)):
            for t in np.linspace(0.0, 1.0, num=40):
                c_points.append(bezier.q(bez[4*c_1_i:4*(c_1_i+1)+1], t))
        s.bezier = bez

        new_s = Stroke([])
        new_s.original_points = s.original_points
        new_s.bezier = bez
        new_s.original_id = s.original_id
        new_s.from_array(c_points)
        for pt_id in range(len(c_points)):
            new_s.points_list[pt_id].add_data("pressure", 1.0)
        new_s.id = s.id
        new_s.acc_radius = s.acc_radius
        new_s.axis_label = 5
        new_s.width = s.width
        #new_s.ellipse_fitting = s.get_ellipse_fitting()
        sketch.strokes[s_id] = new_s

def assign_ellipse_minor_axes(sketch, camera):
    for s_id, s in enumerate(sketch.strokes):
        if not (s.axis_label == 5 and s.is_ellipse()):
            continue
        pts = np.array([p.coords for p in s.points_list])

        ell = EllipseModel()
        ell.estimate(pts)

        xc, yc, a, b, theta_orig = ell.params
        theta = np.rad2deg(theta_orig)
        center = np.array([xc, yc])

        minor_axis_id = np.argmin([a, b])

        if minor_axis_id == 1:
            theta += 90.0
        rotated_x = np.array([np.cos(np.deg2rad(theta)), np.sin(np.deg2rad(theta))])
        rotated_x /= np.linalg.norm(rotated_x)
        vanishing_lines = np.array([(camera.vanishing_points_coords[i]-center)/np.linalg.norm((camera.vanishing_points_coords[i]-center))
                                    for i in range(3)])
        angles = np.arccos(np.abs(np.dot(vanishing_lines, rotated_x)))
        s.minor_axis = rotated_x
        min_angle = np.min(np.rad2deg(angles))
        s.closest_major_axis_id = np.argmin(angles)
        s.closest_major_axis_deviation = min_angle

# Here, we filter out bad lines, cluster strokes, 
# and compute the intersection graph
def preprocessing(sketch, cam):

    # filter out strokes
    max_acc_radius = np.max([s.acc_radius for s in sketch.strokes])
    sketch.shave_empty_strokes(2*max_acc_radius)
    sketch.update_stroke_indices()

    # cluster strokes
    sketch.compute_intersection_graph()
    cluster_straight_lines(sketch)
    sketch.update_stroke_indices()
    sketch.compute_intersection_graph()
    cluster_curves(sketch, VERBOSE=False)
    sketch.update_stroke_indices()
    sketch.compute_intersection_graph()

    bezier_fitting(sketch)
    assign_ellipse_minor_axes(sketch, cam)
    sketch.compute_intersection_graph(extended=True)
    compute_tangential_intersections(sketch, VERBOSE=False)
    for inter in sketch.intersection_graph.get_intersections():
        inter.is_extended = False
    remove_unlikely_intersections(sketch)