from pysbm.symmetry_tools.candidate_correspondences_straight import candidate_correspondences_2d_and_straight_3d
from pysbm.symmetry_tools.candidate_correspondences_curves import process_candidate_correspondences_curves, filter_weak_curve_correspondences
from pysbm.symmetry_tools.common_tools import get_per_correspondence_sym_scores
from pysbm.symmetry_tools.compute_batches import get_batch_decomposition 
from pysbm.optimization.plane_sweep import plane_sweep
from pysbm.optimization.common_tools import update_candidate_strokes, get_planes_scale_factors, get_plane_triplet
from pysbm.intersections import get_intersection_arc_parameters, prepare_triple_intersections
from itertools import product
from pysbm.camera import Camera
from pylowstroke.sketch_camera import assignLineDirection
from pysbm.symmetry_tools.common_tools import extract_fixed_strokes
import numpy as np
import os, json

selected_planes = [0, 1, 2]

def init_camera(sketch):
    cam_param, line_groups = sketch.estimate_camera()
    cam = Camera(proj_mat=cam_param["P"],
                 focal_dist=cam_param["f"],
                 fov=cam_param["fov"],
                 t=cam_param["t"].reshape(-1),
                 view_dir=cam_param["view_dir"],
                 principal_point=cam_param["principal_point"].reshape(-1),
                 rot_mat=cam_param["R"],
                 K=cam_param["K"],
                 cam_pos=np.array(cam_param["C"]).reshape(-1),
                 vanishing_points_coords=cam_param["vp_coord"])
    cam.compute_inverse_matrices()
    assignLineDirection(sketch, line_groups)
    return cam

def compute_symmetry_candidates(sketch, cam, include_more_curves=False):
    max_axes = [0, 1, 2, 3, 5]
    global_candidate_correspondences = []

    per_stroke_proxies = [[] for s_id in range(len(sketch.strokes))]
    nb_planes = len(selected_planes)

    per_axis_per_stroke_candidate_reconstructions = [[[] for s_id in range(len(sketch.strokes))]
                                                     for plane_id in range(nb_planes)]
    candidate_correspondences_2d_and_straight_3d(sketch, cam,
                                                 global_candidate_correspondences,
                                                 per_axis_per_stroke_candidate_reconstructions,
                                                 selected_planes, max_axes,
                                                 include_more_curves=include_more_curves)
    curve_epsilons = process_candidate_correspondences_curves(sketch, cam,
                                                              global_candidate_correspondences,
                                                              per_axis_per_stroke_candidate_reconstructions,
                                                              per_stroke_proxies,
                                                              include_more_curves=include_more_curves)
    per_correspondence_sym_scores = get_per_correspondence_sym_scores(
        sketch, curve_epsilons, global_candidate_correspondences)
    filter_weak_curve_correspondences(per_correspondence_sym_scores, global_candidate_correspondences,
                                                     sketch)
    return global_candidate_correspondences, per_correspondence_sym_scores

def compute_batches(sketch, symm_candidates, VERBOSE=False):
    batches = get_batch_decomposition(symm_candidates,
        #[corr for corr in global_candidate_correspondences if corr[4] == main_axis],
        "", sketch=sketch, min_dist=10, VERBOSE=VERBOSE)
    #global_candidate_correspondences, data_folder, sketch=sketch, min_dist=10)
    return batches

def optimize_symmetry_sketch(sketch, cam, symm_candidates, batches,
    per_correspondence_sym_scores, main_axis=-1, fixed_strokes=[], 
    solver="ortools"):
    if len(fixed_strokes) == 0:
        fixed_strokes = [[] for i in range(len(sketch.strokes))]
    fixed_planes_scale_factors = []
    fixed_intersections = []
    fixed_line_coverages = np.zeros(len(sketch.strokes))
    max_scale_factor_modes = 3
    batch_folder = None
    accumulated_obj_value = []
    extreme_intersections_distances_per_stroke, stroke_lengths = get_intersection_arc_parameters(sketch)
    per_stroke_triple_intersections = prepare_triple_intersections(sketch)
    batches_results = []
    # we optimize iteratively, one optimization per batch
    for batch_id, batch in enumerate(batches):
        
        per_axis_per_stroke_candidate_reconstructions = update_candidate_strokes(
            fixed_strokes, symm_candidates, batch, len(sketch.strokes))
        nb_rec = [len(s) for axis_id in range(3) for s in per_axis_per_stroke_candidate_reconstructions[axis_id]]

        planes_scale_factors = get_planes_scale_factors(
            sketch, cam, batch, batch_id, selected_planes, fixed_strokes,
            fixed_planes_scale_factors, per_axis_per_stroke_candidate_reconstructions,
            max_scale_factor_modes, batch_folder)
        if main_axis != -1 and batch_id > 0:
            planes_scale_factors[main_axis] = [planes_scale_factors[main_axis][0]]

        #print("planes_scale_factors")
        #print(planes_scale_factors)

        planes_scale_factors_numbers = [range(len(planes_scale_factor))
                                        for planes_scale_factor in planes_scale_factors]
        nb_planes_comb = len(list(product(*planes_scale_factors_numbers)))

        if batch_id > 0:
            orig_plane_triplet = get_plane_triplet(
                np.array(cam.cam_pos), planes_scale_factors[0][0],
                planes_scale_factors[1][0], planes_scale_factors[2][0])
            already_distant_plane_triplet = False

        # perform a plane-sweep
        planes_combs = list(product(*planes_scale_factors_numbers))
        plane_sweep(sketch, cam, batch_id, batch, symm_candidates, 
            fixed_strokes, fixed_intersections, fixed_planes_scale_factors, 
            fixed_line_coverages, planes_scale_factors, 
            planes_combs, main_axis, per_correspondence_sym_scores,
            extreme_intersections_distances_per_stroke, stroke_lengths,
            per_stroke_triple_intersections, accumulated_obj_value,
            batches_results, solver)
    main_axis_file = os.path.join(sketch.sketch_folder, "main_axis.npy")
    if main_axis != -1 and main_axis_file == "":
        accumulated_obj_value_file_name = os.path.join(sketch.sketch_folder, 
            "main_axis_"+str(main_axis)+"_acc_obj_value")
        np.save(accumulated_obj_value_file_name, np.sum(accumulated_obj_value))

    return batches_results, accumulated_obj_value


def optimize_symmetry_sketch_pipeline(sketch, cam, symm_candidates, batches,
    per_correspondence_sym_scores, solver="ortools", main_axis=-1):
    batches_result_axes = []
    obj_value_axes = []
    if main_axis != -1:
        batches_result, obj_value = optimize_symmetry_sketch(sketch, cam, 
            symm_candidates, batches, per_correspondence_sym_scores, 
            main_axis=main_axis, solver=solver)
    else:
        for tmp_axis in range(2):
            batches_result, obj_value = optimize_symmetry_sketch(sketch, cam, 
                symm_candidates, batches, per_correspondence_sym_scores, 
                main_axis=tmp_axis, solver=solver)
            batches_result_axes.append(batches_result)
            obj_value_axes.append(np.sum(obj_value))
        print(obj_value_axes)
        print("main axis", np.argmax(obj_value_axes))
        batches_result = batches_result_axes[np.argmax(obj_value_axes)]
    
    with open(os.path.join(sketch.sketch_folder, "batches_result.json"), "w") as fp:
        json.dump(batches_result, fp, indent=4)
    # Second pass without main axis
    batches_result_1, obj_value = optimize_symmetry_sketch(sketch, cam, 
        symm_candidates, batches, per_correspondence_sym_scores, 
        fixed_strokes=extract_fixed_strokes(batches_result), solver=solver)
    with open(os.path.join(sketch.sketch_folder, "batches_result_1.json"), "w") as fp:
        json.dump(batches_result_1, fp, indent=4)
    return batches_result, batches_result_1