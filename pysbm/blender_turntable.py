import bpy
import os, inspect
import bpy_extras
from mathutils import Matrix
from mathutils import Vector
import numpy
import json, pickle
#from scipy.spatial.transform import Rotation

#---------------------------------------------------------------
# 3x4 P matrix from Blender camera
#---------------------------------------------------------------

def align_vectors(a, b, weights=None, return_sensitivity=False):

    """Estimate a rotation to optimally align two sets of vectors.
    Find a rotation between frames A and B which best aligns a set of
    vectors `a` and `b` observed in these frames. The following loss
    function is minimized to solve for the rotation matrix
    :math:`C`:
    .. math::
        L(C) = \\frac{1}{2} \\sum_{i = 1}^{n} w_i \\lVert \\mathbf{a}_i -
        C \\mathbf{b}_i \\rVert^2 ,
    where :math:`w_i`'s are the `weights` corresponding to each vector.
    The rotation is estimated with Kabsch algorithm [1]_.
    Parameters
    ----------
    a : array_like, shape (N, 3)
        Vector components observed in initial frame A. Each row of `a`
        denotes a vector.
    b : array_like, shape (N, 3)
        Vector components observed in another frame B. Each row of `b`
        denotes a vector.
    weights : array_like shape (N,), optional
        Weights describing the relative importance of the vector
        observations. If None (default), then all values in `weights` are
        assumed to be 1.
    return_sensitivity : bool, optional
        Whether to return the sensitivity matrix. See Notes for details.
        Default is False.
    Returns
    -------
    estimated_rotation : `Rotation` instance
        Best estimate of the rotation that transforms `b` to `a`.
    rmsd : float
        Root mean square distance (weighted) between the given set of
        vectors after alignment. It is equal to ``sqrt(2 * minimum_loss)``,
        where ``minimum_loss`` is the loss function evaluated for the
        found optimal rotation.
    sensitivity_matrix : ndarray, shape (3, 3)
        Sensitivity matrix of the estimated rotation estimate as explained
        in Notes. Returned only when `return_sensitivity` is True.
    Notes
    -----
    This method can also compute the sensitivity of the estimated rotation
    to small perturbations of the vector measurements. Specifically we
    consider the rotation estimate error as a small rotation vector of
    frame A. The sensitivity matrix is proportional to the covariance of
    this rotation vector assuming that the vectors in `a` was measured with
    errors significantly less than their lengths. To get the true
    covariance matrix, the returned sensitivity matrix must be multiplied
    by harmonic mean [3]_ of variance in each observation. Note that
    `weights` are supposed to be inversely proportional to the observation
    variances to get consistent results. For example, if all vectors are
    measured with the same accuracy of 0.01 (`weights` must be all equal),
    then you should multiple the sensitivity matrix by 0.01**2 to get the
    covariance.
    Refer to [2]_ for more rigorous discussion of the covariance
    estimation.
    References
    ----------
    .. [1] https://en.wikipedia.org/wiki/Kabsch_algorithm
    .. [2] F. Landis Markley,
            "Attitude determination using vector observations: a fast
            optimal matrix algorithm", Journal of Astronautical Sciences,
            Vol. 41, No.2, 1993, pp. 261-280.
    .. [3] https://en.wikipedia.org/wiki/Harmonic_mean
    """
    a = numpy.asarray(a)
    if a.ndim != 2 or a.shape[-1] != 3:
        raise ValueError("Expected in `a` to have shape (N, 3), "
                         "got {}".format(a.shape))
    b = numpy.asarray(b)
    if b.ndim != 2 or b.shape[-1] != 3:
        raise ValueError("Expected input `b` to have shape (N, 3), "
                         "got {}.".format(b.shape))

    if a.shape != b.shape:
        raise ValueError("Expected inputs `a` and `b` to have same shapes"
                         ", got {} and {} respectively.".format(
                            a.shape, b.shape))

    if weights is None:
        weights = numpy.ones(len(b))
    else:
        weights = numpy.asarray(weights)
        if weights.ndim != 1:
            raise ValueError("Expected `weights` to be 1 dimensional, got "
                             "shape {}.".format(weights.shape))
        if weights.shape[0] != b.shape[0]:
            raise ValueError("Expected `weights` to have number of values "
                             "equal to number of input vectors, got "
                             "{} values and {} vectors.".format(
                                weights.shape[0], b.shape[0]))
        if (weights < 0).any():
            raise ValueError("`weights` may not contain negative values")

    B = numpy.einsum('ji,jk->ik', weights[:, None] * a, b)
    u, s, vh = numpy.linalg.svd(B)

    # Correct improper rotation if necessary (as in Kabsch algorithm)
    if numpy.linalg.det(u @ vh) < 0:
        s[-1] = -s[-1]
        u[:, -1] = -u[:, -1]

    C = numpy.dot(u, vh)

    if s[1] + s[2] < 1e-16 * s[0]:
        print("Optimal rotation is not uniquely or poorly defined "
                      "for the given sets of vectors.")

    rmsd = numpy.sqrt(max(
        numpy.sum(weights * numpy.sum(b ** 2 + a ** 2, axis=1)) - 2 * numpy.sum(s),
        0))

    if return_sensitivity:
        zeta = (s[0] + s[1]) * (s[1] + s[2]) * (s[2] + s[0])
        kappa = s[0] * s[1] + s[1] * s[2] + s[2] * s[0]
        with numpy.errstate(divide='ignore', invalid='ignore'):
            sensitivity = numpy.mean(weights) / zeta * (
                    kappa * numpy.eye(3) + numpy.dot(B, B.T))
        return C, rmsd, sensitivity
    else:
        #print(C)
        return C, rmsd

# Build intrinsic camera parameters from Blender camera data
#
# See notes on this in
# blender.stackexchange.com/questions/15102/what-is-blenders-camera-projection-matrix-model
def get_calibration_matrix_K_from_blender(camd):
    f_in_mm = camd.lens
    scene = bpy.context.scene
    resolution_x_in_px = scene.render.resolution_x
    resolution_y_in_px = scene.render.resolution_y
    scale = scene.render.resolution_percentage / 100
    sensor_width_in_mm = camd.sensor_width
    sensor_height_in_mm = camd.sensor_height
    pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
    if (camd.sensor_fit == 'VERTICAL'):
        # the sensor height is fixed (sensor fit is horizontal),
        # the sensor width is effectively changed with the pixel aspect ratio
        s_u = resolution_x_in_px * scale / sensor_width_in_mm / pixel_aspect_ratio
        s_v = resolution_y_in_px * scale / sensor_height_in_mm
    else: # 'HORIZONTAL' and 'AUTO'
        # the sensor width is fixed (sensor fit is horizontal),
        # the sensor height is effectively changed with the pixel aspect ratio
        pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
        s_u = resolution_x_in_px * scale / sensor_width_in_mm
        s_v = resolution_y_in_px * scale * pixel_aspect_ratio / sensor_height_in_mm


    # Parameters of intrinsic calibration matrix K
    alpha_u = f_in_mm * s_u
    alpha_v = f_in_mm * s_v
    u_0 = resolution_x_in_px * scale / 2
    v_0 = resolution_y_in_px * scale / 2
    skew = 0 # only use rectangular pixels

    K = Matrix(
        ((alpha_u, skew,    u_0),
        (    0  , alpha_v, v_0),
        (    0  , 0,        1 )))
    return K

def get_3x4_RT_matrix_from_blender(cam):
    # bcam stands for blender camera
    R_bcam2cv = Matrix(
        ((1, 0,  0),
         (0, -1, 0),
         (0, 0, -1)))

    # Transpose since the rotation is object rotation,
    # and we want coordinate rotation
    # R_world2bcam = cam.rotation_euler.to_matrix().transposed()
    # T_world2bcam = -1*R_world2bcam * location
    #
    # Use matrix_world instead to account for all constraints
    location, rotation = cam.matrix_world.decompose()[0:2]
    R_world2bcam = rotation.to_matrix().transposed()

    # Convert camera location to translation vector used in coordinate changes
    # T_world2bcam = -1*R_world2bcam*cam.location
    # Use location from matrix_world to account for constraints:
    T_world2bcam = -1*R_world2bcam @ location

    # Build the coordinate transform matrix from world to computer vision camera
    # NOTE: Use * instead of @ here for older versions of Blender
    # TODO: detect Blender version
    R_world2cv = R_bcam2cv@R_world2bcam
    T_world2cv = R_bcam2cv@T_world2bcam

    # put into 3x4 matrix
    RT = Matrix((
        R_world2cv[0][:] + (T_world2cv[0],),
        R_world2cv[1][:] + (T_world2cv[1],),
        R_world2cv[2][:] + (T_world2cv[2],)
         ))
    return RT

def get_3x4_P_matrix_from_blender(cam):
    K = get_calibration_matrix_K_from_blender(cam.data)
    RT = get_3x4_RT_matrix_from_blender(cam)
    return K@RT, K, RT

# ----------------------------------------------------------
# Alternate 3D coordinates to 2D pixel coordinate projection code
# adapted from https://blender.stackexchange.com/questions/882/how-to-find-image-coordinates-of-the-rendered-vertex?lq=1
# to have the y axes pointing up and origin at the top-left corner
def project_by_object_utils(cam, point):
    scene = bpy.context.scene
    co_2d = bpy_extras.object_utils.world_to_camera_view(scene, cam, point)
    render_scale = scene.render.resolution_percentage / 100
    render_size = (
            int(scene.render.resolution_x * render_scale),
            int(scene.render.resolution_y * render_scale),
            )
    return Vector((co_2d.x * render_size[0], render_size[1] - co_2d.y * render_size[1]))


def compute_inverse_matrices(r_t, k):

    r_t_inv = numpy.ones(shape=(4, 4))
    r_t_inv[:3, :] = r_t
    r_t_inv[3, :3] = 0.0
    r_t_inv = numpy.linalg.inv(r_t_inv)
    k_inv = numpy.linalg.inv(k)
    return r_t_inv, k_inv

def lift_point(p, lambda_val, k_inv, r_t_inv):
    u, v = p[0], p[1]

    p_cam = numpy.dot(k_inv, numpy.array([[u], [v], [1.0]]))
    p_cam *= lambda_val
    p_cam = numpy.expand_dims(p_cam, 0)

    p_world = numpy.ones(shape=(4, 1))
    p_world[:3] = p_cam
    p_world = numpy.dot(r_t_inv, p_world)
    p_world[:] /= p_world[3]
    p_world = p_world[:3]
    p_world = numpy.transpose(p_world)
    return p_world[0]


def look_at(obj_camera, point):
    loc_camera = obj_camera.matrix_world.to_translation()
    direction = point - loc_camera
    # point the cameras '-Z' and use its 'Y' as up
    rot_quat = direction.to_track_quat('-Z', 'Y')
    # assume we're using euler rotation
    obj_camera.rotation_euler = rot_quat.to_euler()

def project_point(p, proj_mat):
    hom_p = numpy.ones(4)
    hom_p[:3] = p
    proj_p = numpy.dot(proj_mat, hom_p)
    return proj_p[:2] / proj_p[2]

def calibrate_camera(cam_param, folder):
    fov = cam_param["fov"]
    cam = bpy.data.objects['Camera']
    cam.data.sensor_width = 1
    cam.data.sensor_height = 1
    cam.rotation_mode = "XYZ"
    #cam.location = [-0.82328509792654525, -0.58680546433041747, -0.749133031137398]
    cam.location = cam_param["C"]
    cam_pos = numpy.array(cam_param["C"])
    #cam.data.lens = 885.72437566790518
    P, K, RT = get_3x4_P_matrix_from_blender(cam)
    K = numpy.array(cam_param["K"])
    #print("K")
    #print(K)
    #K = numpy.array([[885.72437566790518,0,356.35539412325687],[0,885.72437566790518,-276.38582435039547],[0,0,1]])

    fx, fy = K[0,0], K[1,1]
    cx, cy = K[0,2], K[1,2]
    if fx > fy:
        bpy.context.scene.render.pixel_aspect_y = fx/fy
    elif fx < fy:
        bpy.context.scene.render.pixel_aspect_x = fy/fx
    width = 512 #fov 29.0416d
    height = 512

    cam.data.lens_unit = 'FOV'
    bpy.context.scene.render.resolution_x = width
    bpy.context.scene.render.resolution_y = height
    max_resolution = max(width, height)
    cam.data.angle = 2 * numpy.arctan(width / (2 * K[0, 0]))
    #cam.data.angle = 2 * numpy.arctan(471.034823785905 / (2 * K[0, 0]))
    #print(numpy.rad2deg(cam.data.angle))
    #shift_x: -0.166805, shift_y: -0.946853
    #cam.data.angle = numpy.deg2rad(35.1282)
    #print(numpy.rad2deg(cam.data.angle))
    cam.data.shift_x = -(cx - width / 2.0) / max_resolution
    cam.data.shift_y = (cy - height / 2.0) / max_resolution
    bpy.context.view_layer.update()

    # find correct rotation
    r_t_inv, k_inv = compute_inverse_matrices(RT, K)
    bpy.context.view_layer.update()

    points_2d = numpy.load(os.path.join(folder, "points_2d.npy"))
    lifted_points_3d = numpy.array([lift_point(p, 1.0, k_inv, r_t_inv) for p in points_2d])
    for p_id in range(len(lifted_points_3d)):
        v = lifted_points_3d[p_id] - cam_pos
        v /= numpy.linalg.norm(v)
        #lifted_points_3d[p_id] = cam_pos + v
        lifted_points_3d[p_id] = v
        #lifted_points_3d[p_id] /= numpy.linalg.norm(lifted_points_3d[p_id])
        #lifted_points_3d[p_id] = cam_pos + lifted_points_3d[p_id]/numpy.linalg.norm(lifted_points_3d[p_id])
    numpy.save(os.path.join(folder, "lifted_points_3d.npy"), lifted_points_3d)

    points_3d = numpy.load(os.path.join(folder, "points_3d.npy"))
    rot_mat, _ = align_vectors(points_3d, lifted_points_3d)
    #print(rot_mat)
    rot_mat = Matrix(rot_mat)
    print("found rotation")
    print(rot_mat.to_euler())
    rot_mat_euler = rot_mat.to_euler()
    print(cam.rotation_euler)
    bpy.context.view_layer.update()
    cam.location = (0,0,0)
    bpy.context.view_layer.update()
    cam.rotation_euler.rotate(rot_mat_euler)
    bpy.context.view_layer.update()
    cam.location = cam_param["C"]
    print("new camera rotation")
    print(cam.rotation_euler)
    bpy.context.view_layer.update()
    bpy.context.view_layer.update()

def sketch_mesh_to_gpencil(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='GPENCIL')
    #bpy.context.object.data.pixel_factor = 0.1
    #bpy.ops.object.gpencil_modifier_add(type='GP_OPACITY')
    #bpy.context.object.grease_pencil_modifiers["Opacity"].factor = 0.5

    #ob = bpy.context.view_layer.objects.active

    #bpy.ops.object.select_all(action='DESELECT')
    #ob.select_set(True)
    #bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    #if mat is None:
    #    mat = new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    #ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')

def sketch_mesh_to_tubes(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='CURVE')

    ob = bpy.context.view_layer.objects.active
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    #if mat is None:
    #    mat = new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')


#MATERIAL_FILE_PATH = os.path.join("/Users/fhahnlei/Pictures/symmetric_sketch/materials.blend")
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
MATERIAL_FILE_PATH = os.path.join(current_dir, "blender/materials.blend")

def load_mat(mat_name):
    path = MATERIAL_FILE_PATH + "\\Material\\"
    bpy.ops.wm.append(filename=mat_name, directory=path)
    mat = bpy.data.materials.get(mat_name)
    return mat

def load_sketch_mesh(sketch_name, sketch_obj_file, thickness=0.001, pressure=None, color=None):

    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF', axis_forward="Y", axis_up="Z")
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}"
    # Apply your transform to correct axis, eg:
    #ob.rotation_euler[0] = numpy.deg2rad(90) # Correct axis orientations
    #bpy.ops.object.transform_apply() # Apply all transforms

    mat = load_mat("stroke-black")
    mat = mat.copy()
    tree = mat.node_tree
    print(tree.nodes)
    if not pressure is None:
        tree.nodes["Mix Shader"].inputs[0].default_value = pressure
    if not color is None:
        tree.nodes["Principled BSDF"].inputs[0].default_value = (color[0], color[1], color[2], 1)
    #bpy.data.materials["stroke-black"].node_tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob


def load_plane(sketch_name, sketch_obj_file, axis):

    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF', axis_forward="Y", axis_up="Z")
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}"
    # Apply your transform to correct axis, eg:
    #ob.rotation_euler[0] = numpy.deg2rad(90) # Correct axis orientations
    #bpy.ops.object.transform_apply() # Apply all transforms

    mat = load_mat("plane-"+axis)
    mat.node_tree.nodes["Mix Shader"].inputs[0].default_value = 0.5

    #sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob

def setup_scene(envmap_path=None):

    bpy.context.scene.render.film_transparent = True

    # Environment lighting
    if envmap_path is not None:
        bpy.context.scene.world.use_nodes = True
        node_tex = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeTexEnvironment")
        node_tex.image = bpy.data.images.load(envmap_path)
        node_tree = bpy.context.scene.world.node_tree
        # Tweak saturation
        node_hsv = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeHueSaturation")
        node_hsv.inputs[1].default_value = 0.2 # Set saturation
        node_tree.links.new(node_tex.outputs['Color'], node_hsv.inputs['Color'])
        node_tree.links.new(node_hsv.outputs['Color'], node_tree.nodes['Background'].inputs['Color'])

    bpy.ops.object.select_all(action = 'SELECT')
    bpy.data.objects.get("Camera").select_set(False)
    #bpy.data.objects.get("Camera_2").select_set(False)
    bpy.ops.object.delete()


if __name__ == "__main__":
    import sys
    argv = sys.argv
    argv = argv[argv.index("--") + 1:]  # get all args after "--"
    folder = argv[0]
    blender_folder = os.path.join(folder, "blender")
    with open(os.path.join(blender_folder, "cam_data.json"), "rb") as fp:
        cam_param = json.load(fp)
    #load_sketch_mesh("sketch", os.path.join(blender_folder, "sketch.obj"))
    # load pressure
    if os.path.exists(os.path.join(blender_folder, "pressures.npy")):
        pressures = numpy.load(os.path.join(blender_folder, "pressures.npy"))

    #1)
    #setup_scene(os.path.join("/Applications/Blender.app/Contents/Resources/3.1/datafiles/studiolights/world/interior.exr"))
    env_map = os.path.join(current_dir, "blender/interior.exr")
    setup_scene(env_map)
    calibrate_camera(cam_param, blender_folder)
    calibrate_camera(cam_param, blender_folder)
    calibrate_camera(cam_param, blender_folder)
    calibrate_camera(cam_param, blender_folder)
    calibrate_camera(cam_param, blender_folder)
    #exit()
    bpy.context.scene.render.resolution_x = 1080
    bpy.context.scene.render.resolution_y = 1080

    #2)
    stroke_collection = bpy.data.collections.new("strokes")
    bpy.context.scene.collection.children.link(stroke_collection)
    stroke_ids = []
    for x in os.listdir(blender_folder):
        if not ".obj" in x:
            continue
        if "stroke_" in x:
            stroke_id = int(x.split("_")[1].split(".obj")[0])
            size = 0.001
            stroke_ids.append(stroke_id)
            load_sketch_mesh("stroke_"+str(stroke_id), os.path.join(blender_folder, x),
                             thickness=size*pressures[stroke_id], pressure=pressures[stroke_id])
            stroke_collection.objects.link(bpy.data.objects["stroke_"+str(stroke_id)])
            #try:
            #    bpy.context.scene.collection.objects.unlink(bpy.data.objects["stroke_"+str(stroke_id)])
            #except:
            #    try:
            #        bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["stroke_"+str(stroke_id)])
            #    except:
            #        continue
            #bpy.data.objects["stroke_"+str(stroke_id)].hide_render = True
            #bpy.data.objects["stroke_"+str(stroke_id)].hide_set(True)
        #if "original_stroke_" in x:
        #    stroke_id = int(x.split("_")[-1].split(".obj")[0])
        #    size = 0.001
        #    if sketch_id in [152]:
        #        size = 0.0005
        #    if sketch_id in [1]:
        #        size = 0.0001
        #    load_sketch_mesh("stroke_"+str(stroke_id), os.path.join(blender_folder, x),
        #                     thickness=size*pressures[stroke_id], pressure=pressures[stroke_id])
        #if "non_reconstructed_" in x:
        #    stroke_id = int(x.split("_")[-1].split(".obj")[0])
        #    size = 0.001
        #    if sketch_id in [152]:
        #        size = 0.0005
        #    load_sketch_mesh("non_reconstructed_stroke_"+str(stroke_id), os.path.join(blender_folder, x),
        #                     thickness=size*pressures[stroke_id], pressure=pressures[stroke_id])
        #    non_reconstructed_collection.objects.link(bpy.data.objects["non_reconstructed_stroke_"+str(stroke_id)])
        #    try:
        #        bpy.context.scene.collection.objects.unlink(bpy.data.objects["non_reconstructed_stroke_"+str(stroke_id)])
        #    except:
        #        try:
        #            bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["non_reconstructed_stroke_"+str(stroke_id)])
        #        except:
        #            continue

        #if "_plane_" in x:
        #    plane_id = int(x.split("_")[2].split(".obj")[0])
        #    axis = x.split("_")[1]
        #    load_plane("plane_"+str(plane_id), os.path.join(blender_folder, x), axis)
        #    bpy.data.objects["plane_"+str(plane_id)].hide_render = True
        #    bpy.data.objects["plane_"+str(plane_id)].hide_set(True)
    #render_folder = "/Users/fhahnlei/Pictures/symmetric_sketch/video_presentation/sketch_102_sequence"
    #for stroke_id in numpy.sort(stroke_ids):
    #    tmp_file_name = os.path.join(render_folder, str(numpy.char.zfill(str(stroke_id), 4))+".png")
    #    print(tmp_file_name)
    #    bpy.data.objects["stroke_"+str(stroke_id)].hide_render = False
    #    bpy.data.objects["stroke_"+str(stroke_id)].hide_set(False)
    #    bpy.data.scenes['Scene'].render.filepath = tmp_file_name
    #    bpy.ops.render.render(write_still = True)
#
#        if "diff_ours_" in x:
#            stroke_id = int(x.split("_")[-1].split(".obj")[0])
#            load_sketch_mesh("diff_ours_"+str(stroke_id), os.path.join(blender_folder, x),
#                             thickness=0.001, pressure=None, color=diff_colors[stroke_id])
#            ours_collection.objects.link(bpy.data.objects["diff_ours_"+str(stroke_id)])
#            try:
#                bpy.context.scene.collection.objects.unlink(bpy.data.objects["diff_ours_"+str(stroke_id)])
#            except:
#                try:
#                    bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["diff_ours_"+str(stroke_id)])
#                except:
#                    continue
#
#
#    #3)
#    for x in os.listdir(blender_folder):
#        if not ".obj" in x:
#            continue
#        if "non_reconstructed_" in x:
#            stroke_id = int(x.split("_")[-1].split(".obj")[0])
#            obj = bpy.data.objects["non_reconstructed_stroke_"+str(stroke_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    #for s_id in only_processed_by_ours:
    #    try:
    #        obj = bpy.data.objects["diff_ours_"+str(s_id)]
    #        obj.data.bevel_depth = 0.001
    #        mat = obj.active_material
    #        tree = mat.node_tree
    #        c = diff_colors[s_id]
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
    #        tree.nodes["Principled BSDF"].inputs[0].default_value = (55/256,126/256,184/256, 1)

    #        load_sketch_mesh("missing_stroke_"+str(s_id), os.path.join(blender_folder, "stroke_diff_ours_"+str(s_id)+".obj"),
    #                         thickness=0.001, pressure=None, color=(55/256,126/256,184/256))
    #        missing_collection.objects.link(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        try:
    #            bpy.context.scene.collection.objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        except:
    #            try:
    #                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #            except:
    #                continue
    #    except:
    #        continue
    #for s_id in only_processed_by_wires:
    #    try:
    #        obj = bpy.data.objects["diff_wires_"+str(s_id)]
    #        obj.data.bevel_depth = 0.001
    #        mat = obj.active_material
    #        tree = mat.node_tree
    #        c = diff_colors[s_id]
    #        #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
    #        tree.nodes["Principled BSDF"].inputs[0].default_value = (55/256,126/256,184/256, 1)

    #        load_sketch_mesh("missing_stroke_"+str(s_id), os.path.join(blender_folder, "stroke_diff_wires_"+str(s_id)+".obj"),
    #                         thickness=0.001, pressure=None, color=(55/256,126/256,184/256))
    #        missing_collection.objects.link(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        try:
    #            bpy.context.scene.collection.objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #        except:
    #            try:
    #                bpy.data.collections["Collection"].objects.unlink(bpy.data.objects["missing_stroke_"+str(s_id)])
    #            except:
    #                continue
    #    except:
    #        continue
#    for s_id in range(len(diff_colors)):
#        try:
#            obj = bpy.data.objects["diff_ours_"+str(s_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            c = diff_colors[s_id]
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#            obj = bpy.data.objects["diff_wires_"+str(s_id)]
#            obj.data.bevel_depth = 0.0005
#            mat = obj.active_material
#            tree = mat.node_tree
#            c = diff_colors[s_id]
#            tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#        except:
#            continue
#    for s_id in range(len(pressures)):
#        try:
#            obj = bpy.data.objects["stroke_"+str(s_id)]
#            obj.data.bevel_depth = 0.001*pressures[s_id]
#            #mat = obj.active_material
#            #tree = mat.node_tree
#            #c = diff_colors[s_id]
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#        except:
#            continue
#    for s_id, c in enumerate(per_stroke_batch_color):
#        try:
#            obj = bpy.data.objects["stroke_"+str(s_id)]
#            #obj.data.bevel_depth = 0.001
#            #mat = load_mat("emission")
#            mat = obj.active_material
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Mix Shader"].inputs[0].default_value = 1.0
#            #tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)
#            #tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)
#            #mat = mat.copy()
#            tree = mat.node_tree
#            #tree.nodes["Emission"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            #tree.nodes["Emission"].inputs[0].default_value = (c[0], c[1], c[2], 1)
#            tree.nodes["Emission"].inputs[1].default_value = 2.0
#            obj.active_material = mat
#        except:
#            continue

#    # single image render
#    render_folder = os.path.join(blender_folder, "turntable_small")
#    #render_folder = os.path.join(blender_folder, "turntable")
#    if not os.path.exists(render_folder):
#        os.mkdir(render_folder)
#    tmp_file_name = os.path.join(render_folder, "input_sketch.png")
#    print(tmp_file_name)
#    bpy.data.scenes['Scene'].render.filepath = tmp_file_name
#    bpy.ops.render.render(write_still = True)

    # animation render
    if os.path.exists(os.path.join(blender_folder, "bbox_center.npy")):
        bbox_center = numpy.load(os.path.join(blender_folder, "bbox_center.npy"))
    bpy.context.scene.frame_start = 0
    bpy.context.scene.frame_end = 359

    #bpy.ops.object.empty_add(type='PLAIN_AXES', align='WORLD', location=(0.272931, -0.0301021, -0.161563), scale=(1, 1, 1))
    bpy.ops.object.empty_add(type='PLAIN_AXES', align='WORLD', location=(bbox_center[0], bbox_center[1], bbox_center[2]), scale=(1, 1, 1))
    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=0)
    #bpy.ops.anim.keyframe_insert_menu(type='Rotation')

    bpy.context.object.rotation_euler[2] = 6.28319
    bpy.context.object.keyframe_insert(data_path='rotation_euler', frame=359)
    fcurves = bpy.context.object.animation_data.action.fcurves
    for fcurve in fcurves:
        for kf in fcurve.keyframe_points:
            kf.interpolation = 'LINEAR'
    #bpy.ops.anim.change_frame(360)
    #bpy.ops.anim.keyframe_insert_menu(type='Rotation')
    for obj in stroke_collection.all_objects:
        obj.select_set(True)
    #bpy.ops.outliner.collection_objects_select()
    #stroke_collection.collection_objects_select()
    bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)
    bpy.ops.wm.save_as_mainfile(filepath=os.path.join(folder, "reconstruction.blend"))
#    #bpy.ops.anim.change_frame(90)
#    #bpy.data.scenes['Scene'].camera = camera
#    #render_folder = os.path.join(blender_folder, "turntable_small")
#    # RENDER
#    #render_folder = os.path.join(blender_folder, "turntable_1080")
#    #if not os.path.exists(render_folder):
#    #    os.mkdir(render_folder)
#    #for i in range(360):
#    #    #bpy.data.scenes['Scene'].frame_set(bpy.data.scenes['Scene'].frame_current + 1)
#    #    bpy.data.scenes['Scene'].frame_current = i
#    #    tmp_file_name = os.path.join(render_folder, str(numpy.char.zfill(str(i), 4))+".png")
#    #    print(tmp_file_name)
#    #    bpy.data.scenes['Scene'].render.filepath = tmp_file_name
#    #    bpy.ops.render.render(write_still = True)
#