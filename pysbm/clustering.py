import numpy as np
import networkx as nx
from sklearn import linear_model
from shapely.geometry import LineString
import matplotlib.pyplot as plt

def get_clusters_straight_lines(sketch):
    # build adjacency matrix
    adj_mat = np.zeros([len(sketch.strokes), len(sketch.strokes)],
                       dtype=np.bool)
    adj_mat[:, :] = False
    all_node_strokes = sketch.intersection_graph.get_strokes()
    all_node_strokes_len = len(all_node_strokes)
    for i in range(all_node_strokes_len):
        if sketch.strokes[all_node_strokes[all_node_strokes_len-i-1].stroke_id].axis_label == 5:
            del all_node_strokes[all_node_strokes_len-i-1]
    for node_stroke_id, node_stroke in enumerate(all_node_strokes):
        s_id = node_stroke.stroke_id
        inters = [inter
                  for inter in sketch.intersection_graph.get_intersections(node_stroke.inter_ids)
                  if sketch.strokes[np.array(inter.stroke_ids)[np.array(inter.stroke_ids)!=s_id][0]].axis_label != 5]
        for inter in inters:
            inter_params = inter.inter_params[0]
            other_stroke_id = inter.stroke_ids[0] if inter.stroke_ids[
                                                         0] != s_id else \
            inter.stroke_ids[1]
            if abs(inter_params[1] - inter_params[0]) >= 0.75 and \
                    sketch.strokes[s_id].axis_label == sketch.strokes[other_stroke_id].axis_label and \
                    abs(s_id - other_stroke_id) <= 5 and \
                    sketch.strokes[s_id].get_line_angle(
                        sketch.strokes[other_stroke_id]) <= np.deg2rad(
                5.0):  # 5 degrees
                adj_mat[s_id, other_stroke_id] = True
                adj_mat[other_stroke_id, s_id] = True
    # return connected components
    components = [list(g) for g in
                  nx.connected_components(nx.from_numpy_array(adj_mat))
                  if len(g) > 1]
    return components


# return one straight line per cluster
def get_aggregated_straight_lines(sketch, clusters):
    regr = linear_model.LinearRegression()
    aggregated_lines = []
    for cluster in clusters:
        cluster_points = []
        for c in cluster:
            cluster_points.append(sketch.strokes[c].points_list[0])
            cluster_points.append(sketch.strokes[c].points_list[-1])
        cluster_points_coords = np.array([p.coords for p in cluster_points])
        x_train = cluster_points_coords[:, 0].reshape(-1, 1)
        y_train = cluster_points_coords[:, 1].reshape(-1, 1)
        use_x_train = True
        if abs(np.max(cluster_points_coords[:, 1]) - np.min(
                cluster_points_coords[:, 1])) > \
                abs(np.max(cluster_points_coords[:, 0]) - np.min(
                    cluster_points_coords[:, 0])):
            use_x_train = False
            x_train = cluster_points_coords[:, 1].reshape(-1, 1)
            y_train = cluster_points_coords[:, 0].reshape(-1, 1)
        regr.fit(x_train, y_train)
        predicted_values = np.array(regr.predict(x_train)).flatten()
        aggr_line = np.array(
            [[cluster_points_coords[p_id, 0], predicted_values[p_id]]
             for p_id in range(len(cluster_points_coords))])
        if not use_x_train:
            aggr_line = np.array(
                [[predicted_values[p_id], cluster_points_coords[p_id, 1]]
                 for p_id in range(len(cluster_points_coords))])
        # aggr_line is unsorted. extract extremal points
        x_min = np.min(aggr_line[:, 0])
        x_max = np.max(aggr_line[:, 0])
        y_min = np.min(aggr_line[:, 1])
        y_max = np.max(aggr_line[:, 1])
        min_min = np.array([x_min, y_min])
        min_max = np.array([x_min, y_max])
        max_min = np.array([x_max, y_min])
        max_max = np.array([x_max, y_max])
        dists_min = [np.linalg.norm(p - min_min) for p in aggr_line]
        dists_max = [np.linalg.norm(p - min_max) for p in aggr_line]
        min_id = np.argmin(dists_min)
        max_id = np.argmin(dists_max)
        if dists_min[min_id] < dists_max[max_id]:
            first_point = aggr_line[min_id]
            dists_min = [np.linalg.norm(p - max_max) for p in aggr_line]
            snd_point = aggr_line[np.argmin(dists_min)]
            aggregated_lines.append(np.array([first_point, snd_point]))
        else:
            first_point = aggr_line[max_id]
            dists_min = [np.linalg.norm(p - max_min) for p in aggr_line]
            snd_point = aggr_line[np.argmin(dists_min)]
            aggregated_lines.append(np.array([first_point, snd_point]))
    return aggregated_lines


def replace_stroke_clusters(sketch, clusters, cluster_lines):
    # replace lines in sketch.strokes
    first_cluster_lines = [cluster[0] for cluster in clusters]
    for cluster_id, s_id in enumerate(first_cluster_lines):
        max_acc_radius = np.max([sketch.strokes[c_i].acc_radius
                                 for c_i in clusters[cluster_id]])
        axis_label = sketch.strokes[s_id].axis_label
        stroke_width = sketch.strokes[s_id].width
        sketch.strokes[s_id].from_array(cluster_lines[cluster_id].tolist())
        sketch.strokes[s_id].from_array(cluster_lines[cluster_id].tolist())
        sketch.strokes[s_id].axis_label = axis_label
        sketch.strokes[s_id].acc_radius = max_acc_radius
        sketch.strokes[s_id].width = stroke_width
        for c_i in clusters[cluster_id][1:]:
            sketch.strokes[s_id].original_id += sketch.strokes[c_i].original_id
    remove_strokes = [c for cluster in clusters for c in cluster[1:]]
    #print(sorted(remove_strokes, reverse=True))
    for s_id in sorted(remove_strokes, reverse=True):
    #for s_id in reversed(remove_strokes):
        del sketch.strokes[s_id]

def cluster_straight_lines(sketch):
    clusters = get_clusters_straight_lines(sketch)
    cluster_lines = get_aggregated_straight_lines(sketch, clusters)
    replace_stroke_clusters(sketch, clusters, cluster_lines)

def cluster_curves(sketch, VERBOSE=False):
    absorbed_stroke_ids = []
    for c_1_id, c_1 in enumerate(sketch.strokes):
        #if c_1.axis_label != 5:
        #    continue
        c_1_linestring = LineString([p.coords for p in c_1.points_list])
        c_1_neighbours = [s_id for s_id in sketch.intersection_graph.get_stroke_neighbours(c_1_id)]
        #c_1_neighbours = [s_id for s_id in sketch.intersection_graph.get_stroke_neighbours(c_1_id)
        #                  if sketch.strokes[s_id].axis_label == 5]
        for c_2_id in c_1_neighbours:
            c_2 = sketch.strokes[c_2_id]
            if c_1.axis_label != c_2.axis_label:
                continue
            if c_2_id == c_1_id:
                continue
            c_2_linestring = LineString([p.coords for p in c_2.points_list])
            max_acc_radius = max(c_1.acc_radius, c_2.acc_radius)
            curve_inters = sketch.intersection_graph.get_intersections_by_stroke_ids(c_1_id, c_2_id)
            #print(c_1_id, c_2_id, len(curve_inters))
            line_inter = c_1_linestring.buffer(max_acc_radius).intersection(c_2_linestring)
            #print(line_inter.length)
            c_1_inter_ratio = line_inter.length/c_1_linestring.length
            c_2_inter_ratio = line_inter.length/c_2_linestring.length
            #print(c_1_id, c_2_id, c_1_inter_ratio, c_2_inter_ratio)
            inter_ratios = [c_1_inter_ratio, c_2_inter_ratio]
            stroke_ids = [c_1_id, c_2_id]
            linestrings = [c_1_linestring, c_2_linestring]
            if np.max(inter_ratios) >= 1.0 and \
                    linestrings[np.argmax(inter_ratios)].length < linestrings[np.argmin(inter_ratios)].length:
                absorbed_stroke_ids.append(stroke_ids[np.argmax(inter_ratios)])
                sketch.strokes[stroke_ids[np.argmin(inter_ratios)]].original_id += sketch.strokes[stroke_ids[np.argmax(inter_ratios)]].original_id
                #print(stroke_ids[np.argmax(inter_ratios)])
            #elif np.min(inter_ratios) >= 0.75:
            #    absorbed_stroke_ids.append(np.argmin(inter_ratios))
            #    print(np.argmin(inter_ratios))
    absorbed_stroke_ids = np.unique(absorbed_stroke_ids)
    new_stroke_ids = list(range(len(sketch.strokes)))
    for del_id in sorted(absorbed_stroke_ids, reverse=True):
        del new_stroke_ids[del_id]
        #del sketch.strokes[del_id]

    if VERBOSE:
        fig, axes = plt.subplots(nrows=1, ncols=2)
        fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                            bottom=0.0,
                            top=1.0)
        #sketch.display_strokes_2(fig=fig, ax=axes[0],
        #                         color_process=lambda s: "green" if s.axis_label == 5 else "black",
        #                         linewidth_data=lambda s: 3.0, norm_global=True)
        sketch.display_strokes_2(fig=fig, ax=axes[0],
                                 color_process=lambda s: "green" if s.axis_label == 5 else "black",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=new_stroke_ids)
        sketch.display_strokes_2(fig=fig, ax=axes[0],
                                 color_process=lambda s: "red",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=absorbed_stroke_ids)
        sketch.display_strokes_2(fig=fig, ax=axes[1],
                                 color_process=lambda s: "green" if s.axis_label == 5 else "black",
                                 linewidth_data=lambda s: 3.0, norm_global=True,
                                 display_strokes=new_stroke_ids)
        for ax in axes:
            ax.set_xlim(0, sketch.width)
            ax.set_ylim(sketch.height, 0)
            ax.axis("equal")
            ax.axis("off")
        plt.show()
    #sys.exit()
    for del_id in sorted(absorbed_stroke_ids, reverse=True):
        del sketch.strokes[del_id]
    for s in sketch.strokes:
        s.original_id = np.unique(s.original_id).tolist()