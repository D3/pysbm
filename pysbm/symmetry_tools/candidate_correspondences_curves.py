import pysbm.tools_3d as tools_3d
import pysbm.symmetry_tools.cluster_proxies as cluster_proxies
import numpy as np
from skspatial.objects import Plane
import pysbm.symmetry_tools.common_tools as common_tools
import networkx as nx

def process_candidate_correspondences_curves(sketch, camera, 
                                             global_candidate_correspondences,
                                             per_axis_per_stroke_candidate_reconstructions,
                                             per_stroke_proxies,
                                             PARTIAL_CURVES=True,
                                             SYMMETRIC_CURVE_FILTERING=False,
                                             include_more_curves=False):

    cluster_proxies.cluster_proxy_strokes(global_candidate_correspondences,
                                                        per_stroke_proxies, sketch,
                                                        EXCLUDE_CURVES=True)
    # reconstruct curve geometry based on straight line proxies
    refl_mats = []
    sym_planes = []
    for tmp_i in range(3):
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[tmp_i] = 1.0
        refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
        sym_planes.append(Plane(sym_plane_point, sym_plane_normal))
        refl_mats.append(refl_mat)

    # delete matches for which no 3D curve pair can be found
    del_indices = []
    self_symmetric_ellipses = [[] for s_id in range(len(sketch.strokes))]
    #ps.init()
    if PARTIAL_CURVES:
        per_axis_per_stroke_segments = [[[] for s in range(len(sketch.strokes))] for i in range(3)]
        per_axis_per_stroke_intervals = [[[] for s in range(len(sketch.strokes))] for i in range(3)]
        per_axis_per_stroke_interval_corr_id = [[[] for s in range(len(sketch.strokes))] for i in range(3)]
        per_axis_per_stroke_corr_id_interval_id_dict = {}
        replace_corr_ids = []

    for corr_id, corr in enumerate(global_candidate_correspondences):
        if sketch.strokes[corr[0]].axis_label != 5:
            continue
        focus_vp = corr[4]
        if sketch.strokes[corr[0]].is_ellipse():
            c_1, c_2 = common_tools.reconstruct_symmetric_curves_planar(
                corr[0], corr[1], focus_vp, sketch, camera,
                per_stroke_proxies,
                minimal_foreshortening=False, VERBOSE=False)
        else:
            if PARTIAL_CURVES:
                c_1, c_2, c_1_interval, c_2_interval = common_tools.reconstruct_symmetric_curves_bezier_partial(
                    corr[0], corr[1], focus_vp, per_stroke_proxies, sketch, camera,
                    minimal_foreshortening=False, VERBOSE=False, include_more_curves=include_more_curves)

        if len(c_1) == 0 or len(c_2) == 0:
            #print("first", corr_id)
            del_indices.append(corr_id)
            continue
        if PARTIAL_CURVES and not sketch.strokes[corr[0]].is_ellipse():
            per_axis_per_stroke_segments[corr[4]][corr[0]].append(c_1)
            per_axis_per_stroke_intervals[corr[4]][corr[0]].append(c_1_interval)
            per_axis_per_stroke_corr_id_interval_id_dict[(corr[4], corr[0], corr_id)] = len(per_axis_per_stroke_interval_corr_id[corr[4]][corr[0]])
            per_axis_per_stroke_interval_corr_id[corr[4]][corr[0]].append(corr_id)
            if corr[1] != corr[0]:
                per_axis_per_stroke_segments[corr[4]][corr[1]].append(c_2)
                per_axis_per_stroke_corr_id_interval_id_dict[(corr[4], corr[1], corr_id)] = len(per_axis_per_stroke_interval_corr_id[corr[4]][corr[1]])
                per_axis_per_stroke_intervals[corr[4]][corr[1]].append(c_2_interval)
                per_axis_per_stroke_interval_corr_id[corr[4]][corr[1]].append(corr_id)
            replace_corr_ids.append(corr_id)
        if corr[0] == corr[1] and sketch.strokes[corr[0]].is_ellipse():
            self_symmetric_ellipses[corr[0]].append([c_1, focus_vp, corr_id])
            #del_indices.append(corr_id)
            continue
        global_candidate_correspondences[corr_id][2] = c_1
        global_candidate_correspondences[corr_id][3] = c_2
        per_axis_per_stroke_candidate_reconstructions[focus_vp][corr[0]].append(c_1)
        if corr[1] != corr[0]:
            per_axis_per_stroke_candidate_reconstructions[focus_vp][corr[1]].append(c_2)

    if PARTIAL_CURVES:

        per_axis_per_stroke_per_interval_other_stroke_id = [[[] for j in range(len(sketch.strokes))] for i in range(3)]
        # merge overlapping (in [0,1] and in 3d) intervals and segments
        for axis_id in range(3):
            for s_id, s in enumerate(sketch.strokes):
                if s.axis_label != 5 or s.is_ellipse():
                    continue
                segments = per_axis_per_stroke_segments[axis_id][s_id]
                intervals = per_axis_per_stroke_intervals[axis_id][s_id]
                merged_segments, merged_intervals, merged_intervals_ids = \
                    common_tools.merge_segments_intervals(segments, intervals, s_id, sketch, camera)
                # rename interval_ids in global_correspondences
                other_stroke_ids = [[] for i in range(len(merged_intervals))]
                for interval_id, corr_id in enumerate(per_axis_per_stroke_interval_corr_id[axis_id][s_id]):
                    new_interval_id = -1
                    for merged_interval_id, merged_interval in enumerate(merged_intervals_ids):
                        if interval_id in merged_interval:
                            new_interval_id = merged_interval_id
                            break
                    assert new_interval_id != -1, "new_interval_id not found"
                    assert (axis_id, s_id, corr_id) in per_axis_per_stroke_corr_id_interval_id_dict, "corr_id not saved"
                    per_axis_per_stroke_corr_id_interval_id_dict[(axis_id, s_id, corr_id)] = new_interval_id
                    corr = global_candidate_correspondences[corr_id]
                    other_stroke_id = np.array(corr[:2])[np.array(corr[:2]) != s_id]
                    other_stroke_ids[new_interval_id].append(other_stroke_id)
                per_axis_per_stroke_segments[axis_id][s_id] = merged_segments
                per_axis_per_stroke_intervals[axis_id][s_id] = merged_intervals
                per_axis_per_stroke_per_interval_other_stroke_id[axis_id][s_id] = other_stroke_ids

        per_axis_per_stroke_curves, per_axis_per_stroke_interval_curve_ids, per_axis_per_stroke_rejected_curves = \
            common_tools.stitch_partial_curves(
                per_axis_per_stroke_segments, per_axis_per_stroke_intervals,
                per_axis_per_stroke_per_interval_other_stroke_id, sketch, camera,
                VERBOSE=False)

        # rejected weakly supported curves
        for s_id, s in enumerate(sketch.strokes):
            if s.axis_label != 5 or s.is_ellipse():
                continue
            curve_supports = []
            for axis_id in range(3):
                for c_id, c in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    if not c_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                        # get intervals for this curve
                        c_i_intervals = []
                        for interval_id, interval_curve_id in enumerate(per_axis_per_stroke_interval_curve_ids[axis_id][s_id]):
                            if c_id in interval_curve_id:
                                c_i_intervals.append(per_axis_per_stroke_intervals[axis_id][s_id][interval_id])
                        curve_supports.append(np.sum([inter[1] - inter[0]
                                                      for inter in c_i_intervals]))
            median_support = 0.0
            if len(curve_supports) > 0:
                median_support = np.median(curve_supports)
            for axis_id in range(3):
                for c_id, c in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    if not c_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                        c_i_intervals = []
                        for interval_id, interval_curve_id in enumerate(per_axis_per_stroke_interval_curve_ids[axis_id][s_id]):
                            if c_id in interval_curve_id:
                                c_i_intervals.append(per_axis_per_stroke_intervals[axis_id][s_id][interval_id])
                        curve_support = np.sum([inter[1] - inter[0]
                                                for inter in c_i_intervals])
                        #if curve_support < 0.5*median_support and not np.isclose(curve_support, 0.5*median_support):
                        if curve_support <= 0.5*median_support and not np.isclose(curve_support, median_support):
                            per_axis_per_stroke_rejected_curves[axis_id][s_id].append(c_id)

        # foreshortening is a bad criteria to reject curves
        # reject overly foreshortened curves
        for s_id, s in enumerate(sketch.strokes):
            if s.axis_label != 5 or s.is_ellipse():
                continue
            curve_foreshortenings = []
            for axis_id in range(3):
                for c_id, c in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    if not c_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                        curve_foreshortenings.append(tools_3d.get_foreshortening(c, np.array(camera.cam_pos)))
            median_foreshortening = 0.0
            if len(curve_foreshortenings) > 0:
                median_foreshortening = np.median(curve_foreshortenings)
            for axis_id in range(3):
                for c_id, c in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    if not c_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                        curve_foreshortening = tools_3d.get_foreshortening(c, np.array(camera.cam_pos))
                        if curve_foreshortening > 5.0*median_foreshortening:
                            per_axis_per_stroke_rejected_curves[axis_id][s_id].append(c_id)

        # get curve epsilons
        curve_epsilons = []
        bbox_diags = []
        for axis_id in range(3):
            seg_lens = []
            all_points = []
            for s_id, s in enumerate(sketch.strokes):
                if s.axis_label != 5 or s.is_ellipse():
                    continue
                for seg_id, seg in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    if seg_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                        continue
                    seg_lens.append(tools_3d.line_3d_length(seg))
                    for p in seg:
                        all_points.append(p)
            all_points = np.array(all_points)
            if len(all_points) < 2:
                epsilon = 0.01
                curve_epsilons.append(epsilon)
                continue
            bbox = tools_3d.bbox_from_points(all_points)
            bbox_diag = tools_3d.bbox_diag(bbox)
            #epsilon = max(np.min(seg_lens)/2, 0.01*bbox_diag)
            epsilon = min(np.min(seg_lens)/2, 0.01*bbox_diag)
            curve_epsilons.append(epsilon)

        if SYMMETRIC_CURVE_FILTERING:
            # Symmetric point cloud
            filtered_out_curves = True
            while filtered_out_curves:
                per_axis_per_stroke_per_seg_sym_score = {}
                bbox_diags = []
                curve_epsilons = []
                for axis_id in range(3):
                    seg_lens = []
                    all_points = []
                    for s_id, s in enumerate(sketch.strokes):
                        if s.axis_label != 5 or s.is_ellipse():
                            continue
                        for seg_id, seg in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                            if seg_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                continue
                            seg_lens.append(tools_3d.line_3d_length(seg))
                            for p in seg:
                                all_points.append(p)
                    all_points = np.array(all_points)
                    if len(all_points) < 2:
                        continue
                    bbox = tools_3d.bbox_from_points(all_points)
                    bbox_diag = tools_3d.bbox_diag(bbox)
                    epsilon = min(np.min(seg_lens)/2, 0.01*bbox_diag)
                    bbox_diags.append(bbox_diag)
                    curve_epsilons.append(epsilon)
                    # equidistant resampling
                    all_points = []
                    point_stroke_seg_ids = []
                    for s_id, s in enumerate(sketch.strokes):
                        if s.axis_label != 5 or s.is_ellipse():
                            continue
                        for seg_id, seg in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                            if seg_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                continue
                            equi_seg = common_tools.equi_resample_polyline(seg, epsilon)
                            for p in equi_seg:
                                all_points.append(p)
                                point_stroke_seg_ids.append([s_id, seg_id])
                    all_points = np.array(all_points)
                    focus_vp = axis_id
                    sym_plane_point = np.zeros(3, dtype=np.float)
                    sym_plane_normal = np.zeros(3, dtype=np.float)
                    sym_plane_normal[focus_vp] = 1.0
                    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)

                    symmetric_points = common_tools.symmetric_point_cloud(all_points, refl_mat,
                                                                            threshold=0.05*bbox_diag,
                                                                            VERBOSE=False)
                    for pt_id, sym_score in enumerate(symmetric_points):
                        s_id, seg_id = point_stroke_seg_ids[pt_id]
                        if not (axis_id, s_id, seg_id) in per_axis_per_stroke_per_seg_sym_score:
                            per_axis_per_stroke_per_seg_sym_score[(axis_id, s_id, seg_id)] = [min(sym_score, 1.0)]
                        else:
                            per_axis_per_stroke_per_seg_sym_score[(axis_id, s_id, seg_id)].append(min(sym_score, 1.0))
                for key in per_axis_per_stroke_per_seg_sym_score.keys():
                    per_axis_per_stroke_per_seg_sym_score[key] = \
                        np.mean(per_axis_per_stroke_per_seg_sym_score[key])

                # Filter out non-symmetric curves
                filtered_out_curves = False
                for axis_id in range(3):
                    for s_id, s in enumerate(sketch.strokes):
                        if s.axis_label != 5 or s.is_ellipse():
                            continue
                        all_scores = []
                        seg_ids = []
                        for seg_id, seg in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                            if seg_id in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                continue
                            all_scores.append(per_axis_per_stroke_per_seg_sym_score[(axis_id, s_id, seg_id)])
                            seg_ids.append(seg_id)
                        sorted_ids = np.argsort(all_scores)
                        sorted_scores = np.sort(all_scores)
                        if len(all_scores) == 0:
                            continue
                        if sorted_scores[-1] > 0.95:
                            for vec_id, id in enumerate(sorted_ids):
                                if sorted_scores[vec_id] < 0.8 and not seg_ids[id] in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                    per_axis_per_stroke_rejected_curves[axis_id][s_id].append(seg_ids[id])
                                    filtered_out_curves = True
                        for vec_id, id in enumerate(sorted_ids):
                            if sorted_scores[vec_id] < 0.2 and not seg_ids[id] in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                per_axis_per_stroke_rejected_curves[axis_id][s_id].append(seg_ids[id])
                                filtered_out_curves = True
                        if len(all_scores) < 2:
                            continue
                        if np.isclose(sorted_scores[-2], 0.0):
                            confidence = 1.0
                        else:
                            confidence = sorted_scores[-1]/sorted_scores[-2]
                        if confidence > 2.0:
                            #per_axis_per_stroke_rejected_curves[axis_id][s_id] += sorted_ids[:-1]
                            for id in sorted_ids[:-1]:
                                if not seg_ids[id] in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                    per_axis_per_stroke_rejected_curves[axis_id][s_id].append(seg_ids[id])
                                    filtered_out_curves = True
                        else:
                            for vec_id, id in enumerate(sorted_ids):
                                if sorted_scores[vec_id] < 2/3*sorted_scores[-1] and not seg_ids[id] in per_axis_per_stroke_rejected_curves[axis_id][s_id]:
                                    per_axis_per_stroke_rejected_curves[axis_id][s_id].append(seg_ids[id])
                                    filtered_out_curves = True

        # filter out rejected curves
        for axis_id in range(3):
            for s_id, s in enumerate(sketch.strokes):
                if s.axis_label != 5 or s.is_ellipse():
                    continue
                rej_curve_ids = np.unique(per_axis_per_stroke_rejected_curves[axis_id][s_id])
                for del_id in sorted(rej_curve_ids, reverse=True):
                    del per_axis_per_stroke_curves[axis_id][s_id][del_id]
                    #per_axis_per_stroke_per_seg_sym_score.pop((axis_id, s_id, seg_id), None)
                for interval_id in range(len(per_axis_per_stroke_interval_curve_ids[axis_id][s_id])):
                    good_ids_mask = [vec_id for vec_id, vec_elem in enumerate(per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id])
                                     if not vec_elem in rej_curve_ids]
                    per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id] = np.array(per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id])[good_ids_mask].tolist()
                    # reindex the remaining indices
                    rej_curve_ids = np.array(rej_curve_ids)
                    for vec_id in range(len(per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id])):
                        nb_before_current_id = np.sum(rej_curve_ids < per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id][vec_id])
                        per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id][vec_id] = per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id][vec_id] - nb_before_current_id

        # cluster curves
        for axis_id in range(3):
            all_points = []
            for s_id, s in enumerate(sketch.strokes):
                if s.axis_label != 5 or s.is_ellipse():
                    continue
                for seg_id, seg in enumerate(per_axis_per_stroke_curves[axis_id][s_id]):
                    for p in seg:
                        all_points.append(p)
            all_points = np.array(all_points)
            if len(all_points) < 2:
                continue
            bbox = tools_3d.bbox_from_points(all_points)
            bbox_diag = tools_3d.bbox_diag(bbox)
            epsilon = 0.1*bbox_diag
            for s_id, s in enumerate(sketch.strokes):
                if s.axis_label != 5 or s.is_ellipse():
                    continue
                all_candidates = per_axis_per_stroke_curves[axis_id][s_id]
                if len(all_candidates) < 2:
                    continue
                proxies, per_proxy_line_ids = cluster_proxies.cluster_lines_non_unique_general(
                    all_candidates, max_length_ratio=0.1)
                per_axis_per_stroke_curves[axis_id][s_id] = proxies
                per_curve_proxy_ids = [[] for i in range(len(all_candidates))]
                for proxy_id, cluster in enumerate(per_proxy_line_ids):
                    for l_id in cluster:
                        per_curve_proxy_ids[l_id].append(proxy_id)
                for interval_id in range(len(per_axis_per_stroke_interval_curve_ids[axis_id][s_id])):
                    new_curve_ids = []
                    for curve_id in per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id]:
                        new_curve_ids += per_curve_proxy_ids[curve_id]
                    new_curve_ids = np.unique(new_curve_ids).tolist()
                    per_axis_per_stroke_interval_curve_ids[axis_id][s_id][interval_id] = new_curve_ids

        for corr_id in replace_corr_ids:
            corr = global_candidate_correspondences[corr_id]
            # get interval_ids for both strokes
            s_id_1 = corr[0]
            s_id_2 = corr[1]
            axis_id = corr[4]
            interval_id_1 = per_axis_per_stroke_corr_id_interval_id_dict[(axis_id, s_id_1, corr_id)]
            interval_id_2 = per_axis_per_stroke_corr_id_interval_id_dict[(axis_id, s_id_2, corr_id)]
            curve_ids_1 = per_axis_per_stroke_interval_curve_ids[axis_id][s_id_1][interval_id_1]
            curve_ids_2 = per_axis_per_stroke_interval_curve_ids[axis_id][s_id_2][interval_id_2]
            for c_1_id in curve_ids_1:
                for c_2_id in curve_ids_2:
                    new_corr = corr.copy()
                    new_corr[2] = per_axis_per_stroke_curves[axis_id][s_id_1][c_1_id]
                    new_corr[3] = per_axis_per_stroke_curves[axis_id][s_id_2][c_2_id]
                    global_candidate_correspondences.append(new_corr)
            #print("snd", corr_id)
            del_indices.append(corr_id)

    for s_id in range(len(sketch.strokes)):
        if len(self_symmetric_ellipses[s_id]) == 0:
            continue
        first_c = self_symmetric_ellipses[s_id][0]
        best_id = 0
        u, v = tools_3d.get_basis_for_planar_point_cloud(first_c[0])
        ecc = tools_3d.get_ellipse_eccentricity(first_c[0], u, v)
        best_ecc = ecc
        ecc_list = [ecc]
        for c_id, (c, focus_vp, corr_id) in enumerate(self_symmetric_ellipses[s_id][1:]):
            u, v = tools_3d.get_basis_for_planar_point_cloud(c)
            ecc = tools_3d.get_ellipse_eccentricity(c, u, v)
            ecc_list.append(ecc)
            if ecc < best_ecc:
                best_ecc = ecc
                best_id = c_id
        for c_id in range(len(self_symmetric_ellipses[s_id])):
            tmp_corr = self_symmetric_ellipses[s_id][c_id]
            ellipse_plane = Plane.best_fit(tmp_corr[0])
            if sketch.strokes[s_id].closest_major_axis_deviation < 15.0:
                minor_axis = np.zeros(3)
                minor_axis[sketch.strokes[s_id].closest_major_axis_id] = 1
                if np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(ellipse_plane.normal, minor_axis))))) > 45.0:
                    #print("3rd", corr_id)
                    del_indices.append(self_symmetric_ellipses[s_id][c_id][2])
                    continue
            if c_id != best_id and ecc_list[c_id] >= 0.8*best_ecc:
                global_candidate_correspondences[tmp_corr[2]][2] = tmp_corr[0]
                global_candidate_correspondences[tmp_corr[2]][3] = tmp_corr[0]
                per_axis_per_stroke_candidate_reconstructions[tmp_corr[1]][s_id].append(tmp_corr[0])
            else:
                #print("4th", corr_id)
                del_indices.append(self_symmetric_ellipses[s_id][c_id][2])

    del_indices = np.unique(del_indices)
    for del_id in sorted(del_indices, reverse=True):
        del global_candidate_correspondences[del_id]

    # look for midline curves
    same_axis_stroke_ids = [[] for i in range(6)]
    for s_id, s in enumerate(sketch.strokes):
        same_axis_stroke_ids[s.axis_label].append(s_id)
    curve_midline_stroke_ids = common_tools.get_midline_stroke_ids([5],
                                                                     global_candidate_correspondences,
                                                                     per_axis_per_stroke_candidate_reconstructions,
                                                                     sym_planes, same_axis_stroke_ids,
                                                                     sketch, camera,
                                                                     accuracy_multiplier=6.0,
                                                                     VERBOSE=False, curve_epsilons=curve_epsilons)

    # check if self-symmetric correspondences have symmetric helper-lines
    correspondence_graphs = [nx.Graph(), nx.Graph(), nx.Graph()]
    for corr in global_candidate_correspondences:
        correspondence_graphs[corr[4]].add_edge(corr[0], corr[1])
    counter = 0
    del_corr_ids = []
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if corr[0] == corr[1]:
            if sketch.strokes[corr[0]].is_ellipse():
                continue
            if not correspondence_graphs[corr[4]].has_edge(corr[-1], corr[-2]):
                counter += 1
                del_corr_ids.append(corr_id)
    for corr_id in reversed(del_corr_ids):
        del global_candidate_correspondences[corr_id]
    return curve_epsilons

def filter_weak_curve_correspondences(per_correspondence_sym_scores, global_candidate_correspondences, sketch, threshold=0.5):
    del_ids = []
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if len(per_correspondence_sym_scores[corr_id]) > 0:
            if np.max(per_correspondence_sym_scores[corr_id]) < threshold:
                del_ids.append(corr_id)
    # also filter weak ellipse correspondences
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if not(sketch.strokes[corr[0]].axis_label == 5 and sketch.strokes[corr[0]].is_ellipse()):
            continue
        if corr[0] == corr[1]:
            continue
        area_1 = sketch.strokes[corr[0]].linestring.linestring.convex_hull.area
        area_2 = sketch.strokes[corr[1]].linestring.linestring.convex_hull.area
        if area_1/area_2 > 3.0 or area_1/area_2:
            del_ids.append(corr_id)
    for del_id in reversed(np.unique(del_ids)):
        del global_candidate_correspondences[del_id]
        del per_correspondence_sym_scores[del_id]