import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.signal import argrelmax, savgol_filter, argrelmin, find_peaks
from sklearn.cluster import MeanShift
from pylowstroke.sketch_io import SketchSerializer as skio
import os

def get_batch_decomposition(global_candidate_correspondences, data_folder="", sketch=None, VERBOSE=True, min_dist=15):
    time_series = np.array([[cand[0], cand[1]] for cand in global_candidate_correspondences])
    batches = get_batch_decomposition_aux(time_series, min_dist=min_dist, VERBOSE=VERBOSE)

    return batches

def get_batch_decomposition_aux(time_series, sketch=None, VERBOSE=False, min_dist=5):
    time_series = np.unique(time_series, axis=0)
    time_series = np.sort(time_series, axis=-1)
    if VERBOSE:
        print("time_series")
        print(time_series)
    nb_strokes = max(np.max(time_series[:, 0]), np.max(time_series[:, 1]))
    if nb_strokes <= min_dist:
        return [[0, nb_strokes]]
    blue_normal = np.array(sns.color_palette("Set1", n_colors=nb_strokes))
    time_scales = {}
    for cand in time_series:
        scale = cand[1] - cand[0]
        if scale == 0:
            continue
        if scale in time_scales.keys():
            time_scales[scale].append(cand)
        else:
            time_scales[scale] = [cand]
    max_key = 0
    for key in sorted(time_scales):
        max_key = np.maximum(max_key, key)

    if VERBOSE:
        print("time_scales")
        print(time_scales)
    scales_hist = np.zeros(max_key+1, dtype=np.int)
    buckets = np.zeros(nb_strokes)
    for key_id, key in enumerate(sorted(time_scales)):
        scales_hist[key] = len(time_scales[key])

    all_min_ids = []
    for key_id, key in enumerate(sorted(time_scales)[:3]):
        for cand in time_scales[key]:
            buckets[cand[0]:cand[1]+1] += 1
        smooth_buckets = savgol_filter(buckets, window_length=7, polyorder=3)
        smooth_buckets[buckets == 0.0] = 0.0
        if VERBOSE:
            print("buckets")
            print(buckets)
            print("smooth_buckets")
            print(smooth_buckets)
        #print(np.argwhere(np.isclose(smooth_buckets, 0.0)).flatten())
        zeros = np.argwhere(np.isclose(smooth_buckets, 0.0)).flatten()
        tmp_min_ids = []
        if VERBOSE:
            print("buckets")
            print(buckets)
        for i in range(1, len(buckets)-1):
            if (buckets[i-1] > 0 and buckets[i] == 0) or (buckets[i+1] > 0 and buckets[i] == 0):
                tmp_min_ids.append(i)
        #for i in range(len(zeros)-1):
        #    if i == 0:
        #        if zeros[i+1] > zeros[i] + 1:
        #            tmp_min_ids.append(zeros[i])
        #    else:
        #        if zeros[i+1] > zeros[i] + 1 or zeros[i] > zeros[i-1] + 1:
        #            tmp_min_ids.append(zeros[i])
        all_min_ids += tmp_min_ids
        if VERBOSE:
            print("tmp_min_ids")
            print(tmp_min_ids)

        if VERBOSE:
            print("-smooth_buckets")
            print(-smooth_buckets)
        min_ids, dict = find_peaks(-smooth_buckets, prominence=0, distance=1)
        if VERBOSE:
            print("min_ids")
            print(min_ids)
        closest_max_ids = [[dict["left_bases"][i], dict["right_bases"][i]]
                           for i in range(len(min_ids))]
        if VERBOSE:
            print("closest_max_ids")
            print(closest_max_ids)
            #plt.bar(np.arange(len(smooth_buckets)), smooth_buckets)
            #plt.show()
        unique, unique_indices = np.unique(closest_max_ids, return_index=True, axis=0)
        final_min_ids = []
        final_closest_max_ids = []
        if VERBOSE:
            print("min_ids", min_ids)
            print("unique_indices")
            print(unique_indices)
        zero_ranges = np.argwhere(np.isclose(buckets, 0.0)).flatten().tolist()
        for i, u_id in enumerate(unique_indices):
            if VERBOSE:
                print("min_ids[unique_indices[i]]")
                print(min_ids[unique_indices[i]])
            if min_ids[unique_indices[i]] in zero_ranges:
                continue
            if i == len(unique_indices)-1:
                min_ids_in_range = min_ids[unique_indices[i]:]
            else:
                min_ids_in_range = min_ids[unique_indices[i]:unique_indices[i+1]]
            if len(min_ids_in_range) == 0:
                continue
            lowest_min_id = int(np.argmin(buckets[min_ids_in_range]))
            final_min_ids.append(min_ids_in_range[lowest_min_id])
            final_closest_max_ids.append(closest_max_ids[u_id])
        all_min_ids += final_min_ids
        if len(all_min_ids) == 0:
            continue
        if VERBOSE:
            print("np.unique(all_min_ids)")
            print(np.unique(all_min_ids))
        #for min_id in final_min_ids:
        #    all_min_ids.append(min_id)
        #all_min_ids += np.argwhere(np.isclose(smooth_buckets, 0.0)).flatten().tolist()
        if VERBOSE:
            print("all_min_ids", np.unique(all_min_ids))
        #plt.bar(np.arange(1, len(buckets)+1), buckets)
        #plt.bar(np.arange(1, len(buckets)+1), smooth_buckets, color="red")
        #plt.show()

    if VERBOSE:
        print("all_min_ids", np.unique(all_min_ids))
    clustering = MeanShift(bandwidth=3).fit(np.array(all_min_ids).reshape(-1, 1))
    nb_clusters = len(clustering.cluster_centers_)
    samples_per_cluster = [np.sum(clustering.labels_ == i) for i in range(nb_clusters)]
    median_samples_per_cluster = np.median(samples_per_cluster)
    if VERBOSE:
        print("samples_per_cluster", samples_per_cluster)
        print("median_samples_per_cluster", median_samples_per_cluster)
        print("clustering.labels_", clustering.labels_)
        print("clustering.cluster_centers_", clustering.cluster_centers_)
    keep_clusters_ids = [i for i in range(len(samples_per_cluster))
                         if samples_per_cluster[i] > 0.5*median_samples_per_cluster] # TODO:improve threshold
    if VERBOSE:
        print("keep_clusters_ids", keep_clusters_ids)
    keep_clusters = clustering.cluster_centers_[keep_clusters_ids].flatten()
    if VERBOSE:
        print("keep_clusters", keep_clusters)
    keep_clusters = keep_clusters[keep_clusters > min_dist]
    if VERBOSE:
        print("keep_clusters", keep_clusters)
    keep_clusters_samples = np.array(samples_per_cluster)[keep_clusters_ids]
    if VERBOSE:
        print("keep_clusters_samples")
        print(keep_clusters_samples)
    filtered_clusters = np.zeros(nb_strokes, dtype=int)
    for c_id, c in enumerate(keep_clusters):
        #print(c_id, c)
        filtered_clusters[int(np.round(c))] = keep_clusters_samples[c_id]
    if VERBOSE:
        print("filtered_clusters", filtered_clusters)
    if len(keep_clusters) == 0:
        return [[0, nb_strokes]]
    regions = [[0, int(np.round(np.min(keep_clusters)))]]
    if VERBOSE:
        print("regions")
        print(regions)
    filtered_clusters[int(np.min(keep_clusters))] = 0
    if VERBOSE:
        print("filtered_clusters", filtered_clusters)
    min_ids, dict = find_peaks(filtered_clusters, prominence=0, distance=min_dist)
    closest_max_ids = [[dict["left_bases"][i], dict["right_bases"][i]]
                       for i in range(len(min_ids))]
    #for base in closest_max_ids:
    #    print("base", base)
    if VERBOSE:
        print("closest_max_ids", closest_max_ids)
        print("filtered_clusters")
        print(filtered_clusters)
    final_min_ids = [base[0]-1+np.argmax(filtered_clusters[max(0, base[0]-1):min(base[1]+1, nb_strokes-1)])
                     for base in closest_max_ids]

    if VERBOSE:
        print("final_min_ids", final_min_ids)
    #print(clustering.cluster_centers_)
    #print(keep_clusters)
    #final_min_ids = np.sort(np.array(keep_clusters, dtype=int).flatten())
    #print(final_min_ids)
        print("regions", regions)
    #print(final_min_ids)
    #print(nb_strokes)
        print("final_min_ids")
        print(final_min_ids)
    if len(final_min_ids) > 1:
        if final_min_ids[0] < min_dist:
            regions = [[0, final_min_ids[1]]]
            final_min_ids = final_min_ids[1:]
        for i in range(len(final_min_ids[1:])):
            regions.append([final_min_ids[i]+1, final_min_ids[i+1]])
    if VERBOSE:
        print("final_min_ids")
        print(final_min_ids)
    #if nb_strokes - final_min_ids[-1] - 1 < min_dist:
    #    regions[-1][1] = nb_strokes
    #else:
    #print("nb_strokes", nb_strokes)
    #print("final_min_ids", final_min_ids)
    if len(final_min_ids) > 0:
        rest_strokes_nb = nb_strokes - final_min_ids[-1]+1
    else:
        rest_strokes_nb = nb_strokes - regions[-1][1]+1
    if VERBOSE:
        print("rest_strokes_nb", rest_strokes_nb)
    if rest_strokes_nb > 20:
        regions.append([final_min_ids[-1]+1, final_min_ids[-1]+1+int(rest_strokes_nb/2)])
        regions.append([final_min_ids[-1]+2+int(rest_strokes_nb/2), nb_strokes])
    else:
        if len(final_min_ids) > 0:
            regions.append([final_min_ids[-1]+1, nb_strokes])
        else:
            regions.append([regions[-1][1]+1, nb_strokes])
    if VERBOSE:
        print("regions")
        print(regions)
    if regions[-1][0] - regions[-2][1] > 1:
        regions[-1][0] = regions[-2][1] + 1
    if nb_strokes - regions[-1][0] < 5:
        regions[-2][1] = nb_strokes
        del regions[-1]

    if regions[0][1] < min_dist:
        del regions[0]
        regions[0][0] = 0

    if VERBOSE:
        print("regions")
        print(regions)
    if len(regions) > 1: 
        if (regions[1][0] - regions[0][1] < min_dist):
            regions[1][0] = regions[0][1]+1
        else:
            regions.insert(1, [regions[0][1]+1, final_min_ids[0]])

    # split first batch if bigger than 30. For scalability
    if VERBOSE:
        print("regions", regions)
    if regions[0][1] - regions[0][0] > 30:
        subregions = np.array_split(np.arange(0, regions[0][1]+1), np.ceil(regions[0][1]/30))
        for sub_id, sub in enumerate(subregions):
            regions.insert(sub_id, [int(sub[0]), int(sub[-1])])
        del regions[len(subregions)]
    if VERBOSE:
        print("regions", regions)

    #if VERBOSE:
    #    nb_cols = 3
    #    nb_regions = len(regions)
    #    fig, axes_2 = plt.subplots(nrows=int(np.ceil(nb_regions/nb_cols)), ncols=nb_cols)
    #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
    #                        bottom=0.0,
    #                        top=1.0)
    #    axes_2 = axes_2.flatten()
    #    for ax in axes_2:
    #        ax.set_xlim(0, sketch.width)
    #        ax.set_ylim(sketch.height, 0)
    #        ax.axis("equal")
    #        ax.axis("off")
    #    for i in range(len(regions)):
    #        sketch.display_strokes_2(fig=fig, ax=axes_2[i],
    #                                 color_process=lambda s: "#000000FF",
    #                                 linewidth_data=lambda s: 3.0, norm_global=True)
    #        sketch.display_strokes_2(fig=fig, ax=axes_2[i],
    #                                 color_process=lambda s: blue_normal[i],
    #                                 linewidth_data=lambda s: 4.0, norm_global=True,
    #                                 display_strokes=np.arange(regions[i][0], regions[i][1]+1))
    #        axes_2[i].set_xlim(0, sketch.width)
    #        axes_2[i].set_ylim(sketch.height, 0)
    #        axes_2[i].axis("equal")
    #        axes_2[i].axis("off")
    #    plt.show()
    return regions

if __name__ == "__main__":
    my_dpi = 96
    sketch_folder_name = "../figures/sketch_svgs"
    output_folder_name = "../figures/stroke_range_decompositions"
    sketch_file_name = "student8_house.svg"
    #sketch_file_name = "student8_mouse.svg"
    #sketch_file_name = "Professional3_vacuum_cleaner.svg"
    #sketch_file_name = "designer2_trash_bin.svg"
    #sketch_file_name = "Prof2task2_armchair_02.svg"
    #sketch_file_name = "designer2_armchair_02.svg"
    #sketch_file_name = "Professional3_mixer.svg"
    #sketch_file_name = "Professional3_tubes.svg"
    #time_series_file_name = "Professional3_tubes.npy"
    time_series_file_name = "student8_house.npy"
    time_series_file_name = "Professional3_vacuum_cleaner.npy"
    time_series_file_name = "Professional1_house.npy"
    #time_series_file_name = "Professional3_mixer.npy"
    #time_series_file_name = "Professional3_vacuum_cleaner.npy"
    #time_series_file_name = "designer2_armchair_02.npy"
    #time_series_file_name = "student8_mouse.npy"
    #time_series_file_name = "designer2_trash_bin.npy"
    #time_series_file_name = "Prof2task2_armchair_02.npy"
    full_file_name = os.path.join(sketch_folder_name, sketch_file_name)
    sketch = skio.load(full_file_name)
    designer_name = sketch_file_name.split("_")[0]
    object_name = "".join(sketch_file_name.split("_")[1:]).split(".")[0]
    print(designer_name)
    print(object_name)
    #sketch = skio.load("../figures/sketch_svgs/Professional3_tubes.svg")
    #time_series = np.load("time_series.np.npy")
    #time_series = np.load("../figures/time_series/Professional3_tubes.npy")
    time_series_folder_name = "../figures/time_series"
    time_series = np.load(os.path.join(time_series_folder_name, time_series_file_name))
    regions = get_batch_decomposition(time_series, min_dist=15)
    print(regions)
    exit()

    time_series = np.unique(time_series, axis=0)
    time_series = np.sort(time_series, axis=-1)
    print(time_series)
    blue_normal = np.array(sns.color_palette("Set1", n_colors=len(sketch.strokes)))

    full_output_folder_name = os.path.join(output_folder_name, designer_name+"_"+object_name)
    if not os.path.exists(full_output_folder_name):
        os.mkdir(full_output_folder_name)

    nb_strokes = np.max(time_series[:, 1])
    buckets = np.zeros(nb_strokes)
    s_id = 0
    cand_id = 0
    #while s_id <= nb_strokes and cand_id < len(time_series):
    #    if time_series[cand_id][1] > s_id:
    #        s_id += 1
    #        #plt.hist(buckets, bins=nb_strokes)
    #        plt.bar(np.arange(nb_strokes), buckets)
    #        plt.ylim(0, 70)
    #        plt.show()
    #        continue
    #    buckets[time_series[cand_id][0]:(time_series[cand_id][1]+1)] += 1
    #    print(time_series[cand_id])
    #    cand_id += 1

    #time_scales = np.zeros([time_series.shape[0], 3], dtype=np.int)
    #print(time_scales.shape)
    ##time_scales[:, :2] = time_series
    ##time_scales[:, 2] = time_scales[:, 1] - time_scales[:, 0]
    #print(time_scales)
    #times_scales_sorted = np.sort(time_scales, axis=-1)
    #print(times_scales_sorted)
    time_scales = {}
    for cand in time_series:
        scale = cand[1] - cand[0]
        if scale == 0:
            continue
        if scale in time_scales.keys():
            time_scales[scale].append(cand)
        else:
            time_scales[scale] = [cand]
    max_key = 0
    for key in sorted(time_scales):
        max_key = np.maximum(max_key, key)

    scales_hist = np.zeros(max_key+1, dtype=np.int)
    buckets = np.zeros(nb_strokes)
    #sketch.display_strokes_2(fig=fig, ax=axes,
    #                                  color_process=lambda s: ["green", "blue", "red", "yellow"][s.axis_label] if s.axis_label < 4 else "black",
    #                                  #color_process=lambda s: "#000000FF",
    #                                  linewidth_data=lambda s: 3.0, norm_global=True)
    #axes.set_xlim(0, original_sketch.width)
    #axes.set_ylim(original_sketch.height, 0)
    #axes.axis("equal")
    #axes.axis("off")
    #plt.savefig("../figures/results/"+sketch_name+"_input_sketch.png")
    #plt.show()
    for key_id, key in enumerate(sorted(time_scales)):
        scales_hist[key] = len(time_scales[key])

    for key_id, key in enumerate(sorted(time_scales)[:10]):
    #    print(key, len(time_scales[key]))
        fig, axes = plt.subplots(nrows=1, ncols=2)
    #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
    #                        bottom=0.0,
    #                        top=1.0)
        scales_colors = np.array(["blue" for i in range((len(buckets)))])
        scales_colors[key] = "red"
        axes[0].bar(np.arange(len(scales_hist)), scales_hist, color=scales_colors)
        axes[0].set_title("Histogram of symmetry correspondence lengths")
        axes[0].set_xlabel("Length of symmetry correspondence")
        axes[0].set_ylabel("Number of symmetry correspondences")
        axes[1].set_title("Histogram of symmetry correspondences over stroke indices")
        axes[1].set_xlabel("Stroke indices")
        axes[1].set_ylabel("Accumulated symmetry correspondences")
        for cand in time_scales[key]:
            buckets[cand[0]:cand[1]+1] += 1
        smooth_buckets = savgol_filter(buckets, window_length=7, polyorder=3)
        max_ids = argrelmax(smooth_buckets, order=3)[0]
        min_ids = argrelmin(smooth_buckets, order=3)[0]
        #min_ids = find_peaks(-buckets, prominence=key_id)[0]
        min_ids, dict = find_peaks(-smooth_buckets, prominence=1)
    #    print(dict)
        #colors = np.array(["blue" for i in range((len(buckets)))])
        colors = np.zeros([len(buckets), 3])
        #colors[max_ids] = "red"
        closest_max_ids = []
    #    print(max_ids)
    #    print(min_ids)
    #    for min_id in min_ids:
    #        # get two closest max_ids
    #        found_max_id = False
    #        for max_id_id, max_id in enumerate(max_ids):
    #            if max_id > min_id:
    #                if max_id_id == 0:
    #                    closest_max_ids.append([-1, max_id])
    #                else:
    #                    closest_max_ids.append([max_ids[max_id_id-1], max_id])
    #                found_max_id = True
    #                break
    #        if not found_max_id:
    #            closest_max_ids.append([max_ids[-1], -1])
        closest_max_ids = [[dict["left_bases"][i], dict["right_bases"][i]]
                           for i in range(len(min_ids))]
        unique, unique_indices = np.unique(closest_max_ids, return_index=True, axis=0)
        final_min_ids = []
        final_closest_max_ids = []
        for i, u_id in enumerate(unique_indices):
            if i == len(unique_indices)-1:
                min_ids_in_range = min_ids[unique_indices[i]:]
            else:
                min_ids_in_range = min_ids[unique_indices[i]:unique_indices[i+1]]
            if len(min_ids_in_range) == 0:
                continue
            lowest_min_id = int(np.argmin(buckets[min_ids_in_range]))
            closest_max_ids_in_range = closest_max_ids[u_id]
            final_min_ids.append(min_ids_in_range[lowest_min_id])
            final_closest_max_ids.append(closest_max_ids[u_id])
        if len(final_min_ids) == 0:
            continue
        regions = [[0, final_min_ids[0]]]
        for i in range(len(final_min_ids[1:])):
            regions.append([final_min_ids[i], final_min_ids[i+1]])
        regions.append([final_min_ids[-1], nb_strokes])
        for region_id, region in enumerate(regions):
            colors[region[0]:region[1]+1] = blue_normal[region_id]
        print(regions)
        axes[1].bar(np.arange(nb_strokes), smooth_buckets, color=colors)
        axes[0].set_ylim(0, np.max(scales_hist)+2)
        axes[1].set_ylim(0, 15)
        nb_regions = len(regions)
        nb_cols = 3
        fig_2, axes_2 = plt.subplots(nrows=int(np.ceil(nb_regions/nb_cols)), ncols=nb_cols)
        fig_2.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                              bottom=0.0,
                              top=1.0)
        axes_2 = axes_2.flatten()
        for ax in axes_2:
            ax.set_xlim(0, sketch.width)
            ax.set_ylim(sketch.height, 0)
            ax.axis("equal")
            ax.axis("off")
        for i in range(len(regions)):
            sketch.display_strokes_2(fig=fig, ax=axes_2[i],
                                              color_process=lambda s: "#000000FF",
                                              linewidth_data=lambda s: 3.0, norm_global=True)
            sketch.display_strokes_2(fig=fig, ax=axes_2[i],
                                     color_process=lambda s: blue_normal[i],
                                     linewidth_data=lambda s: 4.0, norm_global=True,
                                     display_strokes=np.arange(regions[i][0], regions[i][1]+1))
            axes_2[i].set_xlim(0, sketch.width)
            axes_2[i].set_ylim(sketch.height, 0)
            axes_2[i].axis("equal")
            axes_2[i].axis("off")
        #plt.savefig("../figures/results/"+sketch_name+"_input_sketch.png")
        first_file_name = os.path.join(full_output_folder_name, str(key_id)+"_histo.png")
        snd_file_name = os.path.join(full_output_folder_name, str(key_id)+"_sketches.png")
        fig.set_size_inches((2048 / my_dpi, 1024 / my_dpi))
        fig_2.set_size_inches((2048 / my_dpi, 1024 / my_dpi))
        fig.savefig(first_file_name, dpi=my_dpi)
        fig_2.savefig(snd_file_name, dpi=my_dpi)
        #break
        #plt.show()
        #plt.ylim(0, 70)
        #plt.show()
