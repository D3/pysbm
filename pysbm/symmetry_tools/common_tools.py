import numpy as np
from shapely.geometry import MultiLineString, LineString, MultiPoint
import pysbm.bezier as bezier
import pysbm.tools_3d as tools_3d
from pysbm.intersections import get_median_line_lengths
from scipy.spatial import distance
from skspatial.objects import Plane, Points
from hausdorff import hausdorff_distance
from scipy.spatial.distance import directed_hausdorff
from scipy.optimize import minimize, Bounds
from scipy.linalg import lstsq
import networkx as nx

CURVE_SAMPLE_SIZE = 30
def get_midline_stroke_ids(selected_axes, global_candidate_correspondences,
                           per_axis_per_stroke_candidate_reconstructions,
                           sym_planes, same_axis_stroke_ids, sketch, camera,
                           accuracy_multiplier=2.0,
                           VERBOSE=False,
                           curve_epsilons=[]):
    all_midline_stroke_ids = []
    for corr in global_candidate_correspondences:
        if corr[0] == corr[1] or not sketch.strokes[corr[0]].axis_label in selected_axes:
            continue
        # project both 3D strokes on sym_plane
        s_1_projected = np.array([sym_planes[corr[4]].project_point(p) for p in corr[2]])
        s_2_projected = np.array([sym_planes[corr[4]].project_point(p) for p in corr[3]])
        s_1_projected_2d = np.array(camera.project_polyline(s_1_projected))
        s_2_projected_2d = np.array(camera.project_polyline(s_2_projected))
        mulitline = MultiLineString([s_1_projected_2d, s_2_projected_2d])
        tmp_max_acc_radius = accuracy_multiplier*max(sketch.strokes[corr[0]].acc_radius, sketch.strokes[corr[1]].acc_radius)
        # check for possible middle lines
        midline_stroke_ids = []
        #print(same_axis_stroke_ids[sketch.strokes[corr[0]].axis_label])
        for s_id in same_axis_stroke_ids[sketch.strokes[corr[0]].axis_label]:
            #print(s_id)
            if sketch.strokes[corr[0]].axis_label == 5 and \
                (sketch.strokes[s_id].is_ellipse() != sketch.strokes[corr[0]].is_ellipse()):
                continue

            s = sketch.strokes[s_id]
            inter_length = mulitline.buffer(tmp_max_acc_radius).intersection(s.linestring.linestring).length
            if inter_length/s.linestring.linestring.length > 0.75 or \
                    inter_length/mulitline.length > 0.75:
                if s.axis_label == 5 and s.is_ellipse() and sketch.strokes[s_id].closest_major_axis_deviation < 15.0:
                    if corr[4] != sketch.strokes[s_id].closest_major_axis_id:
                        continue
                midline_stroke_ids.append(s_id)

        if len(midline_stroke_ids) == 0:
            continue
        if corr[0] in midline_stroke_ids or corr[1] in midline_stroke_ids:
            continue

        plane_point = np.zeros(3)
        plane_normal = np.zeros(3)
        plane_normal[corr[4]] = 1.0
        #if len(midline_stroke_ids) > 1:
        #    continue
        all_midline_stroke_ids += midline_stroke_ids
        # create a self-symmetric correspondence for each midline stroke
        for midline_stroke_id in midline_stroke_ids:
            if sketch.strokes[midline_stroke_id].axis_label != 5:
                s_proj = np.array(camera.lift_polyline_to_plane([sketch.strokes[midline_stroke_id].points_list[0].coords,
                                                                 sketch.strokes[midline_stroke_id].points_list[-1].coords],
                                                                plane_point, plane_normal))
            else:
                if sketch.strokes[midline_stroke_id].is_ellipse():
                    s_proj = np.array(camera.lift_polyline_to_plane([p.coords
                                                                     for p in sketch.strokes[midline_stroke_id].points_list],
                                                                    plane_point, plane_normal))
                else:
                    c_bez = sketch.strokes[midline_stroke_id].bezier
                    c_points = []
                    for c_i in range(int(len(c_bez)/4)):
                        for t in np.linspace(0.0, 1.0, num=CURVE_SAMPLE_SIZE):
                        #for t in np.arange(0.0, 1.0, 0.1):
                            c_points.append(bezier.q(c_bez[4*c_i:4*(c_i+1)+1], t))
                    s_proj = np.array(camera.lift_polyline_to_plane(c_points, plane_point, plane_normal))

                    all_points = []
                    point_stroke_ids = []
                    #first_seg = np.array([p for p in corr[2]] + [p for p in corr[3]])
                    first_seg = np.array([p for p in corr[2]])
                    both_segs = [s_proj, first_seg]
                    for i, equi_seg in enumerate(both_segs):
                        for p in equi_seg:
                            all_points.append(p)
                            point_stroke_ids.append(i)
                    all_points = np.array(all_points)
                    sym_plane_point = np.zeros(3, dtype=np.float)
                    sym_plane_normal = np.zeros(3, dtype=np.float)
                    sym_plane_normal[corr[4]] = 1.0
                    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
                    min_length = np.min([tools_3d.line_3d_length(both_segs[i]) for i in range(2)])

                    symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                             threshold=0.1*min_length,
                                                             VERBOSE=False)
                    sym_scores = [[] for l in range(2)]
                    for pt_id, sym_score in enumerate(symmetric_points):
                        s_id = point_stroke_ids[pt_id]
                        sym_scores[s_id].append(min(sym_score, 1.0))
                    for s_id, scores in enumerate(sym_scores):
                        sym_scores[s_id] = np.mean(scores)
                    #if midline_stroke_id == 27:
                    first_score = sym_scores[0]

                    all_points = []
                    point_stroke_ids = []
                    first_seg = np.array([p for p in corr[3]])
                    both_segs = [s_proj, first_seg]
                    for i, equi_seg in enumerate(both_segs):
                        for p in equi_seg:
                            all_points.append(p)
                            point_stroke_ids.append(i)
                    all_points = np.array(all_points)
                    sym_plane_point = np.zeros(3, dtype=np.float)
                    sym_plane_normal = np.zeros(3, dtype=np.float)
                    sym_plane_normal[corr[4]] = 1.0
                    refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
                    min_length = np.min([tools_3d.line_3d_length(both_segs[i]) for i in range(2)])

                    symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                             threshold=0.1*min_length,
                                                             VERBOSE=False)
                    sym_scores = [[] for l in range(2)]
                    for pt_id, sym_score in enumerate(symmetric_points):
                        s_id = point_stroke_ids[pt_id]
                        sym_scores[s_id].append(min(sym_score, 1.0))
                    for s_id, scores in enumerate(sym_scores):
                        sym_scores[s_id] = np.mean(scores)
                    snd_score = sym_scores[0]
                    if first_score < 0.8 or snd_score < 0.8:
                        continue

            new_corr = [midline_stroke_id, midline_stroke_id, s_proj, s_proj,
                        corr[4], -1, -1, corr[0], corr[1]]

            global_candidate_correspondences.append(new_corr)
            per_axis_per_stroke_candidate_reconstructions[corr[4]][midline_stroke_id].append(s_proj)
    return np.unique(all_midline_stroke_ids).tolist()

def symmetric_point_cloud(pc, refl_mat, threshold=0.01, VERBOSE=False):

    reflected_points = tools_3d.apply_hom_transform_to_points(pc, refl_mat)
    dist = distance.cdist(pc, reflected_points)
    close_pts = np.zeros(len(pc), dtype=np.int)

    for i in range(len(pc)):
        dist[i, i] = 100.0
    for i in range(len(pc)):
        close_pts[i] = np.sum(dist[i, :] < threshold)
    return close_pts

def curve_symmetry_candidate(s1, s2, vanishing_triangle, axis_id, sketch, cam, angle_threshold=15.0,
                             dist_threshold=0.05, VERBOSE=False):
    axes_distances = get_median_line_lengths(sketch)
    # spatial distance
    center_1 = np.mean(np.array(s1.linestring.linestring.coords), axis=0)
    center_2 = np.mean(np.array(s2.linestring.linestring.coords), axis=0)
    spatial_dist = min(1.0, np.linalg.norm(center_1-center_2)/axes_distances[axis_id])
    spatial_dist = min(1.0, s1.linestring.linestring.distance(s2.linestring.linestring)/axes_distances[axis_id])

    if not vanishing_triangle.intersects(s2.linestring.linestring):
        return False
    vp = np.array(cam.vanishing_points_coords[axis_id])
    translational_symmetry = True
    reflectional_symmetry = True
    if spatial_dist < 0.1:
        translational_symmetry = False

    max_length = max(s1.length(), s2.length())
    max_length = min(s1.length(), s2.length())

    vanishing_triangle_pts = np.array(vanishing_triangle.exterior.coords)
    v1 = np.array([vanishing_triangle_pts[1], vanishing_triangle_pts[0]])
    v2 = np.array([vanishing_triangle_pts[1], vanishing_triangle_pts[2]])
    v_mid = np.array([(v1[1]+v2[1])/2, vanishing_triangle_pts[1]])

    s1_pts = np.array(s1.linestring.linestring.coords)
    s2_intersection = vanishing_triangle.intersection(s2.linestring.linestring)
    if type(s2_intersection) == MultiLineString:
        s2_pts = np.concatenate([np.array(l.coords) for l in s2_intersection.geoms])
    else:
        s2_pts = np.array(s2_intersection.coords)

    # check if strokes are almost overlapping
    registered_s1_pts, angle, reflection = tools_3d.icp_registration(s1_pts, s2_pts)
    if not reflection and angle < angle_threshold and \
        np.linalg.norm(registered_s1_pts[0]-s1_pts[0])/axes_distances[axis_id] < 0.15:
        return False

    proj_center_1 = LineString(v_mid).interpolate(LineString(v_mid).project(MultiPoint(np.array(s1.linestring.linestring.coords)).centroid))
    proj_center_2 = LineString(v_mid).interpolate(LineString(v_mid).project(MultiPoint(s2_pts).centroid))
    in_between_center = (np.array(proj_center_1.coords[0])+np.array(proj_center_2.coords[0]))/2
    v_mid_vec = v_mid[1] - v_mid[0]
    v_mid_vec /= np.linalg.norm(v_mid_vec)
    v_mid_ortho_vec = np.array([-v_mid_vec[1], v_mid_vec[0]])

    scale_1_1 = LineString(v1).interpolate(LineString(v1).project(proj_center_1))
    scale_1_2 = LineString(v2).interpolate(LineString(v2).project(proj_center_1))
    scale_1 = np.linalg.norm(np.array(scale_1_1.coords[0]) - np.array(scale_1_2.coords[0]))
    scale_2_1 = LineString(v1).interpolate(LineString(v1).project(proj_center_2))
    scale_2_2 = LineString(v2).interpolate(LineString(v2).project(proj_center_2))
    scale_2 = np.linalg.norm(np.array(scale_2_1.coords[0]) - np.array(scale_2_2.coords[0]))
    scaling_factor = scale_2/scale_1
    s1_center = np.array(MultiPoint(s1_pts).centroid.coords[0])
    recentered_s1_pts = s1_pts - s1_center
    recentered_s1_pts = s1_pts - np.array(proj_center_1.coords[0])
    x_axis = np.array([1, 0])
    primal_ortho_vec_angle = np.sign(np.cross(x_axis, v_mid_ortho_vec)) * np.arccos(np.abs(np.dot(x_axis, v_mid_ortho_vec)))
    rotated_s1_pts = tools_3d.rotate_angle(recentered_s1_pts, -primal_ortho_vec_angle)
    rescaled_s1_pts = scaling_factor*rotated_s1_pts
    rerotated_s1_pts = tools_3d.rotate_angle(rescaled_s1_pts, primal_ortho_vec_angle)
    retranslated_s1_pts = rerotated_s1_pts + np.array(proj_center_2.coords[0])
    registered_s1_pts, angle, reflection = tools_3d.icp_registration(retranslated_s1_pts, s2_pts)
    dist_1, pointwise_dists = tools_3d.chamfer_distance(registered_s1_pts, s2_pts, return_pointwise_distances=True)
    if reflection:
        translational_symmetry = False
    if angle > angle_threshold:
        translational_symmetry = False
    if dist_1 > max_length*dist_threshold:
        translational_symmetry = False

    # reflection
    reflection_axes = []
    reflectional_symmetries = []
    if axis_id == 0:
        reflection_axes = [1, 2]
    elif axis_id == 1:
        reflection_axes = [0, 2]
    elif axis_id == 2:
        reflection_axes = [0, 1]
    tmp_reflectional_symmetries = [True, True]
    for vec_id, reflection_axis_id in enumerate(reflection_axes):


        v_mid_ortho_vec = np.array(cam.vanishing_points_coords[reflection_axis_id]) - in_between_center
        v_mid_ortho_vec /= np.linalg.norm(v_mid_ortho_vec)
        reflection_line = LineString([in_between_center-1000.0*v_mid_ortho_vec,
                                      in_between_center+1000.0*v_mid_ortho_vec])

        reflected_pts = []
        for p_id, p in enumerate(s1_pts):
            p_vanishing_vec = np.array(cam.vanishing_points_coords[axis_id]) - p
            p_vanishing_vec /= np.linalg.norm(p_vanishing_vec)
            if np.dot(in_between_center-p, p_vanishing_vec) < 0:
                p_vanishing_vec *= -1
            p_vanishing_line = LineString([p, p+1000.0*p_vanishing_vec])
            if p_vanishing_line.intersects(reflection_line):
                dist_to_midline = np.linalg.norm(np.array(p_vanishing_line.intersection(reflection_line).coords[0]) - p)
                refl_p = p + 2.0*dist_to_midline*p_vanishing_vec
                reflected_pts.append(refl_p)
        if len(reflected_pts) == 0:
            tmp_reflectional_symmetries[vec_id] = False
            continue
        reflected_pts = np.array(reflected_pts)
        reflected_registered_s1_pts, angle, reflection = tools_3d.icp_registration(reflected_pts, s2_pts)
        if np.linalg.norm(reflected_pts[0]-reflected_registered_s1_pts[0]) > 0.25*max_length:
            tmp_reflectional_symmetries[vec_id] = False
        dist_2, pointwise_dists = tools_3d.chamfer_distance(reflected_registered_s1_pts, s2_pts, return_pointwise_distances=True)
        if reflection:
            tmp_reflectional_symmetries[vec_id] = False
        if angle > angle_threshold:
            tmp_reflectional_symmetries[vec_id] = False
        if dist_2 > max_length*dist_threshold:
           tmp_reflectional_symmetries[vec_id] = False

    return translational_symmetry or np.any(tmp_reflectional_symmetries)

def reconstruct_symmetric_curves_planar(s_id_1, s_id_2, axis_id,
                                        sketch, camera,
                                        per_stroke_proxies,
                                        minimal_foreshortening=True,
                                        VERBOSE=False):
    s_1_pts = np.array([p.coords for p in sketch.strokes[s_id_1].points_list])
    s_2_pts = np.array([p.coords for p in sketch.strokes[s_id_2].points_list])
    dist_vec = np.array(camera.get_camera_point_ray(s_1_pts[0])[1])
    cam_pos = np.array(camera.cam_pos)
    opt_vars = np.ones(1, dtype=float)

    # x: [plane_dist, plane_n(3)]
    def opt_func(x, tmp_plane_n):

        # FULL CURVE
        plane_p = cam_pos + x[0]*dist_vec
        if np.isclose(np.linalg.norm(x[1:]), 0.0):
            return 100.0
        c_1_full_curve = np.array(camera.lift_polyline_to_plane(s_1_pts, plane_p, tmp_plane_n))
        pts = Points(c_1_full_curve)
        if pts.are_collinear():
            return 100.0
        u, v = tools_3d.get_basis_for_planar_point_cloud(c_1_full_curve)
        ecc_1 = tools_3d.get_ellipse_eccentricity(c_1_full_curve, u, v)
        # reflect c_1
        c_1_refl = c_1_full_curve.copy()
        c_1_refl[:, axis_id] *= -1
        plane_2 = Plane.best_fit(c_1_refl)
        c_2_full_curve = np.array(camera.lift_polyline_to_plane(s_2_pts, plane_2.point, plane_2.normal))
        u, v = tools_3d.get_basis_for_planar_point_cloud(c_2_full_curve)
        ecc_2 = tools_3d.get_ellipse_eccentricity(c_2_full_curve, u, v)

        # OBJECTIVE FUNCTION
        dist = hausdorff_distance(c_1_refl, c_2_full_curve)
        refl_score = 1.0 - np.exp(-dist)
        ecc_score = 1.0 - np.exp(-max(ecc_1, ecc_2))
        weight = 0.5
        obj = weight*refl_score + (1-weight)*ecc_score

        return obj

    #bounds = Bounds([0, 0, 0, 0],
    #                [10, 1, 1, 1])
    bounds = Bounds([0],
                    [10])
    solutions = []
    solutions_fun = []
    for i in range(3):
        plane_n = np.zeros(3)
        plane_n[i] = 1.0
        res = minimize(opt_func, opt_vars, method="SLSQP", bounds=bounds, args=plane_n)
        solutions.append(res.x)
        solutions_fun.append(res.fun)
    res = solutions[np.argmin(solutions_fun)]
    plane_p = cam_pos + res[0]*dist_vec
    plane_n = np.zeros(3)
    plane_n[np.argmin(solutions_fun)] = 1.0
    if np.isclose(np.linalg.norm(plane_n), 0.0):
        return [], []
    plane_n /= np.linalg.norm(plane_n)
    c_1 = np.array(camera.lift_polyline_to_plane(s_1_pts, plane_p, plane_n))
    c_1_refl = c_1.copy()
    c_1_refl[:, axis_id] *= -1
    plane_2 = Plane.best_fit(c_1_refl)
    c_2 = np.array(camera.lift_polyline_to_plane(s_2_pts, plane_2.point, plane_2.normal))
    return c_1, c_2

def reconstruct_symmetric_curves_bezier_partial(s_id_1, s_id_2, axis_id, per_stroke_proxies,
                                                sketch, camera, minimal_foreshortening=True,
                                                VERBOSE=False, include_more_curves=False):
    c_1_bezier = sketch.strokes[s_id_1].bezier
    c_2_bezier = sketch.strokes[s_id_2].bezier
    nb_c_1_bezier_ctrl_pts = len(c_1_bezier)
    nb_c_2_bezier_ctrl_pts = len(c_2_bezier)
    c_1_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_1_bezier])
    for i in range(nb_c_1_bezier_ctrl_pts):
        c_1_vecs[i] /= np.linalg.norm(c_1_vecs[i])

    c_2_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_2_bezier])
    for i in range(nb_c_2_bezier_ctrl_pts):
        c_2_vecs[i] /= np.linalg.norm(c_2_vecs[i])
    cam_pos = np.array(camera.cam_pos)
    opt_vars = np.ones(nb_c_1_bezier_ctrl_pts+nb_c_2_bezier_ctrl_pts+4, dtype=float)
    opt_vars[8] = 0.0
    opt_vars[10] = 0.0

    # x: [c_1_bezier_depths, c_2_bezier_depths, a_1, b_1, a_2, b_2]
    def opt_func(x, c_2_vecs_local):
        c_1_ctrl_pts = cam_pos + x[:4].reshape(-1, 1)*c_1_vecs
        c_2_ctrl_pts = cam_pos + x[4:8].reshape(-1, 1)*c_2_vecs_local
        # FULL CURVE
        c_1_full_curve = cubic_bezier_eval_paralllel(c_1_ctrl_pts, 0.0, 1.0, CURVE_SAMPLE_SIZE)
        c_2_full_curve = cubic_bezier_eval_paralllel(c_2_ctrl_pts, 0.0, 1.0, CURVE_SAMPLE_SIZE)
        c_1_len = np.sum(np.linalg.norm(c_1_full_curve[1:] - c_1_full_curve[:len(c_1_full_curve)-1], axis=1))
        c_2_len = np.sum(np.linalg.norm(c_2_full_curve[1:] - c_2_full_curve[:len(c_2_full_curve)-1], axis=1))
        # PARTIAL CURVE
        c_1_partial_curve = equi_resample_parallel(c_1_ctrl_pts, x[8], x[9], CURVE_SAMPLE_SIZE)
        if len(c_1_partial_curve) == 0:
            c_1_partial_curve = np.array([bezier.q(c_1_ctrl_pts, x[8])])
        c_1_partial_curve[:, axis_id] *= -1
        c_2_partial_curve = equi_resample_parallel(c_2_ctrl_pts, x[10], x[11], CURVE_SAMPLE_SIZE)
        if len(c_2_partial_curve) == 0:
            c_2_partial_curve = np.array([bezier.q(c_2_ctrl_pts, x[10])])
        c_1_partial_len = np.sum(np.linalg.norm(c_1_partial_curve[1:] - c_1_partial_curve[:(len(c_1_partial_curve)-1)], axis=1))
        c_2_partial_len = np.sum(np.linalg.norm(c_2_partial_curve[1:] - c_2_partial_curve[:(len(c_2_partial_curve)-1)], axis=1))
        # OBJECTIVE FUNCTION
        length = 1.0 - np.exp(-min(c_1_len - c_1_partial_len, c_2_len - c_2_partial_len))
        dist = 1.0 - np.exp(-np.mean(np.linalg.norm(c_1_partial_curve - c_2_partial_curve, axis=-1)))
        weight = 0.85
        obj_func = weight*dist + (1-weight)*length

        return obj_func

    bounds = Bounds([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [10, 10, 10, 10, 10, 10, 10, 10, 1, 1, 1, 1])
    solutions = []
    solutions_fun = []
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     (cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     -(cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     -(cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     (cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)

    c_2_vecs = np.flip(c_2_vecs, axis=0)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     (cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     -(cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    ineq_cons = {"type": "ineq",
                 "fun": lambda x: np.array([
                     -(cam_pos[axis_id] + x[0]*c_1_vecs[0][axis_id]),
                     -(cam_pos[axis_id] + x[1]*c_1_vecs[1][axis_id]),
                     -(cam_pos[axis_id] + x[2]*c_1_vecs[2][axis_id]),
                     -(cam_pos[axis_id] + x[3]*c_1_vecs[3][axis_id]),
                     (cam_pos[axis_id] + x[4]*c_2_vecs[0][axis_id]),
                     (cam_pos[axis_id] + x[5]*c_2_vecs[1][axis_id]),
                     (cam_pos[axis_id] + x[6]*c_2_vecs[2][axis_id]),
                     (cam_pos[axis_id] + x[7]*c_2_vecs[3][axis_id]),
                     (x[9] - x[8]) - 0.01,
                     (x[11] - x[10]) - 0.01,
                 ])}
    res = minimize(opt_func, opt_vars, args=(c_2_vecs), method="SLSQP", bounds=bounds,
                   constraints=[ineq_cons])
    solutions.append(res.x)
    solutions_fun.append(res.fun)
    min_solution = np.argmin(solutions_fun)

    res = solutions[min_solution]
    a_1 = res[8]
    b_1 = res[9]
    a_2 = res[10]
    b_2 = res[11]

    if np.abs(a_1 - b_1) <= 0.1 or np.abs(a_2 - b_2) <= 0.1:
        print("REJECTED: intervals too close")
        return [], [], [], []
    c_2_vecs = np.flip(c_2_vecs, axis=0)
    if min_solution > 1:
        res[4:8] = np.flip(res[4:8])
        tmp = 1 - b_2
        b_2 = 1 - a_2
        a_2 = tmp
    assert a_2 <= b_2, "incorrect interval: "+str(a_2)+", "+str(b_2)+" min_solution: "+str(min_solution)+" res:"+str(res)
    # sanity check
    c_1_ctrl_pts = cam_pos + res[:4].reshape(-1, 1)*c_1_vecs
    c_2_ctrl_pts = cam_pos + res[4:8].reshape(-1, 1)*c_2_vecs
    invalid_c_1_ctrl_pts = np.all(np.isclose(c_1_ctrl_pts[0], c_1_ctrl_pts[1])) or \
                           np.all(np.isclose(c_1_ctrl_pts[1], c_1_ctrl_pts[2])) or \
                           np.all(np.isclose(c_1_ctrl_pts[2], c_1_ctrl_pts[3]))
    invalid_c_2_ctrl_pts = np.all(np.isclose(c_2_ctrl_pts[0], c_2_ctrl_pts[1])) or \
                           np.all(np.isclose(c_2_ctrl_pts[1], c_2_ctrl_pts[2])) or \
                           np.all(np.isclose(c_2_ctrl_pts[2], c_2_ctrl_pts[3]))
    if invalid_c_1_ctrl_pts or invalid_c_2_ctrl_pts:
        print("REJECTED: invalid ctrl_pts")
        return [], [], [], []
    c_1_sampled_pts = equi_resample_parallel(c_1_ctrl_pts, a_1, b_1, CURVE_SAMPLE_SIZE)
    c_2_sampled_pts = equi_resample_parallel(c_2_ctrl_pts, a_2, b_2, CURVE_SAMPLE_SIZE)
    # reflect c_1
    c_1_sampled_pts_reflected = c_1_sampled_pts.copy()
    c_1_sampled_pts_reflected[:, axis_id] *= -1
    c_1_len = tools_3d.line_3d_length(c_1_sampled_pts)
    c_2_len = tools_3d.line_3d_length(c_2_sampled_pts)
    c_1_acc_radius = 0.2*c_1_len
    c_2_acc_radius = 0.2*c_2_len
    c_1_interval_length = np.abs(res[9] - res[8])
    c_2_interval_length = np.abs(res[11] - res[10])
    dist = np.mean(np.linalg.norm(c_1_sampled_pts_reflected - c_2_sampled_pts, axis=-1))
    # official
    if not include_more_curves and min_solution > 1:
        return [], [], [], []
#    if min_solution > 1:
#        dist = np.max(np.linalg.norm(c_1_sampled_pts_reflected - np.flip(c_2_sampled_pts, axis=0), axis=-1))
#
#    # DEBUG
#    #if c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
#    #    print("REJECTED")
#    #    if dist > np.max([c_1_acc_radius, c_2_acc_radius]):
#    #        print("acc_radii: ", c_1_acc_radius, c_2_acc_radius)
#    #        print("dist: ", dist)
#    #    if c_1_interval_length < 0.1 or c_2_interval_length < 0.1:
#    #        print("interval lengths: ", c_1_interval_length, c_2_interval_length)
#        return [], [], [], []

    assert a_1 <= b_1, "incorrect interval: "+str(a_1)+", "+str(b_1)
    assert a_2 <= b_2, "incorrect interval: "+str(a_2)+", "+str(b_2)
    return c_1_sampled_pts, c_2_sampled_pts, [a_1, b_1], [a_2, b_2]

bezier_coeff_mat = np.array([[1, 0, 0, 0],
                             [-3, 3, 0, 0],
                             [3, -6, 3, 0],
                             [-1, 3, -3, 1]])
def cubic_bezier_eval_paralllel(ctrl_pts, a, b, n):
    t_coeffs = np.ones([n, 4], dtype=float)
    t_simple = np.linspace(a, b, n).transpose()
    t_coeffs[:, 1] = t_simple
    t_coeffs[:, 2] = t_simple*t_simple
    t_coeffs[:, 3] = t_simple*t_simple*t_simple
    return np.matmul(t_coeffs, np.matmul(bezier_coeff_mat, ctrl_pts))


def equi_resample_parallel(ctrl_pts, a, b, N):
    if np.isclose(np.abs(a - b), 0.0):
        return np.array([])
    pts = cubic_bezier_eval_paralllel(ctrl_pts, a, b, 100)
    dists = np.linalg.norm(pts[1:] - pts[:len(pts)-1], axis=1)
    if np.isclose(np.sum(dists), 0.0):
        return []
    chord_lengths = np.cumsum(dists)/np.sum(dists)
    arc_lengths = np.zeros(len(pts))
    arc_lengths[1:] = chord_lengths
    arc_lengths = a + (b-a)*arc_lengths
    equi_pts = np.array([pts[np.argmin(np.abs(arc_lengths - t))]
                         for t in np.linspace(a, b, N)])
    return equi_pts

def merge_segments_intervals(segments, intervals, s_id, sketch, camera):

    merged_segments = []
    merged_intervals = []
    merged_intervals_ids = []
    c_bezier = sketch.strokes[s_id].bezier
    b_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_bezier])
    for i in range(4):
        b_vecs[i] /= np.linalg.norm(b_vecs[i])
    cam_pos = np.array(camera.cam_pos)

    # find connected components of interval graph
    nb_intervals = len(intervals)
    intervals_adj_mat = np.zeros([nb_intervals, nb_intervals], dtype=np.int)
    for i in range(nb_intervals):
        for j in range(nb_intervals):
            if j <= i:
                continue
            if intervals[i][0] <= intervals[j][0] and intervals[i][1] >= intervals[j][0] or \
                intervals[j][0] <= intervals[i][0] and intervals[j][1] >= intervals[i][0]:
                intervals_adj_mat[i][j] = 1
                intervals_adj_mat[j][i] = 1
    intervals_components = list(nx.connected_components(nx.Graph(intervals_adj_mat)))

    # for each interval component find connected components of segments graph
    for interval_component in intervals_components:
        local_segments = np.array(segments)[list(interval_component)]
        local_intervals = np.array(intervals)[list(interval_component)]
        nb_local_segments = len(local_segments)
        segments_adj_mat = np.zeros([nb_local_segments, nb_local_segments], dtype=np.int)
        for i in range(nb_local_segments):
            for j in range(nb_local_segments):
                if j <= i:
                    continue
                if not(local_intervals[i][0] <= local_intervals[j][0] and local_intervals[i][1] >= local_intervals[j][0] or \
                        local_intervals[j][0] <= local_intervals[i][0] and local_intervals[j][1] >= local_intervals[i][0]):
                    continue
                overlapping_interval = [max(local_intervals[i][0], local_intervals[j][0]),
                                        min(local_intervals[i][1], local_intervals[j][1])]
                assert overlapping_interval[0] <= overlapping_interval[1], "overlapping interval incorrect: "+str(overlapping_interval)
                pt_i_a, interval_i_a = tools_3d.interpolate_segment(
                    local_segments[i], local_intervals[i], overlapping_interval[0])
                pt_i_b, interval_i_b = tools_3d.interpolate_segment(
                    local_segments[i], local_intervals[i], overlapping_interval[1])
                seg_i = [pt_i_a] + local_segments[i][interval_i_a+1:interval_i_b].tolist() + [pt_i_b]
                seg_i = np.array(seg_i)
                len_seg_i = tools_3d.line_3d_length(seg_i)
                pt_j_a, interval_j_a = tools_3d.interpolate_segment(
                    local_segments[j], local_intervals[j], overlapping_interval[0])
                pt_j_b, interval_j_b = tools_3d.interpolate_segment(
                    local_segments[j], local_intervals[j], overlapping_interval[1])
                seg_j = [pt_j_a] + local_segments[j][interval_j_a+1:interval_j_b].tolist() + [pt_j_b]
                seg_j = np.array(seg_j)
                len_seg_j = tools_3d.line_3d_length(seg_j)
                dist = max(directed_hausdorff(seg_i, seg_j)[0],
                           directed_hausdorff(seg_j, seg_i)[0])
                if dist < max(0.2*len_seg_i, 0.2*len_seg_j):
                    segments_adj_mat[i][j] = 1
                    segments_adj_mat[j][i] = 1

        local_seg_graph = nx.Graph(segments_adj_mat)
        local_segments_components = list(nx.connected_components(local_seg_graph))
        # merge clique segment components
        for local_seg_comp in list(local_segments_components):
            # check if local_seg_comp is clique
            local_seg_comp = list(local_seg_comp)
            n = len(local_seg_comp)
            local_seg_comp_graph = local_seg_graph.subgraph(local_seg_comp)
            if local_seg_comp_graph.size() != n*(n-1)/2:
                continue
            # "resample" points from segments
            resampled_pts = []
            for i in local_seg_comp:
                for t_id, t in enumerate(np.linspace(local_intervals[i][0], local_intervals[i][1], CURVE_SAMPLE_SIZE)):
                    resampled_pts.append([t, -1, local_segments[i][t_id]])
            # fit bezier to sampled points
            bezier_depths = fit_full_bezier_lstsq(resampled_pts, b_vecs, cam_pos)
            ctrl_pts_3d = cam_pos + np.array(bezier_depths).reshape(-1, 1)*b_vecs
            # resample max_interval from full bezier
            max_interval = [np.min(local_intervals[local_seg_comp][:, 0]),
                            np.max(local_intervals[local_seg_comp][:, 1])]
            new_pts = [bezier.q(ctrl_pts_3d, t)
                       for t in np.linspace(max_interval[0], max_interval[1], CURVE_SAMPLE_SIZE)]
            new_interval_ids = np.array(list(interval_component))[local_seg_comp]
            merged_segments.append(np.array(new_pts))
            merged_intervals.append(max_interval)
            merged_intervals_ids.append(new_interval_ids)

    # append all unmerged intervals as autonomous components
    used_interval_ids = [i for interval in merged_intervals_ids for i in interval]
    unused_interval_ids = [i for i in range(len(intervals)) if not i in used_interval_ids]
    for i in unused_interval_ids:
        merged_segments.append(segments[i])
        merged_intervals.append(intervals[i])
        merged_intervals_ids.append([i])

    return merged_segments, merged_intervals, merged_intervals_ids

def get_min_var_coeffs(ctrl_pts):
    a_mat = np.array([[ctrl_pts[0][0], ctrl_pts[1][0], ctrl_pts[2][0]],
                      [ctrl_pts[0][1], ctrl_pts[1][1], ctrl_pts[2][1]],
                      [ctrl_pts[0][2], ctrl_pts[1][2], ctrl_pts[2][2]],
                      [1, 1, 1]])
    b_mat = np.array([ctrl_pts[3][0], ctrl_pts[3][1], ctrl_pts[3][2], 1])
    min_var_coeffs, _, _, _ = lstsq(a_mat, b_mat)
    return min_var_coeffs

def fit_full_bezier_lstsq(comb_t_interval_id_pt, b_vecs, cam_pos):

    # compute minimal variation coefficients
    ctrl_pts = cam_pos + b_vecs
    min_var_coeffs = get_min_var_coeffs(ctrl_pts)

    a_mat = np.zeros([3*len(comb_t_interval_id_pt)+3, 4])
    b_mat = np.zeros(3*len(comb_t_interval_id_pt)+3)
    # close to sampled pts
    for id, triplet in enumerate(comb_t_interval_id_pt):
        t, interval_id, pt = triplet
        b_coeff_0 = (1-t)*(1-t)*(1-t)
        b_coeff_1 = 3*t*(1-t)*(1-t)
        b_coeff_2 = 3*t*t*(1-t)
        b_coeff_3 = t*t*t
        for i in range(3):
            a_mat[3*id+i][0] = b_coeff_0*b_vecs[0][i]
            a_mat[3*id+i][1] = b_coeff_1*b_vecs[1][i]
            a_mat[3*id+i][2] = b_coeff_2*b_vecs[2][i]
            a_mat[3*id+i][3] = b_coeff_3*b_vecs[3][i]
            b_mat[3*id+i] = pt[i] - (b_coeff_0+b_coeff_1+b_coeff_2+b_coeff_3)*cam_pos[i]
    # minimal variation
    for i in range(3):
        a_mat[-i-1][0] = 0.05*min_var_coeffs[0]*b_vecs[0][i]
        a_mat[-i-1][1] = 0.05*min_var_coeffs[1]*b_vecs[1][i]
        a_mat[-i-1][2] = 0.05*min_var_coeffs[2]*b_vecs[2][i]
        a_mat[-i-1][3] = -0.05*b_vecs[3][i]
        b_mat[-i-1] = -0.05*cam_pos[i]*(min_var_coeffs[0]+min_var_coeffs[1]+min_var_coeffs[2]) + 0.05*cam_pos[i]

    bezier_depths, _, _, _ = lstsq(a_mat, b_mat)
    return bezier_depths

def equi_resample_polyline(poly, dist):
    dists = np.linalg.norm(poly[1:] - poly[:len(poly)-1], axis=1)
    poly_len = np.sum(dists)
    if poly_len/dist > 100:
        dist = poly_len/100
    chord_lengths = np.cumsum(dists)/poly_len
    arc_lengths = np.zeros(len(poly))
    arc_lengths[1:] = chord_lengths
    equi_pts = []
    for t in np.linspace(0, 1, int(poly_len/dist)):
        if np.isclose(t, 0.0):
            equi_pts.append(poly[0])
            continue
        elif np.isclose(t, 1.0):
            equi_pts.append(poly[-1])
            continue
        closest_ids = np.argsort(np.abs(arc_lengths-t))
        min_id = np.min(closest_ids[:2])
        max_id = np.max(closest_ids[:2])
        equi_pts.append(poly[min_id] +
                        (t - arc_lengths[min_id])/(arc_lengths[max_id] - arc_lengths[min_id])*\
                        (poly[max_id] - poly[min_id]))
    return np.array(equi_pts)

def stitch_partial_curves(per_axis_per_stroke_segments,
                          per_axis_per_stroke_intervals,
                          per_axis_per_stroke_per_interval_other_stroke_ids, sketch, camera,
                          VERBOSE=False):
    per_axis_per_stroke_curves = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    per_axis_per_stroke_interval_curve_ids = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    per_axis_per_stroke_rejected_curves = [[[] for i in range(len(sketch.strokes))] for j in range(3)]
    curve_connectivity_adj_mat = np.zeros([len(sketch.strokes), len(sketch.strokes)], dtype=int)
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5 or s.is_ellipse():
            continue
        neighbour_intersections = sketch.intersection_graph.get_intersections_by_stroke_id(s_id)
        for inter in neighbour_intersections:
            other_s_id = np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s_id][0]
            if sketch.strokes[other_s_id].axis_label == 5 and not sketch.strokes[other_s_id].is_ellipse():
                curve_connectivity_adj_mat[s_id, other_s_id] = 1
                curve_connectivity_adj_mat[other_s_id, s_id] = 1
    curve_connected_components = list(nx.connected_components(nx.Graph(curve_connectivity_adj_mat)))
    per_curve_component = np.zeros(len(sketch.strokes))
    for comp_id, comp in enumerate(curve_connected_components):
        for s_id in comp:
            per_curve_component[s_id] = comp_id

    for axis_id in range(3):
        for s_id, s in enumerate(sketch.strokes):
            if s.axis_label != 5 or s.is_ellipse():
                continue
            if len(per_axis_per_stroke_intervals[axis_id][s_id]) == 0:
                continue
            if VERBOSE:
                ps.remove_all_structures()
            segments = per_axis_per_stroke_segments[axis_id][s_id]
            seg_pts = np.array([p for seg in segments for p in seg])
            bbox = tools_3d.bbox_from_points(seg_pts)
            intervals = per_axis_per_stroke_intervals[axis_id][s_id]
            # go through different interval priority queues
            all_combinations_t_interval_id_pt = []
            # find connected components of interval graph
            nb_intervals = len(intervals)
            intervals_adj_mat = np.ones([nb_intervals, nb_intervals], dtype=np.int)
            for i in range(nb_intervals):
                intervals_adj_mat[i][i] = 0
                int_i_components = [per_curve_component[other_s_id]
                                    for other_s_id in per_axis_per_stroke_per_interval_other_stroke_ids[axis_id][s_id][i]]
                for j in range(nb_intervals):
                    if j <= i:
                        continue
                    int_j_components = [per_curve_component[other_s_id]
                                        for other_s_id in per_axis_per_stroke_per_interval_other_stroke_ids[axis_id][s_id][j]]
                    if VERBOSE:
                        print("components", i, int_i_components, j, int_j_components)
                    if len(np.intersect1d(int_i_components, int_j_components)) == 0:
                        intervals_adj_mat[i, j] = 0
                        intervals_adj_mat[j, i] = 0
                        continue
                    overlapping_interval = [max(intervals[i][0], intervals[j][0]),
                                            min(intervals[i][1], intervals[j][1])]
                    if not ((intervals[i][0] < intervals[j][0] and intervals[i][1] < intervals[j][1])
                            or (intervals[j][0] < intervals[i][0] and intervals[j][1] < intervals[i][1])):
                        intervals_adj_mat[i][j] = 0
                        intervals_adj_mat[j][i] = 0
                        continue
                    if min((overlapping_interval[1] - overlapping_interval[0])/(intervals[i][1] - intervals[i][0]), \
                           (overlapping_interval[1] - overlapping_interval[0])/(intervals[j][1] - intervals[j][0])) > 0.2:
                        intervals_adj_mat[i][j] = 0
                        intervals_adj_mat[j][i] = 0
            intervals_cliques = list(nx.enumerate_all_cliques(nx.Graph(intervals_adj_mat)))
            for comb in intervals_cliques:
                comb_t_interval_id_pt = []
                for t in np.linspace(0.0, 1.0, CURVE_SAMPLE_SIZE):
                    for interval_id in comb:
                        if (t > intervals[interval_id][0] or np.isclose(t, intervals[interval_id][0])) \
                                and (t < intervals[interval_id][1] or np.isclose(t, intervals[interval_id][1])):
                            comb_t_interval_id_pt.append([t, interval_id, []])
                            break
                if len(comb_t_interval_id_pt) == 0:
                    continue
                all_combinations_t_interval_id_pt.append(comb_t_interval_id_pt)
            # find 3d points from segments
            for comb_id, comb_t_interval_id_pt in enumerate(all_combinations_t_interval_id_pt):
                pc = []
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, _ = triplet

                    pt, _ = tools_3d.interpolate_segment(segments[interval_id],
                                                      intervals[interval_id], t)
                    if len(pt) > 0:
                        all_combinations_t_interval_id_pt[comb_id][id][-1] = pt
                        pc.append(pt)

            # fit full bezier curves to sampled pts
            curves = []
            c_bezier = sketch.strokes[s_id].bezier
            b_vecs = np.array([camera.get_camera_point_ray(b_i)[1] for b_i in c_bezier])
            for i in range(4):
                b_vecs[i] /= np.linalg.norm(b_vecs[i])
            cam_pos = np.array(camera.cam_pos)

            interval_curve_ids = [[] for l in range(len(intervals))]
            for comb_id, comb_t_interval_id_pt in enumerate(all_combinations_t_interval_id_pt):
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, pt = triplet
                    interval_curve_ids[interval_id].append(len(curves))
                curve_sampled = []
                bezier_depths = fit_full_bezier_lstsq(comb_t_interval_id_pt, b_vecs, cam_pos)
                ctrl_pts_3d = cam_pos + np.array(bezier_depths).reshape(-1, 1)*b_vecs
                # sanity check
                dists = []
                for id, triplet in enumerate(comb_t_interval_id_pt):
                    t, interval_id, pt = triplet
                    dists.append(np.linalg.norm(np.array(bezier.q(ctrl_pts_3d, t)) - np.array(pt)))
                for t in np.linspace(0.0, 1.0, CURVE_SAMPLE_SIZE):
                    curve_sampled.append(bezier.q(ctrl_pts_3d, t))
                length = np.sum([np.linalg.norm(np.array(curve_sampled[i+1]) - np.array(curve_sampled[i]))
                          for i in range(9)])
                acc_radius = 0.2*length
                assert len(dists) > 0, "len(dists) == 0: no points sampled"
                if np.max(dists) > acc_radius:
                    per_axis_per_stroke_rejected_curves[axis_id][s_id].append(len(curves))
                curves.append(np.array(curve_sampled))

            for l in range(len(intervals)):
                interval_curve_ids[l] = np.unique(interval_curve_ids[l]).tolist()
            per_axis_per_stroke_curves[axis_id][s_id] = curves
            per_axis_per_stroke_interval_curve_ids[axis_id][s_id] = interval_curve_ids

    return per_axis_per_stroke_curves, per_axis_per_stroke_interval_curve_ids, per_axis_per_stroke_rejected_curves

def get_per_correspondence_sym_scores(sketch, curve_epsilons, global_candidate_correspondences):
    # account for actual point-to-point symmetry for curves
    per_correspondence_sym_scores = []
    for corr_id, corr in enumerate(global_candidate_correspondences):
        if sketch.strokes[corr[0]].axis_label != 5 or sketch.strokes[corr[0]].is_ellipse():
            per_correspondence_sym_scores.append([])
            continue
        if corr[0] == corr[1]:
            per_correspondence_sym_scores.append([])
            continue
        # equidistant resampling
        all_points = []
        point_stroke_ids = []
        for i in range(2):
            equi_seg = equi_resample_polyline(corr[2+i], curve_epsilons[corr[4]])
            for p in equi_seg:
                all_points.append(p)
                point_stroke_ids.append(i)
        all_points = np.array(all_points)
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[corr[4]] = 1.0
        refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)
        min_length = np.min([tools_3d.line_3d_length(corr[2+i]) for i in range(2)])

        symmetric_points = symmetric_point_cloud(all_points, refl_mat,
                                                                threshold=0.1*min_length,
                                                                VERBOSE=False)
        sym_scores = [[] for l in range(2)]
        for pt_id, sym_score in enumerate(symmetric_points):
            s_id = point_stroke_ids[pt_id]
            sym_scores[s_id].append(min(sym_score, 1.0))
        for s_id, scores in enumerate(sym_scores):
            sym_scores[s_id] = np.mean(scores)
        per_correspondence_sym_scores.append(sym_scores)
    return per_correspondence_sym_scores

def extract_fixed_strokes(batches):
    fixed_strokes = batches[-1]["fixed_strokes"]
    proxies = batches[-1]["final_proxies"]
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0:
            fixed_strokes[p_id] = p
    for i, s in enumerate(fixed_strokes):
        fixed_strokes[i] = np.array(s)
    return fixed_strokes