import numpy as np
import networkx as nx
import polyscope as ps
import os
import pysbm.tools_3d as tools_3d
import pysbm.symmetry_tools.common_tools as common_tools
from skspatial.objects import Plane
import matplotlib.pyplot as plt
#from descartes import PolygonPatch
from shapely.geometry import MultiPoint, LineString, Polygon, MultiLineString
from hausdorff import hausdorff_distance

def candidate_correspondences_2d_and_straight_3d(sketch, camera,
                                                 global_candidate_correspondences,
                                                 per_axis_per_stroke_candidate_reconstructions,
                                                 selected_planes, max_axes,
                                                 include_more_curves=False):
    for i in selected_planes:

        focus_vp = i
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[focus_vp] = 1.0
        refl_mat = tools_3d.get_reflection_mat(sym_plane_point, sym_plane_normal)

        for j in max_axes:
            if i == j:
                for s_id, s in enumerate(sketch.strokes):
                    if s.axis_label == 5 and s.is_ellipse():
                        if s.closest_major_axis_deviation < 15.0 and s.closest_major_axis_id == focus_vp:
                            continue
                        global_candidate_correspondences.append([s_id, s_id, [], [], focus_vp, -1, -1, -1, -1])
                        continue
                    # no self-symmetric curves!
                    if s.axis_label == 5:
                        continue
                    if s.axis_label != focus_vp:
                        continue
                    candidate_tuples, candidate_tuple_s_ids = get_self_symmetry_candidates(
                        s,
                        sketch.intersection_graph.get_intersections_by_stroke_id(s_id),
                        sketch
                    )
                    if len(candidate_tuples) == 0:
                        continue
                    # 3D candidates
                    candidate_reconstructions = np.array(
                        [tools_3d.reconstruct_two_points(p1, p2, refl_mat, camera)
                         for p1, p2 in candidate_tuples])
                    for rec_id, rec in enumerate(candidate_reconstructions):
                        rec_vec = rec[-1] - rec[0]
                        rec_vec /= np.linalg.norm(rec_vec)
                        rec = np.array(camera.lift_polyline_close_to_line([s.points_list[0].coords,
                                                                           s.points_list[-1].coords],
                                                                          rec[0], rec_vec))
                        # proxy-ids will get filled in later
                        global_candidate_correspondences.append([s_id, s_id, rec, rec, focus_vp, -1, -1,
                                                                 candidate_tuple_s_ids[rec_id][0], candidate_tuple_s_ids[rec_id][1]])
                        per_axis_per_stroke_candidate_reconstructions[focus_vp][s_id].append(rec)
            else:
                focus_axis_label = j

                tmp_candidate_correspondences = gather_symmetry_correspondences_2d(sketch, camera,
                                                       [0, 1, 2], overlap_thresh=0.4,
                                                       focus_axis_label=focus_axis_label,
                                                       focus_vp=focus_vp)
                #print("axis", i, tmp_candidate_correspondences)
                # official
                if include_more_curves:
                    tmp_candidate_correspondences_curves = gather_symmetry_correspondences_2d_curves(
                        sketch, camera, overlap_thresh=0.3,
                        focus_axis_label=focus_axis_label,
                        focus_vp=focus_vp)
                else:
                    tmp_candidate_correspondences_curves = gather_symmetry_correspondences_2d_curves_v2(
                        sketch, camera, overlap_thresh=0.3,
                        focus_axis_label=focus_axis_label,
                        focus_vp=focus_vp)
                tmp_candidate_correspondences = np.array(tmp_candidate_correspondences + \
                                                         tmp_candidate_correspondences_curves)


                for corr_id, corr in enumerate(tmp_candidate_correspondences):
                    s_1 = []
                    s_2 = []
                    add_corr = True
                    # we cannot compute curve geometry right now
                    if sketch.strokes[corr[0]].axis_label != 5:
                        s_1, s_2 = tools_3d.reconstruct_symmetric_strokes_straight(
                            sketch.strokes[corr[0]], sketch.strokes[corr[1]],
                            sym_plane_point, sym_plane_normal, camera)
                        # reproject input stroke on candidate
                        try:
                            s_1_dir_vec = s_1[-1] - s_1[0]
                            s_1_dir_vec /= np.linalg.norm(s_1_dir_vec)
                            s_1 = np.array(camera.lift_polyline_close_to_line([sketch.strokes[corr[0]].points_list[0].coords,
                                                                               sketch.strokes[corr[0]].points_list[-1].coords],
                                                                              s_1[0], s_1_dir_vec))
                            s_2_dir_vec = s_2[-1] - s_2[0]
                            s_2_dir_vec /= np.linalg.norm(s_2_dir_vec)
                            s_2 = np.array(camera.lift_polyline_close_to_line([sketch.strokes[corr[1]].points_list[0].coords,
                                                                               sketch.strokes[corr[1]].points_list[-1].coords],
                                                                              s_2[0], s_2_dir_vec))
                            length_ratio = tools_3d.line_3d_length(s_1)/tools_3d.line_3d_length(s_2)
                            if length_ratio < 0.33 or length_ratio > 1.66:
                                add_corr = False
                            else:
                                per_axis_per_stroke_candidate_reconstructions[focus_vp][corr[0]].append(s_1)
                                per_axis_per_stroke_candidate_reconstructions[focus_vp][corr[1]].append(s_2)
                        except:
                            add_corr = False
                        #continue
                    # proxy-ids will get filled in later
                    if add_corr:
                        global_candidate_correspondences.append([corr[0], corr[1], s_1, s_2, focus_vp, -1, -1, -1, -1])

    # get midline correspondences
    sym_planes = []
    for tmp_i in range(3):
        sym_plane_point = np.zeros(3, dtype=np.float)
        sym_plane_normal = np.zeros(3, dtype=np.float)
        sym_plane_normal[tmp_i] = 1.0
        sym_planes.append(Plane(sym_plane_point, sym_plane_normal))
    same_axis_stroke_ids = [[] for i in range(6)]
    for s_id, s in enumerate(sketch.strokes):
        same_axis_stroke_ids[s.axis_label].append(s_id)

    all_midline_stroke_ids = common_tools.get_midline_stroke_ids([0, 1, 2, 3],
                                                                   global_candidate_correspondences,
                                                                   per_axis_per_stroke_candidate_reconstructions,
                                                                   sym_planes, same_axis_stroke_ids,
                                                                   sketch, camera,
                                                                   VERBOSE=False)

# s is a pystroke.Stroke
# intersections is a list of pystroke.EdgeIntersection
def get_self_symmetry_candidates(s, intersections, sketch, VERBOSE=False):

    # only do combinations of intersections between intersections at endpoints
    # of stroke
    inter_params = [np.array(inter.mid_inter_param)[np.array(inter.stroke_ids) == s.id][0]
                    for inter in intersections]
    inter_other_stroke_ids = [np.array(inter.stroke_ids)[np.array(inter.stroke_ids) != s.id][0]
                              for inter in intersections]
    inter_params_middle = 0.5
    start_points = [inter.inter_coords
                    for inter_id, inter in enumerate(intersections)
                    if inter_params[inter_id] < inter_params_middle - 0.001]
    start_points_other_stroke_ids = [inter_other_stroke_ids[inter_id]
                                     for inter_id, inter in enumerate(intersections)
                                     if inter_params[inter_id] < inter_params_middle - 0.001]
    end_points = [inter.inter_coords
                  for inter_id, inter in enumerate(intersections)
                  if inter_params[inter_id] > inter_params_middle + 0.001]
    end_points_other_stroke_ids = [inter_other_stroke_ids[inter_id]
                                   for inter_id, inter in enumerate(intersections)
                                   if inter_params[inter_id] > inter_params_middle + 0.001]
    candidate_tuples = [[p_start, p_end]
                        for p_start_id, p_start in enumerate(start_points)
                        for p_end_id, p_end in enumerate(end_points)
                        if (start_points_other_stroke_ids[p_start_id] == -1 or \
                        end_points_other_stroke_ids[p_end_id] == -1 or \
                        sketch.strokes[start_points_other_stroke_ids[p_start_id]].axis_label == sketch.strokes[end_points_other_stroke_ids[p_end_id]].axis_label)
                        and np.linalg.norm(np.array(p_start) - np.array(p_end)) > s.acc_radius]
    candidate_tuple_s_ids = [[start_points_other_stroke_ids[p_start_id], end_points_other_stroke_ids[p_end_id]]
                             for p_start_id, p_start in enumerate(start_points)
                             for p_end_id, p_end in enumerate(end_points)
                             if (start_points_other_stroke_ids[p_start_id] == -1 or \
                                 end_points_other_stroke_ids[p_end_id] == -1 or \
                                 sketch.strokes[start_points_other_stroke_ids[p_start_id]].axis_label == sketch.strokes[end_points_other_stroke_ids[p_end_id]].axis_label)
                             and np.linalg.norm(np.array(p_start) - np.array(p_end)) > s.acc_radius]

    return candidate_tuples, candidate_tuple_s_ids

def gather_symmetry_correspondences_2d(sketch, camera, selected_planes,
                                       close_thresh=20.0, overlap_thresh=0.7, 
                                       focus_vp=0, focus_axis_label=1,
                                       VERBOSE=False):
    intersected_stroke_ids = []
    straight_line_overlap_thresh = overlap_thresh
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label == 5:
            continue
        else:
            overlap_thresh = straight_line_overlap_thresh
        if s.axis_label != focus_axis_label:
            continue

        s_coords = [p.coords for p in s.points_list]

        vanishing_triangles = \
            [MultiPoint(s_coords + [vp_i]).convex_hull
             for i, vp_i in enumerate(camera.vanishing_points_coords) if not i == s.axis_label and i == focus_vp]
        # extract vanishing_triangle for each convex poly
        for vanishing_triangle_id, vanishing_triangle in enumerate(vanishing_triangles):
            vp_id = -1
            for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
                found_vp = False
                for vp in camera.vanishing_points_coords:
                    if np.isclose(np.linalg.norm(p - vp), 0.0):
                        vp_id = p_id
                        found_vp = True
                        break
                if found_vp:
                    break
            triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]), vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
            # extend vanishing triangle to "infinity"
            triangle[0] += (triangle[0] - triangle[1])
            triangle[-1] += (triangle[-1] - triangle[1])
            vanishing_triangles[vanishing_triangle_id] = Polygon(triangle)

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue

            if s.axis_label < 3 and \
                    other_s.linestring.linestring.distance(s.linestring.linestring) < min(other_s.acc_radius, s.acc_radius):
                continue
            if s.axis_label > 3 and \
                    (s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/s.length() > 0.5 or
                      s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                continue

            for i, vanishing_triangle in enumerate(vanishing_triangles):

                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue
                #if VERBOSE:
                #    print("s_id", s_id, "other_s_id", other_s_id)
                #    print(other_intersecting_segment.length/sketch.strokes[other_s_id].length(), overlap_thresh)
                #    fig, ax = plt.subplots(nrows=1, ncols=1)
                #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                #                        bottom=0.0,
                #                        top=1.0)

                #    ax.plot(np.array(sketch.strokes[other_s_id].linestring.linestring)[:,0],
                #            np.array(sketch.strokes[other_s_id].linestring.linestring)[:,1],
                #            c="blue", lw=3)
                #    ax.plot(np.array(other_intersecting_segment)[:,0],
                #            np.array(other_intersecting_segment)[:,1],
                #            c="green")
                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "#0000007D",
                #                             linewidth_data=lambda s: 2.0)
                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "red",
                #                             display_strokes=[s_id, other_s_id],
                #                             linewidth_data=lambda s: 3.0)
                #    poly_patch = PolygonPatch(vanishing_triangle, fc='#6699cc',
                #                              alpha=0.5)
                #    ax.add_patch(poly_patch)

                #    ax.set_xlim(0, sketch.width)
                #    ax.set_ylim(sketch.height, 0)
                #    ax.axis("equal")
                #    ax.axis("off")
                #    plt.show()
                if other_intersecting_segment.length/sketch.strokes[other_s_id].length() > overlap_thresh:
                    intersected_stroke_ids.append([s_id, other_s_id])
    return intersected_stroke_ids

def get_vanishing_triangle(s, vp, epsilon=0.0):
    s_coords = np.array(s.linestring.linestring.coords).tolist()
    vanishing_triangle = MultiPoint(s_coords + [vp]).convex_hull
    for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
        if np.isclose(np.linalg.norm(p - vp), 0.0):
            vp_id = p_id
            break
    outer_1 = np.array(vanishing_triangle.exterior.coords)[:-1][(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]
    outer_2 = np.array(vanishing_triangle.exterior.coords)[:-1][(vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]
    outer_vec = outer_1 - outer_2
    outer_1 += epsilon*outer_vec/2
    outer_2 -= epsilon*outer_vec/2
    triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]),
                                                                  vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
    triangle = np.array([outer_1, vp, outer_2])
    # extend vanishing triangle to "infinity"
    triangle[0] += (triangle[0] - triangle[1])
    triangle[-1] += (triangle[-1] - triangle[1])
    vanishing_triangle = Polygon(triangle)
    return vanishing_triangle

def gather_symmetry_correspondences_2d_curves_v2(sketch, camera, close_thresh=20.0,
                                                 overlap_thresh=0.1, focus_vp=0,
                                                 focus_axis_label=1, VERBOSE=False):
    intersected_stroke_ids = []
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5:
            continue
        if s.axis_label != focus_axis_label:
            continue
        if s.is_ellipse() and s.closest_major_axis_deviation < 15.0 and \
                s.closest_major_axis_id != focus_vp:
            continue

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue
            elif s.axis_label == 5 and s.is_ellipse() and other_s.is_ellipse():
                if np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(s.minor_axis, other_s.minor_axis))))) > 30.0:
                    continue

            if (s.linestring.linestring.buffer(s.acc_radius).intersection(
                    other_s.linestring.linestring).length/s.length() > 0.5 or
                    s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                continue

            # check what axis suits this stroke pair best
            axis_check = []
            axis_ids = []
            axis_overlaps = []
            for axis_id in range(3):
                vanishing_triangle = get_vanishing_triangle(
                    s, np.array(camera.vanishing_points_coords[axis_id]),
                    epsilon=1.5)
                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue

                if s.is_ellipse() and other_s.is_ellipse():
                    if not s.linestring.linestring.convex_hull.intersects(other_s.linestring.linestring.convex_hull):
                        intersected_stroke_ids.append([s_id, other_s_id])
                        # can only arrive here if focus_vp == closest_minor_axis_id
                        break
                else:
                    intersect = common_tools.curve_symmetry_candidate(s, other_s, vanishing_triangle,
                                                         focus_vp, sketch, camera,
                                                         dist_threshold=0.15, VERBOSE=False)
                    if intersect:
                        #intersected_stroke_ids.append([s_id, other_s_id])
                        #break
                        axis_check.append(True)
                        axis_ids.append(axis_id)
                        tight_vanishing_triangle = get_vanishing_triangle(
                            s, np.array(camera.vanishing_points_coords[axis_id]),
                            epsilon=0.0)
                        axis_overlaps.append(tight_vanishing_triangle.intersection(
                            other_s.linestring.linestring).length)
            if len(axis_overlaps) > 0 and axis_ids[np.argmax(axis_overlaps)] == focus_vp \
                    and axis_check[np.argmax(axis_overlaps)]:
                intersected_stroke_ids.append([s_id, other_s_id])
    return intersected_stroke_ids

def gather_symmetry_correspondences_2d_curves(sketch, camera, close_thresh=20.0,
                                              overlap_thresh=0.1, focus_vp=0,
                                              focus_axis_label=1, VERBOSE=False):
    intersected_stroke_ids = []
    sketch_inter_bbox = MultiPoint([inter.inter_coords
                                    for inter in sketch.intersection_graph.get_intersections()
                                    if not inter.is_extended]).bounds
    sketch_inter_diag = np.linalg.norm(np.array(sketch_inter_bbox[:2]) -
                                       np.array(sketch_inter_bbox[2:]))
    for s_id, s in enumerate(sketch.strokes):
        if s.axis_label != 5:
            continue
        if s.axis_label != focus_axis_label:
            continue

        s_coords = [p.coords for p in s.points_list]

        vanishing_triangles = \
            [MultiPoint(s_coords + [vp_i]).convex_hull
             for i, vp_i in enumerate(camera.vanishing_points_coords)]
        # extract vanishing_triangle for each convex poly
        for vanishing_triangle_id, vanishing_triangle in enumerate(vanishing_triangles):
            vp_id = -1
            for p_id, p in enumerate(np.array(vanishing_triangle.exterior.coords)):
                found_vp = False
                for vp in camera.vanishing_points_coords:
                    if np.isclose(np.linalg.norm(p - vp), 0.0):
                        vp_id = p_id
                        found_vp = True
                        break
                if found_vp:
                    break
            triangle = np.array(vanishing_triangle.exterior.coords)[:-1][[(vp_id-1)%len(np.array(vanishing_triangle.exterior.coords)[:-1]), vp_id, (vp_id+1)%len(np.array(vanishing_triangle.exterior.coords)[:-1])]]
            # extend vanishing triangle to "infinity"
            triangle[0] += (triangle[0] - triangle[1])
            triangle[-1] += (triangle[-1] - triangle[1])
            vanishing_triangles[vanishing_triangle_id] = Polygon(triangle)

        # go through all lines substantially intersecting this triangle
        for other_s_id in range(len(sketch.strokes)):
            if other_s_id >= s_id:
                continue
            # to avoid oversketching issues, keep track of strokes that
            # already processed strokes
            other_s = sketch.strokes[other_s_id]
            if other_s.axis_label != s.axis_label:
                continue
            if s.axis_label == 5 and (s.is_ellipse() != other_s.is_ellipse()):
                continue
            elif s.axis_label == 5 and s.is_ellipse() and other_s.is_ellipse():
                if np.rad2deg(np.arccos(min(1.0, np.abs(np.dot(s.minor_axis, other_s.minor_axis))))) > 30.0:
                    continue

            if (s.linestring.linestring.buffer(s.acc_radius).intersection(
                    other_s.linestring.linestring).length/s.length() > 0.5 or
                    s.linestring.linestring.buffer(s.acc_radius).intersection(
                        other_s.linestring.linestring).length/other_s.length() > 0.5):
                continue

            overlaps = []
            axis_ids = []
            hausdorff_distances = []
            #if s_id == 17 and other_s_id == 16:
            #    print("first")
            for i, vanishing_triangle in enumerate(vanishing_triangles):

                if s.is_ellipse() and s.closest_major_axis_deviation < 15.0 and \
                        s.closest_major_axis_id != i:
                    continue
                if not vanishing_triangle.intersects(sketch.strokes[other_s_id].linestring.linestring):
                    #if s_id == 17 and other_s_id == 16:
                    #    print("not intersecting", i)
                    continue
                other_intersecting_segment = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if not (type(other_intersecting_segment) == LineString or type(other_intersecting_segment) == MultiLineString):
                    continue
                if type(other_intersecting_segment) == MultiLineString:
                    pts = [p for l in other_intersecting_segment.geoms for p in l.coords]
                else:
                    pts = [p for p in other_intersecting_segment.coords]
                #print(np.array(pts))
                #print(np.array(sketch.strokes[s_id].linestring.linestring.coords))
                hd = hausdorff_distance(np.array(pts),
                                        np.array(sketch.strokes[s_id].linestring.linestring.coords))
                if s.is_ellipse() and other_s.is_ellipse():
                    if s.linestring.linestring.convex_hull.intersects(other_s.linestring.linestring.convex_hull):
                        continue
                    overlaps.append(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                    hausdorff_distances.append(hd)
                    axis_ids.append(i)
                    continue
                if VERBOSE:
                    print("hd", hd, 0.05*sketch.strokes[s_id].length())
                #if s_id == 17 and other_s_id == 16:
                #    print("second", i)
                s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                other_s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                if type(s_vanishing_intersection) is MultiLineString:
                    s_vanishing_pts = np.concatenate([np.array(l.coords) for l in s_vanishing_intersection.geoms])
                elif type(s_vanishing_intersection) is LineString:
                    s_vanishing_pts = np.array(s_vanishing_intersection.coords)
                else:
                    continue
                if type(other_s_vanishing_intersection) is MultiLineString:
                    other_s_vanishing_pts = np.concatenate([np.array(l.coords) for l in other_s_vanishing_intersection.geoms])
                elif type(other_s_vanishing_intersection) is LineString:
                    other_s_vanishing_pts = np.array(other_s_vanishing_intersection.coords)
                else:
                    continue

                overlaps.append(other_intersecting_segment.length/sketch.strokes[other_s_id].length())
                hausdorff_distances.append(hd)
                axis_ids.append(i)

                #if VERBOSE:# and s_id == 65 and s.axis_label == 5:
                #    s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                #    other_s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[other_s_id].linestring.linestring)
                #    if type(s_vanishing_intersection) is MultiLineString:
                #        s_vanishing_pts = np.concatenate([np.array(l) for l in s_vanishing_intersection])
                #    else:
                #        s_vanishing_pts = np.array(s_vanishing_intersection)
                #    if type(other_s_vanishing_intersection) is MultiLineString:
                #        other_s_vanishing_pts = np.concatenate([np.array(l) for l in other_s_vanishing_intersection])
                #    else:
                #        other_s_vanishing_pts = np.array(other_s_vanishing_intersection)
                #    print(s_vanishing_pts, other_s_vanishing_pts)
                #    initial_T = np.identity(4) # Initial transformation for ICP
                #    source = np.zeros([len(s_vanishing_pts), 3])
                #    source[:, :2] = s_vanishing_pts
                #    print(source.shape)
                #    target = np.zeros([len(other_s_vanishing_pts), 3])
                #    target[:, :2] = other_s_vanishing_pts
                #    source_pc = o3d.geometry.PointCloud()
                #    source_pc.points = o3d.utility.Vector3dVector(source)
                #    target_pc = o3d.geometry.PointCloud()
                #    target_pc.points = o3d.utility.Vector3dVector(target)

                #    distance = 1000.0 # The threshold distance used for searching correspondences
                #    icp_type = o3d.pipelines.registration.TransformationEstimationPointToPoint(with_scaling=False)
                #    iterations = o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration = 10000)
                #    result = o3d.pipelines.registration.registration_icp(source_pc, target_pc, distance, initial_T, icp_type, iterations)
                #    print(result)
                #    print(result.transformation)
                #    trans = np.asarray(deepcopy(result.transformation))
                #    source_pc.transform(trans)
                #    scale_x = np.linalg.norm(trans[:, 0])
                #    scale_y = np.linalg.norm(trans[:, 1])
                #    scale_z = np.linalg.norm(trans[:, 2])
                #    scale_z = trans[2, 2]
                #    reflection = scale_z < 0
                #    rot_mat = deepcopy(trans)
                #    print(scale_x, scale_y, scale_z)
                #    rot_mat[:, 0] /= scale_z
                #    rot_mat[:, 1] /= scale_z
                #    rot_mat[:, 2] /= scale_z
                #    print("rot_mat", rot_mat)
                #    print(np.rad2deg(np.arccos(rot_mat[0, 0])))
                #    print(np.rad2deg(np.arcsin(rot_mat[1, 0])))
                #    angle = np.rad2deg(np.arccos(rot_mat[0, 0]))
                #    print("reflection", reflection)

                #    rot_angle = 270*int(reflection) + angle
                #    print("rot_angle", rot_angle)
                #    reject = True
                #    if abs(rot_angle) < 20.0 or abs(abs(rot_angle) - 360) < 20.0 or abs(abs(rot_angle) - 180.0) < 20.0:
                #        reject = False
                #    if abs(scale_z) < 0.2:
                #        reject = True

                #    print("overlaps", overlaps)
                #    print("hausdorff_distances", hausdorff_distance(s_vanishing_pts,
                #                          other_s_vanishing_pts), 0.30*sketch.strokes[s_id].length(), s.acc_radius)
                #    if hausdorff_distance(np.array(pts),
                #                          np.array(sketch.strokes[s_id].linestring.linestring)) < 0.3*sketch.strokes[s_id].length():

                #        reject = True
                #    print("REJECT", reject)
                #    print(s_id, other_s_id)
                #    plt.rcParams["figure.figsize"] = (10, 10)
                #    fig, ax = plt.subplots(nrows=1, ncols=1)
                #    fig.subplots_adjust(wspace=0.0, hspace=0.0, left=0.0, right=1.0,
                #                        bottom=0.0,
                #                        top=1.0)

                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "#0000007D",
                #                             linewidth_data=lambda s: 2.0)
                #    sketch.display_strokes_2(fig=fig, ax=ax, norm_global=True,
                #                             color_process=lambda s: "red",
                #                             display_strokes=[s_id, other_s_id],
                #                             linewidth_data=lambda s: 3.0)
                #    poly_patch = PolygonPatch(vanishing_triangle, fc='#6699cc',
                #                              alpha=0.5)
                #    transformed_source_pts = np.asarray(source_pc.points)
                #    #print(transformed_source_pts)
                #    plt.plot(transformed_source_pts[:, 0], transformed_source_pts[:, 1], c="green", lw=3)
                #    s_vanishing_intersection = vanishing_triangle.intersection(sketch.strokes[s_id].linestring.linestring)
                #    if type(s_vanishing_intersection) is MultiLineString:
                #        s_vanishing_pts = np.concatenate([np.array(l) for l in s_vanishing_intersection])
                #    else:
                #        s_vanishing_pts = np.array(s_vanishing_intersection)
                #    #print(s_vanishing_intersection)
                #    #plt.plot(s_vanishing_pts[:, 0],
                #    #         s_vanishing_pts[:, 1])
                #    ax.add_patch(poly_patch)

                #    ax.set_xlim(0, sketch.width)
                #    ax.set_ylim(sketch.height, 0)
                #    ax.axis("equal")
                #    ax.axis("off")
                #    plt.show()
            if len(overlaps) == 0:
                continue
            if len(overlaps) == 1 and axis_ids[0] != focus_vp:
                continue
            if len(overlaps) == 1 and axis_ids[0] == focus_vp and overlaps[0] > overlap_thresh:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
                continue
            sorted_overlap_ids = np.argsort(overlaps)
            #if axis_ids[sorted_overlap_ids[-1]] != focus_vp:
            #    continue
            if VERBOSE:
                print(overlaps[sorted_overlap_ids[-1]])
            if overlaps[sorted_overlap_ids[-1]] < overlap_thresh:
                continue
            if np.isclose(overlaps[sorted_overlap_ids[-2]], 0.0) and \
                    axis_ids[sorted_overlap_ids[-1]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
                continue
            confidence = overlaps[sorted_overlap_ids[-1]]/overlaps[sorted_overlap_ids[-2]]
            if VERBOSE:
                print("confidence", confidence)
            if confidence < 1.33 and overlaps[sorted_overlap_ids[-2]] > overlap_thresh and \
                    axis_ids[sorted_overlap_ids[-2]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
            if overlaps[sorted_overlap_ids[-1]] > overlap_thresh and \
                    axis_ids[sorted_overlap_ids[-1]] == focus_vp:
                intersected_stroke_ids.append([s_id, other_s_id])
                if VERBOSE:
                    print("added ", s_id, other_s_id, "along axis ", focus_vp)
            #    exit()

    return intersected_stroke_ids