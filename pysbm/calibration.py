from pylowstroke.sketch_io import SketchSerializer as skio
import os, inspect, json
import pysbm.camera as camera
from pysbm.sketching import scale_center_sketch, compute_accuracy_radius


# this function calls the matlab scripts from the Lifting3D project
def calibrate_camera(sketch, recalibrate=True):

    current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    data_folder = os.path.join(current_dir, "deps", "sketches_json_first_viewpoint", "designer_tmp", "obj_tmp")
    #print(data_folder)

    # DEBUG
    if recalibrate:
        # prep matlab data
        sketch_file_name = os.path.join(data_folder, "view1_concept.json")
        #skio.save(sketch, sketch_file_name)
        os.system("cp "+os.path.join(sketch.sketch_folder, "view1_concept.json")+ " "+sketch_file_name)
        sketch_file_name = os.path.join(data_folder, "view1_concept.png")
        skio.save(sketch, sketch_file_name)

        os.system("rm -rf "+current_dir+"/deps/StraightStrokesMatlab/indices_set.txt")
        os.system("rm -rf "+current_dir+"/deps/StraightStrokesMatlab/cam_param.txt")
        os.system("rm -rf "+current_dir+"/deps/StraightStrokesMatlab/vps.txt")
        os.system("rm -rf "+os.path.join(current_dir, "deps", "StraightStrokesMatlab",
            "folder_save", "designer_tmp", "obj_tmp", "view1"))
        # call matlab script
        matlab_code_path = current_dir+"/deps/StraightStrokesMatlab"
        os.system("/Applications/MATLAB_R2021a.app/bin/matlab -nodisplay -r \"cd "
            +matlab_code_path+"; addpath(genpath('"+matlab_code_path+"')); obj_name = \\\"obj_tmp\\\"; run_all_sketches(obj_name);exit();\"")
    # UNDEBUG

    # extract calibration data
    cam_data_file_name = os.path.join(sketch.sketch_folder, "cam_data.json")
    os.system("cp "+current_dir+"/deps/StraightStrokesMatlab/folder_save/designer_tmp/obj_tmp/view1/designer_tmp_obj_tmp_confident_full.json" +
    " "+cam_data_file_name)
    os.system("cp "+current_dir+"/deps/StraightStrokesMatlab/indices_set.txt" +
    " "+os.path.join(sketch.sketch_folder, "matlab_indices.txt"))
    print("cp "+current_dir+"/deps/StraightStrokesMatlab/vps.txt" +
    " "+os.path.join(sketch.sketch_folder, "matlab_vps.txt"))
    os.system("cp "+current_dir+"/deps/StraightStrokesMatlab/vps.txt" +
    " "+os.path.join(sketch.sketch_folder, "matlab_vps.txt"))
    os.system("cp "+current_dir+"/deps/StraightStrokesMatlab/cam_param.txt" +
    " "+os.path.join(sketch.sketch_folder, "matlab_cam_param.txt"))
    fh = open(cam_data_file_name)
    file_content = json.load(fh)
    fh.close()
#    cam = camera.Camera(proj_mat=file_content["cam_param"]["P"],
#                 focal_dist=file_content["cam_param"]["f"],
#                 fov=file_content["cam_param"]["fov"],
#                 t=file_content["cam_param"]["t"],
#                 view_dir=file_content["cam_param"]["view_dir"],
#                 principal_point=file_content["cam_param"]["principal_point"],
#                 rot_mat=file_content["cam_param"]["R"],
#                 K=file_content["cam_param"]["K"],
#                 cam_pos=file_content["cam_param"]["C"],
#                 vanishing_points_coords=file_content["cam_param"]["vp_coord"])
#    cam.compute_inverse_matrices()
#    # assign axis labels
#    for i, s in enumerate(file_content["strokes_topology"]):
#        sketch.strokes[s["original_id"]-1].axis_label = s["line_group"] - 1
#        #sketch.strokes[s["original_id"]-1].acc_radius = s["accuracy_radius"]
#    # lifting3d scales and centers the sketch. we have to do the same
#    scale_center_sketch(sketch)
#    compute_accuracy_radius(sketch)

    #return cam
    return None