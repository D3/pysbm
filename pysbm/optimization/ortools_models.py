from time import process_time, time
import os
from copy import deepcopy
from hausdorff import hausdorff_distance
from scipy.spatial.distance import directed_hausdorff
from ortools.linear_solver import pywraplp

import numpy as np
import sys

def key_check(tmp_key, tmp_indices, tmp_values):
    return np.all([tmp_key[i] == tmp_values[val_i] for val_i, i in enumerate(tmp_indices)])

# we suppose that if an element of tmp_tuple is a str, it's a "*"
def star_selector_dict(tmp_dict, tmp_tuple):
    indices = [i for i, v in enumerate(tmp_tuple) if str(v) != "*"]
    values = [v for i, v in enumerate(tmp_tuple) if str(v) != "*"]
    items = [tmp_dict[key] for key in tmp_dict.keys()
        if key_check(key, indices, values)]
    #if len(items) == 0:
    #    return 0
    return items

def star_selector(array_tuple, tmp_tuple):
    indices = np.array([i for i, v in enumerate(tmp_tuple) if str(v) != "*"])
    values = np.array([v for i, v in enumerate(tmp_tuple) if str(v) != "*"])
    return array_tuple[1][np.all(array_tuple[0][:,indices] == values, axis=1).reshape(-1)]

class Correspondence:

    def __init__(self, stroke_3d, own_stroke_id, own_candidate_id,
                 partner_stroke_id, partner_candidate_id, plane_id, proxy_ids,
                 first_inter_stroke_id=-1, snd_inter_stroke_id=-1,
                 corr_id=-1, proxy_distances=-1):
        self.stroke_3d = stroke_3d
        self.own_stroke_id = own_stroke_id
        self.own_candidate_id = own_candidate_id
        self.partner_stroke_id = partner_stroke_id
        self.partner_candidate_id = partner_candidate_id
        self.plane_id = plane_id
        self.proxy_ids = proxy_ids
        # if self-symmetric, note first and second inter-stroke-ids
        self.first_inter_stroke_id = first_inter_stroke_id
        self.snd_inter_stroke_id = snd_inter_stroke_id
        self.corr_id = corr_id
        self.proxy_distances = proxy_distances

    def get_own_var_name(self):
        return str(self.own_stroke_id) + "_" + str(self.plane_id) + "_" + str(self.own_candidate_id)

    def get_partner_var_name(self):
        return str(self.partner_stroke_id) + "_" + str(self.plane_id) + "_" + str(self.partner_candidate_id)

def solve_symm_bip_ortools(correspondences, proxy_strokes, intersections_3d,
                                          line_coverages, per_correspondence_sym_scores,
                                          batch, fixed_strokes, fixed_intersections, sketch,
                                          sketch_connectivity=False, return_final_strokes=False,
                                          line_coverage=False, eccentricity_weights=[],
                                          stroke_lengths=[], intersection_constraint=True,
                                          best_obj_value=-1.0, ref_correspondences=[],
                                          enforce_strokes=[],
                                          suppress_correspondences=[],
                                          sketch_compacity=False, camera=None,
                                          per_axis_median_lengths=[],
                                          end_intersections=True,
                                          per_stroke_triple_intersections=[],
                                          main_axis=-1,
                                          ABLATE_LINE_COVERAGE=False,
                                          ABLATE_PROXIMITY=False,
                                          ABLATE_CURVE_CORR=False,
                                          ABLATE_ECCENTRICITY=False,
                                          ABLATE_TANGENTIAL_INTERSECTIONS=False,
                                          ABLATE_ANCHORING=False
                                          ):
    #print("correspondences", len(correspondences))
    # this structure keeps correspondence information consistent
    # in particular, we know for the k-th candidate stroke of any stroke, which
    # stroke and candidate stroke is the partner stroke, forming the
    # symmetry correspondence
    solve_time = time()
    nb_strokes = batch[1]+1
    #print("nb_strokes", nb_strokes)
    #print([corr[4] for corr in correspondences])
    nb_planes = np.max(np.array([corr[4] for corr in correspondences])) + 1
    #	print(nb_strokes)
    #print("nb_planes", nb_planes)

    check_time = process_time()
    per_stroke_per_plane_correspondences = [
        [[] for j in range(nb_planes)]
        for i in range(nb_strokes)]

    for corr_id, tmp_corr in enumerate(correspondences):
        first_stroke_id = tmp_corr[0]
        snd_stroke_id = tmp_corr[1]
        plane_id = tmp_corr[4]
        first_candidate_id = len(per_stroke_per_plane_correspondences[first_stroke_id][plane_id])
        snd_candidate_id = len(per_stroke_per_plane_correspondences[snd_stroke_id][plane_id])
        first_stroke_3d = tmp_corr[2]
        snd_stroke_3d = tmp_corr[3]
        first_proxy_ids = tmp_corr[5]
        snd_proxy_ids = tmp_corr[6]
        #first_proxy_id = 0
        #snd_proxy_id = 0
        first_inter_stroke_id = tmp_corr[7]
        snd_inter_stroke_id = tmp_corr[8]
        first_proxy_distances = []
        if type(first_proxy_ids) == int:
            continue
        for p_id in first_proxy_ids:
            h_d = max(directed_hausdorff(np.array(first_stroke_3d), np.array(proxy_strokes[first_stroke_id][p_id]))[0],
                       directed_hausdorff(np.array(proxy_strokes[first_stroke_id][p_id]), np.array(first_stroke_3d))[0])
            first_proxy_distances.append(h_d)

        #per_stroke_per_plane_correspondences[first_stroke_id][plane_id].append(
        #    Correspondence(first_stroke_3d, first_stroke_id, first_candidate_id,
        #                   snd_stroke_id, snd_candidate_id, plane_id, first_proxy_ids,
        #                   first_inter_stroke_id, snd_inter_stroke_id, corr_id,
        #                   deepcopy(first_proxy_distances)))
        per_stroke_per_plane_correspondences[first_stroke_id][plane_id].append(
            Correspondence(stroke_3d=first_stroke_3d, own_stroke_id=first_stroke_id,
                           own_candidate_id=first_candidate_id, partner_stroke_id=snd_stroke_id,
                           partner_candidate_id=snd_candidate_id, plane_id=plane_id,
                           proxy_ids=first_proxy_ids, first_inter_stroke_id=first_inter_stroke_id,
                           snd_inter_stroke_id=snd_inter_stroke_id, corr_id=corr_id,
                           proxy_distances=deepcopy(first_proxy_distances)))
        if first_stroke_id != snd_stroke_id:
            snd_proxy_distances = []
            for p_id in snd_proxy_ids:
                h_d = max(directed_hausdorff(np.array(snd_stroke_3d), np.array(proxy_strokes[snd_stroke_id][p_id]))[0],
                          directed_hausdorff(np.array(proxy_strokes[snd_stroke_id][p_id]), np.array(snd_stroke_3d))[0])
                snd_proxy_distances.append(h_d)
            per_stroke_per_plane_correspondences[snd_stroke_id][plane_id].append(
                Correspondence(stroke_3d=snd_stroke_3d, own_stroke_id=snd_stroke_id, own_candidate_id=snd_candidate_id,
                               partner_stroke_id=first_stroke_id, partner_candidate_id=first_candidate_id, plane_id=plane_id,
                               proxy_ids=snd_proxy_ids, corr_id=corr_id, proxy_distances=deepcopy(snd_proxy_distances)))
                #Correspondence(stroke_3d=snd_stroke_3d, snd_stroke_id, snd_candidate_id,
                #               first_stroke_id, first_candidate_id, plane_id, snd_proxy_ids, corr_id,
                #               deepcopy(snd_proxy_distances)))

    #print("check_time", time()-check_time)
    check_time = time()
    # start by defining the integer problem
    #symm_prob = cp_model.CpModel()
    symm_prob = pywraplp.Solver.CreateSolver('SAT_INTEGER_PROGRAMMING')
    if not symm_prob:
        print("The OR-Tools solver could not be created. Check your installation")
        return
    symm_prob.SuppressOutput()

    # we create the stroke variables, proxy variables and correspondence variables
    # stroke-by-stroke
    # structure of stroke-variables: ["s_i", ["p_j", ["c_ilkj", w_ilkj] ] ]
    # stroke_variables is a nested list, representing the integer program graph
    stroke_indices = [s_id for s_id in range(nb_strokes)
                      if len(fixed_strokes[s_id]) == 0 and s_id <= batch[1]]
    #stroke_variables = symm_prob.addVars(stroke_indices, vtype=GRB.BINARY, name="stroke_variables")
    #stroke_variables = dict([(i, symm_prob.IntVar(0,1,str(i))) for i in stroke_indices])
    #stroke_variables = np.array([symm_prob.IntVar(0,1,str(i)) for i in stroke_indices])
    stroke_variables_array = [np.array(stroke_indices).reshape(-1,1), np.array([symm_prob.IntVar(0,1,str(i)) for i in stroke_indices])]
    stroke_variables_indices_dict = dict([(v, i) for i,v in enumerate(stroke_indices)])
    #print(stroke_variables)
    # only count symmetry correspondences if they exceed 1 correspondence per stroke
    symm_stroke_variables = dict([(i, symm_prob.IntVar(0,1,str(i))) for i in stroke_indices])
    per_stroke_plane_indices = [(s_id, l) for l in range(nb_planes)
                                for s_id in range(nb_strokes)
                                if len(fixed_strokes[s_id]) == 0 and s_id <= batch[1]]
    #print(per_stroke_plane_indices)
    #per_stroke_plane_variables = symm_prob.addVars(per_stroke_plane_indices, vtype=GRB.BINARY, name="per_stroke_plane_variables")
    #per_stroke_plane_variables = dict([(i, symm_prob.IntVar(0,1,str(i))) for i in per_stroke_plane_indices])
    #per_stroke_plane_variables = [symm_prob.IntVar(0,1,str(i)) for i in per_stroke_plane_indices]
    per_stroke_plane_variables_array = [np.array(per_stroke_plane_indices), np.array([symm_prob.IntVar(0,1,str(i)) for i in per_stroke_plane_indices])]
    per_stroke_plane_indices_dict = dict([(v, i) for i,v in enumerate(per_stroke_plane_indices)])
    #non_self_symmetric_per_stroke_per_plane_variables = symm_prob.addVars(per_stroke_plane_indices, vtype=GRB.BINARY, name="non_symmetric_per_stroke_plane_variables")
    proxy_indices = [(s_id, p_id)
                        for s_id in range(nb_strokes)
                        for p_id in range(len(proxy_strokes[s_id]))
                        if len(fixed_strokes[s_id]) == 0]
    #proxy_variables = symm_prob.addVars(proxy_indices, vtype=GRB.BINARY, name="proxy_variables")
    #proxy_variables = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in proxy_indices])
    proxy_variables_array = [np.array(proxy_indices), np.array([symm_prob.IntVar(0,1,str(i)) for i in proxy_indices])]
    proxy_indices_dict = dict([(v, i) for i,v in enumerate(proxy_indices)])
    #proxy_variables_array = np.array([symm_prob.IntVar(0,1,str(i)) for i in proxy_indices])
    #proxies_per_stroke = [(s_id, len(proxy_strokes[s_id])) for s_id in range(nb_strokes)]
    #print("proxy_indices")
    #print(proxy_indices)
    #print("proxies_per_stroke")
    #print(proxies_per_stroke)
    #print("proxy_vars", len(proxy_indices))
    correspondence_indices = []
    nb_corr_vars = 0
    nb_corr_links = 0
    cluster_proximity_weights = {}
    for s_id in range(0, batch[1]+1):
        if len(fixed_strokes[s_id]) > 0:
            continue
        for l in range(nb_planes):
            for corr in per_stroke_per_plane_correspondences[s_id][l]:
                nb_corr_vars += 1
                for vec_id, p_id in enumerate(corr.proxy_ids):
                    correspondence_indices.append((s_id, p_id, corr.own_candidate_id,
                                                   corr.partner_stroke_id, corr.partner_candidate_id, l,
                                                   corr.corr_id))
                    #print(corr.proxy_distances)
                    cluster_proximity_weights[correspondence_indices[-1]] = corr.proxy_distances[vec_id]
                    nb_corr_links += 1
    #print(cluster_proximity_weights)
    #print("corr_vars", correspondence_indices)
    #correspondence_variables = symm_prob.addVars(correspondence_indices, vtype=GRB.BINARY, name="correspondence_variables")
    #correspondence_variables = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in correspondence_indices])
    correspondence_variables_array = [np.array(correspondence_indices), 
            np.array([symm_prob.IntVar(0,1,str(i)) for i in correspondence_indices])]
    #print("corr_vars", nb_corr_vars)
    #print("corr_links", nb_corr_links)
    #print("check_time", time()-check_time)
    check_time = time()

    # Intersection variables
    if sketch_connectivity:
        intersection_indices = []
        parallel_intersection_indices = []
        already_used = set()
        for inter in intersections_3d:
            inter_name = "i_" + str(inter.stroke_ids[0]) + "_" + str(inter.stroke_ids[1]) + "_" + str(inter.inter_id)
            if inter_name in already_used:
                continue
            already_used.add(inter_name)
            intersection_indices.append((inter.stroke_ids[0], inter.stroke_ids[1], inter.inter_id))
            if inter.is_parallel:
                parallel_intersection_indices.append((inter.stroke_ids[0], inter.stroke_ids[1], inter.inter_id))
        #intersection_variables = symm_prob.addVars(intersection_indices, vtype=GRB.BINARY, name="intersection_variables")
        #intersection_variables = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in intersection_indices])
        intersection_variables_array = [np.array(intersection_indices),np.array([symm_prob.IntVar(0,1,str(i)) for i in intersection_indices])]
        intersection_indices_dict = dict([(v, i) for i,v in enumerate(intersection_indices)])
        #intersection_threshold_variables = symm_prob.addVars(intersection_indices, vtype=GRB.BINARY, name="intersection_threshold_variables")

        #print("parallel_intersection_indices")
        #print(parallel_intersection_indices)
        # parallel intersections
        #parallel_intersection_variables = symm_prob.addVars(parallel_intersection_indices, vtype=GRB.BINARY, name="parallel_intersection_variables")
        #parallel_intersection_variables = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in parallel_intersection_indices])
        parallel_intersection_variables_array = [np.array(parallel_intersection_indices),np.array([symm_prob.IntVar(0,1,str(i)) for i in parallel_intersection_indices])]
        parallel_intersection_indices_dict = dict([(v, i) for i,v in enumerate(parallel_intersection_indices)])

        #print("len(intersection_indices)")
        #print("len(intersection_indices", len(intersection_indices))
    #print("check_time", time()-check_time)
    check_time = time()
    #symm_prob.update()
    #print("check_time", time()-check_time)
    check_time = time()
    #print("nb_variables", len(symm_prob.getVars()), "nb_constraints", len(symm_prob.getConstrs()))
    # Line coverage variables
    nb_line_cov = 0
    if line_coverage:
        #print("len(line_coverages)")
        #print(len(line_coverages))
        #print(line_coverages)
        all_line_coverage_variables = []
        structured_line_coverage_variables = []
        structured_line_coverage_variables_weights = []
        for s_i in range(len(line_coverages)):
            vars_indices = []
            min_vars_weights = {}
            max_vars_weights = {}
            if len(eccentricity_weights[s_i]) == 0:
                for j in range(len(line_coverages[s_i])):

                    vars_indices.append((s_i, j))
                    min_vars_weights[(s_i, j)] = -line_coverages[s_i][j].weight
                    max_vars_weights[(s_i, j)] = line_coverages[s_i][j].weight

            #min_vars = symm_prob.addVars(vars_indices, vtype=GRB.BINARY)
            #max_vars = symm_prob.addVars(vars_indices, vtype=GRB.BINARY)
            #min_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in vars_indices])
            min_vars_array = [np.array(vars_indices),np.array([symm_prob.IntVar(0,1,str(i)) for i in vars_indices])]
            #max_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in vars_indices])
            max_vars_array = [np.array(vars_indices),np.array([symm_prob.IntVar(0,1,str(i)) for i in vars_indices])]
            nb_line_cov += 2*len(vars_indices)
            structured_line_coverage_variables.append([min_vars_array, max_vars_array])
            structured_line_coverage_variables_weights.append([min_vars_weights, max_vars_weights])

    #print("check_time", time()-check_time)
    check_time = time()
    if len(per_stroke_triple_intersections) > 0:
        half_anchored_ids = [s["s_id"] for s in per_stroke_triple_intersections
                             if s["s_id"] in stroke_indices]
        full_anchored_ids = [s["s_id"] for s in per_stroke_triple_intersections
                             if s["s_id"] in stroke_indices]
        #half_anchored_vars = symm_prob.addVars(half_anchored_ids, vtype=GRB.BINARY, name="half_anchored_vars")
        #half_anchored_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in half_anchored_ids])
        half_anchored_vars_array = [np.array(half_anchored_ids).reshape(-1,1),np.array([symm_prob.IntVar(0,1,str(i)) for i in half_anchored_ids])]
        half_anchored_ids_dict = dict([(v, i) for i,v in enumerate(half_anchored_ids)])
        #full_anchored_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in full_anchored_ids])
        full_anchored_vars_array = [np.array(full_anchored_ids).reshape(-1,1),np.array([symm_prob.IntVar(0,1,str(i)) for i in full_anchored_ids])]
        full_anchored_ids_dict = dict([(v, i) for i,v in enumerate(full_anchored_ids)])
        max_i_triple_ids = []
        min_i_triple_ids = []
        i_triple_ids = []
        k_axes_ids = []
        simple_inter_ids = []
        potentially_full_anchored_stroke_ids = []
        for s in per_stroke_triple_intersections:
            if not s["s_id"] in stroke_indices:
                continue
            for i_triple_inter in s["i_triple_intersections"]:
                i_triple_ids.append((s["s_id"], i_triple_inter["inter_id"]))
                for i in range(len(i_triple_inter["k_axes"])):
                    k_axes_ids.append((s["s_id"], i_triple_inter["inter_id"], i))
                    #for inter_id in i_triple_inter["k_axes"][i]:
                    #    simple_inter_ids.append(s["s_id"], i_triple_inter["inter_id"], i, inter_id)
        #i_triple_vars = symm_prob.addVars(gp.tuplelist(i_triple_ids), vtype=GRB.BINARY, name="i_triple_vars")
        #max_i_triple_vars = symm_prob.addVars(gp.tuplelist(i_triple_ids), vtype=GRB.BINARY, name="max_i_triple_vars")
        #min_i_triple_vars = symm_prob.addVars(gp.tuplelist(i_triple_ids), vtype=GRB.BINARY, name="min_i_triple_vars")
        #k_axes_vars = symm_prob.addVars(gp.tuplelist(k_axes_ids), vtype=GRB.BINARY, name="k_axes_vars")
        #i_triple_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in i_triple_ids])
        i_triple_vars_array = [np.array(i_triple_ids),np.array([symm_prob.IntVar(0,1,str(i)) for i in i_triple_ids])]
        #max_i_triple_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in i_triple_ids])
        max_i_triple_vars_array = [np.array(i_triple_ids),np.array([symm_prob.IntVar(0,1,str(i)) for i in i_triple_ids])]
        #min_i_triple_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in i_triple_ids])
        min_i_triple_vars_array = [np.array(i_triple_ids),np.array([symm_prob.IntVar(0,1,str(i)) for i in i_triple_ids])]
        i_triple_ids_dict = dict([(v, i) for i,v in enumerate(i_triple_ids)])
        #k_axes_vars = dict([(i,symm_prob.IntVar(0,1,str(i))) for i in k_axes_ids])
        k_axes_vars_array = [np.array(k_axes_ids),np.array([symm_prob.IntVar(0,1,str(i)) for i in k_axes_ids])]
        k_axes_ids_dict = dict([(v, i) for i,v in enumerate(k_axes_ids)])
        #simple_triple_inter_vars = symm_prob.addVars(gp.tuplelist(simple_inter_ids), vtype=GRB.BINARY, name="simple_triple_inter_vars")
        #symm_prob.update()
        #print("variable time", time() - solve_time)
        solve_time = time()
        # constraints
        for s in per_stroke_triple_intersections:
            if not s["s_id"] in stroke_indices:
                continue
            #print(half_anchored_vars.sum(s["s_id"]) <= i_triple_vars.sum(s["s_id"], "*"))
            #symm_prob.addConstr(half_anchored_vars.sum(s["s_id"]) <= i_triple_vars.sum(s["s_id"], "*"))
            #symm_prob.Add(half_anchored_vars[s["s_id"]] <= sum([i_triple_vars[key] for key in i_triple_vars.keys() if key[0] == s["s_id"]]))
            #symm_prob.Add(sum(star_selector(half_anchored_vars_array, [s["s_id"]])) <= sum(star_selector(i_triple_vars_array, (s["s_id"], "*"))))
            symm_prob.Add(half_anchored_vars_array[1][half_anchored_ids_dict[s["s_id"]]] <= sum(star_selector(i_triple_vars_array, (s["s_id"], "*"))))
            # choose only two i_triple per stroke
            #symm_prob.addConstr(i_triple_vars.sum(s["s_id"], "*") <= 2)
            symm_prob.Add(sum(star_selector(i_triple_vars_array, (s["s_id"], "*"))) <= 2)
            # full anchored strokes must have i_triples which are far away
            tmp_inters = sketch.intersection_graph.get_intersections([i_triple_inter["inter_id"] for i_triple_inter in s["i_triple_intersections"]])
            inter_params = {}
            for tmp_inter in tmp_inters:
                #print(tmp_inter.inter_id)
                #print(tmp_inter.mid_inter_param)
                #print(np.array(tmp_inter.stroke_ids) == s["s_id"])
                inter_params[tmp_inter.inter_id] = np.array(tmp_inter.mid_inter_param)[np.array(tmp_inter.stroke_ids) == s["s_id"]]

                #symm_prob.addConstr(max_i_triple_vars.sum(s["s_id"], tmp_inter.inter_id) <= i_triple_vars.sum(s["s_id"], tmp_inter.inter_id))
                #symm_prob.Add(sum(star_selector(max_i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))) <= sum(star_selector(i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))))
                symm_prob.Add(max_i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]] <= 
                    i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]])
                #symm_prob.addConstr(min_i_triple_vars.sum(s["s_id"], tmp_inter.inter_id) <= i_triple_vars.sum(s["s_id"], tmp_inter.inter_id))
                #symm_prob.Add(sum(star_selector(min_i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))) <= sum(star_selector(i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))))
                symm_prob.Add(min_i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]] <= 
                    i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]])
                #symm_prob.addConstr(i_triple_vars.sum(s["s_id"], tmp_inter.inter_id) <= min_i_triple_vars.sum(s["s_id"], "*"))
                #symm_prob.Add(sum(star_selector(i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))) <= sum(star_selector(min_i_triple_vars_array, (s["s_id"], "*"))))
                symm_prob.Add(i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]] <= sum(star_selector(min_i_triple_vars_array, (s["s_id"], "*"))))
                #symm_prob.addConstr(i_triple_vars.sum(s["s_id"], tmp_inter.inter_id) <= max_i_triple_vars.sum(s["s_id"], "*"))
                #symm_prob.Add(sum(star_selector(i_triple_vars_array, (s["s_id"], tmp_inter.inter_id))) <= sum(star_selector(max_i_triple_vars_array, (s["s_id"], "*"))))
                symm_prob.Add(i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]] <= sum(star_selector(max_i_triple_vars_array, (s["s_id"], "*"))))
            #if s["s_id"] == 19:
            if np.max(list(inter_params.values())) - np.min(list(inter_params.values())) >= 0.5:
                potentially_full_anchored_stroke_ids.append(s["s_id"])

            #max_t_i_sum = gp.quicksum([max_i_triple_vars.sum(s["s_id"], tmp_inter.inter_id)*inter_params[tmp_inter.inter_id]
            #                           for i, tmp_inter in enumerate(tmp_inters)])
            #max_t_i_sum = sum([sum(star_selector(max_i_triple_vars_array, (s["s_id"], tmp_inter.inter_id)))*inter_params[tmp_inter.inter_id][0]
            #                           for i, tmp_inter in enumerate(tmp_inters)])
            max_t_i_sum = sum([max_i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]]*inter_params[tmp_inter.inter_id][0]
                                       for i, tmp_inter in enumerate(tmp_inters)])
            #min_t_i_sum = gp.quicksum([min_i_triple_vars.sum(s["s_id"], tmp_inter.inter_id)*inter_params[tmp_inter.inter_id]
            #                           for i, tmp_inter in enumerate(tmp_inters)])
            #min_t_i_sum = sum([sum(star_selector(min_i_triple_vars_array, (s["s_id"], tmp_inter.inter_id)))*inter_params[tmp_inter.inter_id][0]
            #                           for i, tmp_inter in enumerate(tmp_inters)])
            min_t_i_sum = sum([min_i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], tmp_inter.inter_id)]]*inter_params[tmp_inter.inter_id][0]
                                       for i, tmp_inter in enumerate(tmp_inters)])

            #symm_prob.addConstr(max_t_i_sum - min_t_i_sum >= 0.5 - 100.0*(1-full_anchored_vars.sum(s["s_id"])))
            #symm_prob.addConstr(max_i_triple_vars.sum(s["s_id"], "*") <= 1)
            #symm_prob.addConstr(min_i_triple_vars.sum(s["s_id"], "*") <= 1)
            #symm_prob.Add(max_t_i_sum - min_t_i_sum >= 0.5 - 100.0*(1-sum(star_selector(full_anchored_vars_array, [s["s_id"]]))))
            symm_prob.Add(max_t_i_sum - min_t_i_sum >= 0.5 - 100.0*(1-full_anchored_vars_array[1][full_anchored_ids_dict[s["s_id"]]]))
            symm_prob.Add(sum(star_selector(max_i_triple_vars_array, (s["s_id"], "*"))) <= 1)
            symm_prob.Add(sum(star_selector(min_i_triple_vars_array, (s["s_id"], "*"))) <= 1)

            #symm_prob.addConstr(i_triple_vars.sum(s["s_id"], "*") >= 4 - 100.0*(1-full_anchored_vars.sum(s["s_id"])))

            for i_triple_inter in s["i_triple_intersections"]:
                #symm_prob.addConstr(k_axes_vars.sum(s["s_id"], i_triple_inter["inter_id"], "*")  \
                #                    >= 3 - 1000.0*(1-i_triple_vars.sum(s["s_id"], i_triple_inter["inter_id"])))
                #symm_prob.Add(sum(star_selector(k_axes_vars_array, (s["s_id"], i_triple_inter["inter_id"], "*")))  \
                #                    >= 3 - 1000.0*(1-sum(star_selector(i_triple_vars_array, (s["s_id"], i_triple_inter["inter_id"])))))
                symm_prob.Add(sum(star_selector(k_axes_vars_array, (s["s_id"], i_triple_inter["inter_id"], "*")))  \
                                    >= 3 - 1000.0*(1-i_triple_vars_array[1][i_triple_ids_dict[(s["s_id"], i_triple_inter["inter_id"])]]))

                for i in range(len(i_triple_inter["k_axes"])):
                    #print(i_triple_inter["k_axes"][i])
                    #print(k_axes_vars.sum(s["s_id"], i_triple_inter["inter_id"], i) \
                    #      <= gp.quicksum([intersection_variables.sum("*", "*", inter_id) for inter_id in i_triple_inter["k_axes"][i]]) \
                    #      + np.sum([inter_id in fixed_intersections for inter_id in i_triple_inter["k_axes"][i]]))
                    #symm_prob.addConstr(k_axes_vars.sum(s["s_id"], i_triple_inter["inter_id"], i) \
                    #      <= gp.quicksum([intersection_variables.sum("*", "*", inter_id) for inter_id in i_triple_inter["k_axes"][i]]) \
                    #      + np.sum([inter_id in fixed_intersections for inter_id in i_triple_inter["k_axes"][i]]))
                    #symm_prob.Add(sum(star_selector(k_axes_vars_array, (s["s_id"], i_triple_inter["inter_id"], i))) \
                    #      <= sum([sum(star_selector(intersection_variables_array, ("*", "*", inter_id))) for inter_id in i_triple_inter["k_axes"][i]]) \
                    #      + np.sum([inter_id in fixed_intersections for inter_id in i_triple_inter["k_axes"][i]]))
                    symm_prob.Add(k_axes_vars_array[1][k_axes_ids_dict[(s["s_id"], i_triple_inter["inter_id"], i)]] \
                          <= sum([sum(star_selector(intersection_variables_array, ("*", "*", inter_id))) for inter_id in i_triple_inter["k_axes"][i]]) \
                          + np.sum([inter_id in fixed_intersections for inter_id in i_triple_inter["k_axes"][i]]))
        # obj-term
        #half_anchored_term = 0.5*half_anchored_vars.sum() + 0.5*full_anchored_vars.sum()
        #half_anchored_term = -gp.quicksum([stroke_variables.sum(s["s_id"])*(1-half_anchored_vars.sum(s["s_id"]))
        #                                   for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices])
        #half_anchored_term += -gp.quicksum([stroke_variables.sum(s["s_id"])*(1-full_anchored_vars.sum(s["s_id"]))
        #                                    for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices
        #                                    if s["s_id"] in potentially_full_anchored_stroke_ids])
        #half_anchored_mult_vars = dict([(i, symm_prob.IntVar(0,1,str(i))) for i in stroke_indices])
        half_anchored_mult_vars_array = [np.array(stroke_indices).reshape(-1,1), np.array([symm_prob.IntVar(0,1,str(i)) for i in stroke_indices])]
        for s in per_stroke_triple_intersections:
            if not s["s_id"] in stroke_indices:
                continue
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) >= sum(star_selector(stroke_variables_array, [s["s_id"]]))+sum(star_selector(half_anchored_vars_array, [s["s_id"]])) -1)
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) <= sum(star_selector(stroke_variables_array, [s["s_id"]])))
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+sum(star_selector(half_anchored_vars_array, [s["s_id"]])) -1)
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) <= sum(star_selector(half_anchored_vars_array, [s["s_id"]])))
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+half_anchored_vars_array[1][half_anchored_ids_dict[s["s_id"]]] -1)
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) <= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]])
            #symm_prob.Add(sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]])) <= half_anchored_vars_array[1][half_anchored_ids_dict[s["s_id"]]])

            symm_prob.Add(half_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+half_anchored_vars_array[1][half_anchored_ids_dict[s["s_id"]]] -1)
            symm_prob.Add(half_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] <= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]])
            symm_prob.Add(half_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] <= half_anchored_vars_array[1][half_anchored_ids_dict[s["s_id"]]])

        #half_anchored_term = -sum([stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]-sum(star_selector(half_anchored_mult_vars_array, [s["s_id"]]))
        #                                   for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices])
        half_anchored_term = -sum([stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]-half_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]]
                                           for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices])
        #full_anchored_mult_vars = dict([(i, symm_prob.IntVar(0,1,str(i))) for i in stroke_indices])
        full_anchored_mult_vars_array = [np.array(stroke_indices).reshape(-1,1), np.array([symm_prob.IntVar(0,1,str(i)) for i in stroke_indices])]
        for s in per_stroke_triple_intersections:
            if not s["s_id"] in potentially_full_anchored_stroke_ids:
                continue
            #symm_prob.Add(sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]])) >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+sum(star_selector(full_anchored_vars_array, [s["s_id"]])) -1)
            #symm_prob.Add(sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]])) <= sum(star_selector(full_anchored_vars_array, [s["s_id"]])))
            #symm_prob.Add(sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]])) >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+full_anchored_vars_array[1][full_anchored_ids_dict[s["s_id"]]] -1)
            #symm_prob.Add(sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]])) <= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]])
            #symm_prob.Add(sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]])) <= full_anchored_vars_array[1][full_anchored_ids_dict[s["s_id"]]])
            symm_prob.Add(full_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] >= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]+full_anchored_vars_array[1][full_anchored_ids_dict[s["s_id"]]] -1)
            symm_prob.Add(full_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] <= stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]])
            symm_prob.Add(full_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]] <= full_anchored_vars_array[1][full_anchored_ids_dict[s["s_id"]]])
        #half_anchored_term += -sum([stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]]-sum(star_selector(full_anchored_mult_vars_array, [s["s_id"]]))
        #                                    for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices
        #                                    if s["s_id"] in potentially_full_anchored_stroke_ids])
        half_anchored_term += -sum([stroke_variables_array[1][stroke_variables_indices_dict[s["s_id"]]] - full_anchored_mult_vars_array[1][stroke_variables_indices_dict[s["s_id"]]]
                                            for s in per_stroke_triple_intersections if s["s_id"] in stroke_indices
                                            if s["s_id"] in potentially_full_anchored_stroke_ids])

    #print("check_time", time()-check_time)
    check_time = time()

    #print("check_time", time()-check_time)
    check_time = time()
    #print("nb_line_cov: ", nb_line_cov)
    #symm_prob.update()
    #print("nb_variables", len(symm_prob.getVars()), "nb_constraints", len(symm_prob.getConstrs()))
    # Objective function
    #obj_expr = gp.LinExpr()
    obj_expr = 0
    if len(per_stroke_triple_intersections) > 0:
        if not ABLATE_ANCHORING:
            obj_expr += 5.0*half_anchored_term
    coeff = 2.0
    #obj_expr = coeff*symm_stroke_variables.sum("*")
    corr_term = 0
    corr_term += coeff*sum(per_stroke_plane_variables_array[1])
    #corr_term += coeff*non_self_symmetric_per_stroke_per_plane_variables.sum("*")
    obj_expr += corr_term

    cluster_proximity_term = 0
    #cluster_proximity_term += -100.0*correspondence_variables.prod(cluster_proximity_weights)
    #cluster_proximity_term += -100.0*sum([cluster_proximity_weights[i]*sum(star_selector(correspondence_variables_array, i)) for i in correspondence_indices])
    cluster_proximity_term += -100.0*sum([cluster_proximity_weights[v]*correspondence_variables_array[1][i] for i,v in enumerate(correspondence_indices)])
    if not ABLATE_PROXIMITY:
        obj_expr += cluster_proximity_term

    # maximize curve symmetry
    curve_corr_term = 0
    if not ABLATE_CURVE_CORR:
        coeff = 10.0
        for corr_id, corr in enumerate(correspondences):
            if corr[0] == corr[1] and sketch.strokes[corr[0]].axis_label == 5 and not sketch.strokes[corr[0]].is_ellipse():
                #obj_expr += coeff*correspondence_variables.sum("*", "*", "*", "*", "*", "*", corr_id)
                #curve_corr_term += coeff*correspondence_variables.sum("*", "*", "*", "*", "*", "*", corr_id)
                curve_corr_term += coeff*sum(star_selector(correspondence_variables_array, ("*", "*", "*", "*", "*", "*", corr_id)))
            if len(per_correspondence_sym_scores[corr_id]) > 0:
                # here, we have two symmetric curves
                # TODO: weight by stroke length
                corr_var_stroke = 0
                if len(fixed_strokes[corr[0]]) > 0:
                    corr_var_stroke = 1
                #obj_expr += coeff*stroke_lengths[corr[0]]*per_correspondence_sym_scores[corr_id][0]* \
                #            correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                #obj_expr += coeff*stroke_lengths[corr[1]]*per_correspondence_sym_scores[corr_id][1]* \
                #            correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                curve_corr_term += coeff*stroke_lengths[corr[0]]*per_correspondence_sym_scores[corr_id][0]* \
                            sum(star_selector(correspondence_variables_array, (corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)))
                curve_corr_term += coeff*stroke_lengths[corr[1]]*per_correspondence_sym_scores[corr_id][1]* \
                            sum(star_selector(correspondence_variables_array, (corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)))
        obj_expr += curve_corr_term
#    for s_id in range(nb_strokes):
#        coeff = 1.0
#        #if len(stroke_lengths) > 0:
#        #    coeff = stroke_lengths[s_id]
#        if len(fixed_strokes[s_id]) > 0:
#            continue
#        for other_s_id in range(nb_strokes):
#            coeff = 1.0
#            if s_id != other_s_id:
#                if len(fixed_strokes[other_s_id]) > 0:
#                    coeff = 2.0
#                non_self_symm_sum += coeff*correspondence_variables.sum(s_id, "*", "*",
#                                                                        other_s_id, "*", "*")
#            else:
#                coeff = 2.0
#                self_symm_sum += coeff*correspondence_variables.sum(s_id, "*", "*",
#                                                              other_s_id, "*", "*")
#    obj_expr = non_self_symm_sum + self_symm_sum
    line_coverage_term = 0
    eccentricity_term = 0
    tangential_intersection_term = 0
    if sketch_connectivity and line_coverage:
        coeff = 1.0
        #if len(stroke_lengths) > 0:
        #    coeff = stroke_lengths[s_i]
        #obj_expr -= 2.0*intersection_threshold_variables.sum()
        for s_i in range(len(structured_line_coverage_variables)):
            if len(fixed_strokes[s_i]) > 0:
                continue
            if len(eccentricity_weights[s_i]) == 0:
                #line_coverage_term += 4.0*coeff*(structured_line_coverage_variables[s_i][0].prod(structured_line_coverage_variables_weights[s_i][0]) + \
                #                                 structured_line_coverage_variables[s_i][1].prod(structured_line_coverage_variables_weights[s_i][1]))
                #line_coverage_term += 4.0*coeff*(sum([structured_line_coverage_variables[s_i][0][(s_i,i)]*structured_line_coverage_variables_weights[s_i][0][(s_i,i)] for i in range(len(structured_line_coverage_variables_weights[s_i][0]))]) + \
                #                                sum([structured_line_coverage_variables[s_i][1][(s_i,i)]*structured_line_coverage_variables_weights[s_i][1][(s_i,i)] for i in range(len(structured_line_coverage_variables_weights[s_i][1]))]))
                line_coverage_term += 4.0*coeff*(sum([sum(star_selector(structured_line_coverage_variables[s_i][0], (s_i,i)))*structured_line_coverage_variables_weights[s_i][0][(s_i,i)] for i in range(len(structured_line_coverage_variables_weights[s_i][0]))]) + \
                                                sum([sum(star_selector(structured_line_coverage_variables[s_i][1], (s_i,i)))*structured_line_coverage_variables_weights[s_i][1][(s_i,i)] for i in range(len(structured_line_coverage_variables_weights[s_i][1]))]))
            else:
                #for p_id, p in enumerate(proxy_variables.select(s_i, "*")):
                for p_id, p in enumerate(star_selector(proxy_variables_array, (s_i, "*"))):
                    #obj_line_cov += 3.0*coeff*eccentricity_weights[s_i][p_id]*p
                    #print("HERE")
                    #print(eccentricity_weights[s_i][p_id])
                    eccentricity_term += 1.0*coeff*eccentricity_weights[s_i][p_id]*p
        for inter in intersections_3d:
            if inter.is_tangential:
                #tangential_intersection_term += 5.0*intersection_variables.sum("*", "*", inter.inter_id)
                tangential_intersection_term += 5.0*sum(star_selector(intersection_variables_array, ("*", "*", inter.inter_id)))
                #obj_line_cov += 10.0*intersection_variables.sum("*", "*", inter.inter_id)
            #if inter.is_triplet:
            #    obj_expr += intersection_variables.sum("*", "*", inter.inter_id)

    #            print(s_i)
    #            print(structured_line_coverage_variables[s_i][0])
    #            print(structured_line_coverage_variables_weights[s_i][0])
    #            print(structured_line_coverage_variables[s_i][0].prod(structured_line_coverage_variables_weights[s_i][0]) + \
    #                  structured_line_coverage_variables[s_i][1].prod(structured_line_coverage_variables_weights[s_i][1]))
    if not ABLATE_LINE_COVERAGE:
        obj_expr += line_coverage_term
    if not ABLATE_ECCENTRICITY:
        obj_expr -= eccentricity_term
    if not ABLATE_TANGENTIAL_INTERSECTIONS:
        obj_expr += tangential_intersection_term
    #obj_expr += line_coverage_term - eccentricity_term + tangential_intersection_term
    #intersection_term = gp.LinExpr()
    intersection_term = 0
    if sketch_connectivity and not line_coverage:
        #obj_expr += 5.0*intersection_variables.sum()
        intersection_term += 5.0*sum(intersection_variables_array[1])
    obj_expr += intersection_term
    #obj_expr += 10.0*parallel_intersection_variables.sum()

    #symm_prob.setObjective(obj_line_cov+obj_expr, sense=GRB.MAXIMIZE)
    #symm_prob.setObjective(obj_expr, sense=GRB.MAXIMIZE)
    symm_prob.Maximize(obj_expr)
    #symm_prob.update()
    #symm_prob.setObjectiveN(obj_line_cov, index=0, priority=2)
    #symm_prob.setObjectiveN(obj_expr, index=1, priority=0)
    #symm_prob.ModelSense = GRB.MAXIMIZE
    #print(symm_prob.getObjective())

    # only allow curves which exhibit a certain amount of symmetry
    if not ABLATE_CURVE_CORR:
        #curve_symmetry = [gp.LinExpr() for s_id in range(nb_strokes)]
        non_empty_curve_symmetry = [False for s_id in range(nb_strokes)]
        curve_symmetry = [0 for s_id in range(nb_strokes)]
        for corr_id, corr in enumerate(correspondences):
            if corr[0] == corr[1] and sketch.strokes[corr[0]].axis_label == 5 and not sketch.strokes[corr[0]].is_ellipse():
                #curve_symmetry[corr[0]] += correspondence_variables.sum("*", "*", "*", "*", "*", "*", corr_id)
                curve_symmetry[corr[0]] += sum(star_selector(correspondence_variables_array, ("*", "*", "*", "*", "*", "*", corr_id)))
                non_empty_curve_symmetry[corr[0]] = True
            if len(per_correspondence_sym_scores[corr_id]) > 0:
                # here, we have two symmetric curves
                # TODO: weight by stroke length
                corr_var_stroke = 0
                if len(fixed_strokes[corr[0]]) > 0:
                    corr_var_stroke = 1
                #curve_symmetry[corr[corr_var_stroke]] += correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                #curve_symmetry[corr[1-corr_var_stroke]] += correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                curve_symmetry[corr[corr_var_stroke]] += per_correspondence_sym_scores[corr_id][0]* \
                                                         sum(star_selector(correspondence_variables_array, (corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)))
                                                         #correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                curve_symmetry[corr[1-corr_var_stroke]] += per_correspondence_sym_scores[corr_id][1]* \
                                                           sum(star_selector(correspondence_variables_array, (corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)))
                                                           #correspondence_variables.sum(corr[corr_var_stroke], "*", "*", corr[1-corr_var_stroke], "*", corr[4], corr_id)
                non_empty_curve_symmetry[corr[corr_var_stroke]] = True
                non_empty_curve_symmetry[corr[1-corr_var_stroke]] = True

        for s_id in range(nb_strokes):
            if len(fixed_strokes[s_id]) > 0:
                continue
            if non_empty_curve_symmetry[s_id]:
                #symm_prob.addConstr(curve_symmetry[s_id] >= 0.75*stroke_variables[s_id])
                symm_prob.Add(curve_symmetry[s_id] >= 0.75*stroke_variables_array[1][stroke_variables_indices_dict[s_id]])

    # only choose a single proxy per stroke and only choose it if the stroke has been chosen
    for s_id in range(nb_strokes):
        #if s_id in enforce_strokes:
        #    symm_prob.addConstr(stroke_variables[s_id] == 1)
        other_stroke_ids = [other_s_id for other_s_id in range(nb_strokes) if other_s_id != s_id]
        if len(fixed_strokes[s_id]) == 0:
            #symm_prob.addConstr(proxy_variables.sum(s_id, "*") <= stroke_variables[s_id])
            symm_prob.Add(sum(star_selector(proxy_variables_array, (s_id, "*"))) <= stroke_variables_array[1][stroke_variables_indices_dict[s_id]])
            #print(sum(star_selector(proxy_variables_array, (s_id, "*"))) <= sum(star_selector(stroke_variables_array, [s_id])))
            # OLD
            #symm_prob.addConstr(2 <= correspondence_variables.sum(s_id, "*", "*", "*", "*", "*", "*") \
            #                    + (1-symm_stroke_variables.sum(s_id))*100.0)
            # Better way of writing constraint
            #symm_prob.addConstr(correspondence_variables.sum(s_id, "*", "*", "*", "*", "*", "*") >= 2 - (1-symm_stroke_variables.sum(s_id))*100.0)
            # Cap correspondences to one per plane. We only want to know if a certain plane has been chosen
            for l in range(nb_planes):
                #symm_prob.addConstr(correspondence_variables.sum(s_id, "*", "*", "*", "*", l, "*") + correspondence_variables.sum("*", "*", "*", s_id, "*", l, "*") >= 1
                #                    - (1-per_stroke_plane_variables.sum(s_id, l))*100.0)
                #symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", "*", "*", "*", l, "*"))) + sum(star_selector(correspondence_variables_array, ("*", "*", "*", s_id, "*", l, "*"))) >= 1 - (1-sum(star_selector(per_stroke_plane_variables_array, [s_id, l])))*100.0)
                symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", "*", "*", "*", l, "*"))) + 
                    sum(star_selector(correspondence_variables_array, ("*", "*", "*", s_id, "*", l, "*"))) >= 1 - 
                    (1-per_stroke_plane_variables_array[1][per_stroke_plane_indices_dict[s_id, l]])*100.0)
    # the contribution of a single stroke to a proxy can at most be one
    for s_id in range(nb_strokes):
        if len(fixed_strokes[s_id]) > 0:
            continue
        for p_id in range(len(proxy_strokes[s_id])):
            for other_s_id in range(nb_strokes):
                if s_id != other_s_id:
                    #symm_prob.addConstr(correspondence_variables.sum(s_id, p_id, "*", other_s_id, "*", "*", "*") <= proxy_variables.sum(s_id, p_id))
                    #symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", other_s_id, "*", "*", "*"))) <= sum(star_selector(proxy_variables_array, [s_id, p_id])))
                    symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", other_s_id, "*", "*", "*"))) <= proxy_variables_array[1][proxy_indices_dict[s_id, p_id]])
                else:
                    # we want to allow orthogonal AND planar self-symmetric strokes
                    for l in range(nb_planes):
                        #symm_prob.addConstr(correspondence_variables.sum(s_id, p_id, "*", other_s_id, "*", l, "*") <= proxy_variables.sum(s_id, p_id))
                        #symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", other_s_id, "*", l, "*"))) <= sum(star_selector(proxy_variables_array, [s_id, p_id])))
                        symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", other_s_id, "*", l, "*"))) <= proxy_variables_array[1][proxy_indices_dict[s_id, p_id]])
            # global symmetry constraint
            if main_axis > -1:
                #symm_prob.addConstr(proxy_variables.sum(s_id, p_id) <= correspondence_variables.sum(s_id, p_id, "*", "*", "*", main_axis, "*"))
                #symm_prob.Add(sum(star_selector(proxy_variables_array, [s_id, p_id])) <= sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", "*", "*", main_axis, "*"))))
                symm_prob.Add(proxy_variables_array[1][proxy_indices_dict[s_id, p_id]] <= sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", "*", "*", main_axis, "*"))))


    already_covered_correspondences = set()
    for s_id in range(nb_strokes):
        if len(fixed_strokes[s_id]) > 0:
            continue
        for p_id in range(len(proxy_strokes[s_id])):
            # An active proxy must have at least one correspondence
            # sum(c_ilkj) => p_j
            #symm_prob.addConstr(proxy_variables.sum(s_id, p_id) <= correspondence_variables.sum(s_id, p_id, "*", "*", "*", "*", "*"))
            #symm_prob.Add(sum(star_selector(proxy_variables_array, [s_id, p_id])) <= sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", "*", "*", "*", "*"))))
            symm_prob.Add(proxy_variables_array[1][proxy_indices_dict[s_id, p_id]] <= sum(star_selector(correspondence_variables_array, (s_id, p_id, "*", "*", "*", "*", "*"))))

        # coherent symmetry correspondence selection
        for l in range(nb_planes):
            for corr in per_stroke_per_plane_correspondences[s_id][l]:
                i_1 = s_id
                k_1 = corr.own_candidate_id
                i_2 = corr.partner_stroke_id
                if len(fixed_strokes[i_2]) > 0:
                    continue
                k_2 = corr.partner_candidate_id
                if i_1 == i_2 and k_1 == k_2:
                    continue
                if (l, i_1, k_1, i_2, k_2) in already_covered_correspondences or \
                        (l, i_2, k_2, i_1, k_1) in already_covered_correspondences:
                    continue
                already_covered_correspondences.add((l, i_1, k_1, i_2, k_2))

                #symm_prob.addConstr(correspondence_variables.sum(s_id, "*", "*", corr.partner_stroke_id, "*", l, "*") == \
                #                    correspondence_variables.sum(corr.partner_stroke_id, "*", "*", s_id, "*", l, "*"))
                symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", "*", corr.partner_stroke_id, "*", l, "*"))) == \
                                    sum(star_selector(correspondence_variables_array, (corr.partner_stroke_id, "*", "*", s_id, "*", l, "*"))))

        # coherent self-symmetry selection
        # only select a self-symmetric correspondence if both of their intersecting
        # strokes are symmetric
        # except for ellipses
        if len(eccentricity_weights[s_id]) > 0:
            continue
        for l in range(nb_planes):
            for corr in per_stroke_per_plane_correspondences[s_id][l]:
                i_1 = s_id
                i_2 = corr.partner_stroke_id
                k_1 = corr.own_candidate_id
                if i_1 != i_2:
                    continue
                first_inter_stroke_id = corr.first_inter_stroke_id
                snd_inter_stroke_id = corr.snd_inter_stroke_id

                # avoid degenerate cases
                if first_inter_stroke_id == snd_inter_stroke_id:
                    # allow self-symmetric cases where there's no intersection
                    if first_inter_stroke_id == -1:
                        continue
                    symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", k_1, "*", "*", l, "*"))) == 0)
                    continue

                # ignore for two fixed strokes
                if len(fixed_strokes[first_inter_stroke_id]) > 0 and len(fixed_strokes[snd_inter_stroke_id]) > 0:
                    continue
                if len(fixed_strokes[first_inter_stroke_id]) > 0:
                    tmp = snd_inter_stroke_id
                    snd_inter_stroke_id = first_inter_stroke_id
                    first_inter_stroke_id = tmp

                # get candidate_id for stroke-pair
                stroke_pair_k_1 = -1
                for tmp_k in range(len(per_stroke_per_plane_correspondences[first_inter_stroke_id][l])):
                    if per_stroke_per_plane_correspondences[first_inter_stroke_id][l][tmp_k].partner_stroke_id == snd_inter_stroke_id:
                        stroke_pair_k_1 = tmp_k
                        break
                if stroke_pair_k_1 == -1:
                    # no symmetry correspondence between intersecting strokes
                    symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", k_1, "*", "*", l, "*"))) == 0)
                    continue

                # coherent symmetry selection constraint
                # only allow for this self-symmetry if any symmetry correspondence
                # between the two first strokes has been validated
                #if len(correspondence_variables.select(s_id, "*", k_1, "*", "*", l)) > 0:
                #    print(correspondence_variables.select(s_id, "*", k_1, "*", "*", l))
                symm_prob.Add(sum(star_selector(correspondence_variables_array, (s_id, "*", k_1, "*", "*", l, "*"))) <= \
                                    sum(star_selector(correspondence_variables_array, (first_inter_stroke_id, "*", stroke_pair_k_1, "*", "*", l, "*"))))# + \
                #correspondence_variables.sum(first_inter_stroke_id, "*", "*", snd_inter_stroke_id, "*", l))# + \
                #correspondence_variables.sum(snd_inter_stroke_id, "*", "*", first_inter_stroke_id, "*", l))


    #if 77 < batch[1]:
    #    symm_prob.addConstr(stroke_variables.sum(77) == 1)
    #    symm_prob.addConstr(stroke_variables.sum(77) == 1)
    #symm_prob.addConstr(stroke_variables.sum(30) == 0)
    #symm_prob.addConstr(stroke_variables.sum(22) == 0)
    #symm_prob.addConstr(stroke_variables.sum(23) == 0)
    #symm_prob.addConstr(stroke_variables.sum(24) == 0)
    #symm_prob.addConstr(stroke_variables.sum(25) == 0)
    #symm_prob.addConstr(stroke_variables.sum(26) == 0)
    #symm_prob.addConstr(stroke_variables.sum(31) == 0)
    #symm_prob.addConstr(stroke_variables.sum(32) == 0)
    #symm_prob.addConstr(correspondence_variables.sum(0, "*", "*", 2, "*", 0, "*") + correspondence_variables.sum(2, "*", "*", 0, "*", 2, "*") >= 1)
    #symm_prob.addConstr(proxy_variables.sum(1, "*") <= correspondence_variables.sum(1, "*", "*", 54, "*", 1, "*") + correspondence_variables.sum(54, "*", "*", 1, "*", 1, "*"))
    #symm_prob.addConstr(proxy_variables.sum(36, "*") <= correspondence_variables.sum(38, "*", "*", 36, "*", 2, "*") + correspondence_variables.sum(36, "*", "*", 38, "*", 2, "*"))
    #symm_prob.addConstr(proxy_variables.sum(28, "*") <= correspondence_variables.sum(28, "*", "*", 30, "*", 1, "*") + correspondence_variables.sum(30, "*", "*", 28, "*", 1, "*"))
    #symm_prob.addConstr(proxy_variables.sum(30, "*") <= correspondence_variables.sum(28, "*", "*", 30, "*", 1, "*") + correspondence_variables.sum(30, "*", "*", 28, "*", 1, "*"))

    #symm_prob.update()
    if sketch_connectivity:
        for vec_id, inter_var in enumerate(intersection_variables_array[0]):
            s_0, s_1, inter_id = inter_var
            epsilon = intersections_3d[vec_id].epsilon

            if intersections_3d[vec_id].is_fixed:
                free_stroke_id = 0
                if len(intersections_3d[vec_id].cam_depths[0]) == 0:
                    free_stroke_id = 1
                # avoid empty solutions
                #symm_prob.addConstr(intersection_variables.sum(s_0, s_1, inter_id) <= proxy_variables.sum(inter_var[1-free_stroke_id], "*"))
                cam_depths_0 = intersections_3d[vec_id].cam_depths[free_stroke_id]
                tmp_proxies = star_selector(proxy_variables_array, (inter_var[free_stroke_id], "*"))
                #x_0 = gp.quicksum([cam_depths_0[i]*tmp_proxies[i]
                #                   for i in range(len(cam_depths_0))])
                x_0 = sum([cam_depths_0[i]*tmp_proxies[i]
                                   for i in range(len(cam_depths_0))])
                x_1 = intersections_3d[vec_id].fix_depth
                #symm_prob.Add(sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id])) <= \
                #                    sum(star_selector(proxy_variables_array, ([s_0, s_1][free_stroke_id], "*"))))
                symm_prob.Add(intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]] <= \
                                    sum(star_selector(proxy_variables_array, ([s_0, s_1][free_stroke_id], "*"))))
            else:
                cam_depths_0 = intersections_3d[vec_id].cam_depths[0]
                tmp_proxies = star_selector(proxy_variables_array, (s_0, "*"))
                x_0 = sum([cam_depths_0[i]*tmp_proxies[i]
                                   for i in range(len(cam_depths_0))])

                cam_depths_1 = intersections_3d[vec_id].cam_depths[1]
                tmp_proxies = star_selector(proxy_variables_array, (s_1, "*"))
                x_1 = sum([cam_depths_1[i]*tmp_proxies[i]
                                   for i in range(len(cam_depths_1))])
                #symm_prob.Add(sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id])) <= \
                #                    sum(star_selector(proxy_variables_array, (s_0, "*"))))
                symm_prob.Add(intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]] <= \
                                    sum(star_selector(proxy_variables_array, (s_0, "*"))))
                #symm_prob.Add(sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id])) <= \
                #                    sum(star_selector(proxy_variables_array, (s_1, "*"))))
                symm_prob.Add(intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]] <= \
                                    sum(star_selector(proxy_variables_array, (s_1, "*"))))
            #symm_prob.addConstr(x_0 - x_1 <= epsilon + 5*epsilon*intersection_threshold_variables.sum(s_0, s_1, inter_id) + 100.0*(1-intersection_variables.sum(s_0, s_1, inter_id)))
            #symm_prob.addConstr(x_1 - x_0 <= epsilon + 5*epsilon*intersection_threshold_variables.sum(s_0, s_1, inter_id) + 100.0*(1-intersection_variables.sum(s_0, s_1, inter_id)))
            #symm_prob.Add(x_0 - x_1 <= epsilon + 100.0*(1-sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id]))))
            #symm_prob.Add(x_1 - x_0 <= epsilon + 100.0*(1-sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id]))))
            symm_prob.Add(x_0 - x_1 <= epsilon + 100.0*(1-intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]]))
            symm_prob.Add(x_1 - x_0 <= epsilon + 100.0*(1-intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]]))
            #symm_prob.addConstr(intersection_threshold_variables.sum(s_0, s_1, inter_id) <= intersection_variables.sum(s_0, s_1, inter_id))

        for vec_id, inter_var in enumerate(parallel_intersection_variables_array[1]):
            s_0, s_1, inter_id = inter_var
            #symm_prob.addConstr(parallel_intersection_variables.sum(s_0, s_1, inter_id) <= intersection_variables.sum(s_0, s_1, inter_id))
            #symm_prob.Add(sum(star_selector(parallel_intersection_variables_array, [s_0, s_1, inter_id])) <= sum(star_selector(intersection_variables_array, [s_0, s_1, inter_id])))
            symm_prob.Add(parallel_intersection_variables_array[1][parallel_intersection_indices_dict[s_0, s_1, inter_id]] <= intersection_variables_array[1][intersection_indices_dict[s_0, s_1, inter_id]])

    if line_coverage:
        for s_i in range(len(structured_line_coverage_variables)):
            if len(eccentricity_weights[s_i]) > 0:
                continue
            if len(structured_line_coverage_variables[s_i][0]) == 0:
                continue
            # there can at most be one max/min line_coverage
            #symm_prob.addConstr(structured_line_coverage_variables[s_i][0].sum() <= 1)
            #symm_prob.addConstr(structured_line_coverage_variables[s_i][1].sum() <= 1)
            symm_prob.Add(sum(structured_line_coverage_variables[s_i][0][1]) <= 1)
            symm_prob.Add(sum(structured_line_coverage_variables[s_i][1][1]) <= 1)

            all_associated_inter_vars = []
            for j in range(len(line_coverages[s_i])):
                #stroke_proxy_list = line_coverages[s_i][j].stroke_proxy_ids
                inter_id = line_coverages[s_i][j].inter_id
                # an intersection can only be considered for line coverage,
                # if there's an actual 3d intersection
                #if len(intersection_variables.select("*", "*", inter_id)) > 0:
                #symm_prob.addConstr(structured_line_coverage_variables[s_i][0].sum("*", j) \
                #                    <= intersection_variables.sum("*", "*", inter_id))
                #symm_prob.addConstr(structured_line_coverage_variables[s_i][1].sum("*", j) \
                #                    <= intersection_variables.sum("*", "*", inter_id))
                symm_prob.Add(sum(star_selector(structured_line_coverage_variables[s_i][0], ("*", j))) \
                                    <= sum(star_selector(intersection_variables_array, ("*", "*", inter_id))))
                symm_prob.Add(sum(star_selector(structured_line_coverage_variables[s_i][1], ("*", j))) \
                                    <= sum(star_selector(intersection_variables_array, ("*", "*", inter_id))))
            # there should at least be one max/min line_coverage if there's a 3d intersection
            for inter in intersections_3d:
                if inter.is_parallel:
                    continue
                if inter.stroke_ids[0] == s_i or inter.stroke_ids[1] == s_i:
                    #symm_prob.addConstr(intersection_variables.sum("*", "*", inter.inter_id) <= \
                    #                    structured_line_coverage_variables[s_i][0].sum())
                    #symm_prob.addConstr(intersection_variables.sum("*", "*", inter.inter_id) <= \
                    #                    structured_line_coverage_variables[s_i][1].sum())
                    symm_prob.Add(sum(star_selector(intersection_variables_array, ("*", "*", inter.inter_id))) <= \
                                        sum(structured_line_coverage_variables[s_i][0][1]))
                    symm_prob.Add(sum(star_selector(intersection_variables_array, ("*", "*", inter.inter_id))) <= \
                                        sum(structured_line_coverage_variables[s_i][1][1]))
                #symm_prob.addConstr(inter_var <= structured_line_coverage_variables[s_i][1].sum())


    if best_obj_value > 0.0:
        #print("Cutoff", best_obj_value)
        #symm_prob.setParam("Cutoff", best_obj_value)
        symm_prob.Add(obj_expr >= best_obj_value)
        #symm_prob.setParam("OptimalityTol", 10e-8)
        #symm_prob.setParam("FeasibilityTol", 10e-9)
        #symm_prob.setParam("NumericFocus", 3)
    #print("constraints time", time() - solve_time)
    solve_time = time()
    #symm_prob.update()
    #print("nb_variables", len(symm_prob.getVars()), "nb_constraints", len(symm_prob.getConstrs()))
    #print("start solve")
    #symm_prob.optimize()
    status = symm_prob.Solve()
    #print("status", status)
    #print("solve time: ", time() - solve_time)
    #cluster_proximity_term = -100.0*sum([cluster_proximity_weights[i]*star_selector(correspondence_variables_array, i)[0].solution_value() for i in correspondence_indices])
    #print("cluster_proximity_term", cluster_proximity_term)
    #corr_term = coeff*sum([v.solution_value() for v in per_stroke_plane_variables_array[1]])
    #print("corr_term", corr_term)
    #for v in proxy_variables_array[1]:
    #    print(v, v.solution_value())
    #for v in per_stroke_plane_variables_array[1]:
    #    print(v, v.solution_value())
    #for v in correspondence_variables_array[1]:
    #    print(v, v.solution_value())

    if  status != 0:
        #print("obj_value", -1)
        return -1, 0, 0, 0, 0, 0, 0, 0, 0, 0

    if status == 0:
    # Print solution
        #symm_prob.write(os.path.join(data_folder, "symm_prob.mps"))
        #symm_prob.write(os.path.join(data_folder, "symm_prob.sol"))
        #solution = symm_prob.getAttr('x', symm_stroke_variables)
        #for s_id, sol in enumerate(solution):
        #    print(s_id, sol)
        solution_terms = {
        #    "corr_term": corr_term.getValue(),
        #    "curve_corr_term": curve_corr_term.getValue(),
        #    "ref_corr_term": ref_corr_term.getValue(),
        #    "line_coverage_term": line_coverage_term.getValue(),
        #    "intersection_term": intersection_term.getValue(),
        #    "tangential_intersection_term": tangential_intersection_term.getValue(),
        #    "eccentricity_term": eccentricity_term.getValue(),
        #    "cluster_proximity_term": cluster_proximity_term.getValue(),
        }
        best_obj = symm_prob.Objective().Value()
        #print('obj_value', obj_value)
        #solution = symm_prob.getAttr('x', per_stroke_plane_variables)
        #for corr_id, corr in enumerate(solution):
        #    #print(corr, solution[corr])
        #    if not np.isclose(solution[corr], 1.0):
        #        continue
        #    print("per_stroke_plane_variables", corr)
        #solution = symm_prob.getAttr('x', non_self_symmetric_per_stroke_per_plane_variables)
        #for corr_id, corr in enumerate(solution):
        #    #print(corr, solution[corr])
        #    if not np.isclose(solution[corr], 1.0):
        #        continue
        #    print("non_self_symmetric_per_stroke_plane_variables", corr)
        #solution = symm_prob.getAttr('x', parallel_intersection_variables)
        #print("parallel_intersection_vars")
        #for corr_id, corr in enumerate(solution):
        #    #print(corr, solution[corr])
        #    if not np.isclose(solution[corr], 1.0):
        #        continue
        #    print(corr)

        final_end_intersections = []

        final_half_anchored_stroke_ids = []
        final_full_anchored_stroke_ids = []
        final_triple_intersections = []
        #if len(per_stroke_triple_intersections) > 0:
        #    #solution_terms["half_anchored_term"] = half_anchored_term.getValue()
        #    solution = symm_prob.getAttr('x', half_anchored_vars)
        #    #print("half_anchored_vars")
        #    for corr_id, corr in enumerate(solution):
        #        #print(corr, solution[corr])
        #        if not np.isclose(solution[corr], 1.0):
        #            continue
        #        #print(corr)
        #        final_half_anchored_stroke_ids.append(corr)
        #    solution = symm_prob.getAttr('x', full_anchored_vars)
        #    #print("full_anchored_vars")
        #    for corr_id, corr in enumerate(solution):
        #        #print(corr, solution[corr])
        #        if not np.isclose(solution[corr], 1.0):
        #            continue
        #        #print(corr)
        #        final_full_anchored_stroke_ids.append(corr)
        #    solution = symm_prob.getAttr('x', i_triple_vars)
        #    for corr_id, corr in enumerate(solution):
        #        #print(corr, solution[corr])
        #        if not np.isclose(solution[corr], 1.0):
        #            continue
        #        #print(corr)
        #        final_triple_intersections.append(corr[-1])

        if not return_final_strokes:
            return best_obj

        # get chosen correspondences and proxies
        stroke_results = np.zeros(nb_strokes, dtype=np.int)
        #print("results")
        variable_count = 0
        already_covered_correspondences = set()
        final_correspondences = []
        for corr_id, corr_var in enumerate(correspondence_variables_array[1]):
            #print(corr, solution[corr])
            if not np.isclose(corr_var.solution_value(), 1.0):
                continue
            corr = np.array(corr_var.name().split("(")[1].split(")")[0].split(","), dtype=int).tolist()
            variable_count += 1
            i_1 = corr[0]
            proxy_id = corr[1]
            k_1 = corr[2]
            i_2 = corr[3]
            k_2 = corr[4]
            plane_id = corr[5]

            #already_covered_correspondences.add((plane_id, i_1, k_1, i_2, k_2, proxy_id))
            first_inter_stroke_id = per_stroke_per_plane_correspondences[i_1][plane_id][k_1].first_inter_stroke_id
            snd_inter_stroke_id = per_stroke_per_plane_correspondences[i_1][plane_id][k_1].snd_inter_stroke_id
            final_correspondences.append([i_1, i_2,
                                          per_stroke_per_plane_correspondences[i_1][plane_id][k_1].stroke_3d,
                                          per_stroke_per_plane_correspondences[i_2][plane_id][k_2].stroke_3d,
                                          plane_id, first_inter_stroke_id, snd_inter_stroke_id, proxy_id,
                                          corr[-1]])
        final_proxies = [None]*nb_strokes
        final_intersections = []
        final_line_weights = [[-1, -1] for i in range(nb_strokes)]
        final_line_coverages = []
        # TODO: check for conflicting results
        #solution = symm_prob.getAttr('x', proxy_variables)
        #for s_id, c_expr in enumerate(curve_symmetry):
        #    if c_expr.size() > 0:
        #        #print(s_id, c_expr)
        #        #print(s_id, c_expr.getValue())
        for corr_var in proxy_variables_array[1]:
            if not np.isclose(corr_var.solution_value(), 1.0):
                continue
            corr = np.array(corr_var.name().split("(")[1].split(")")[0].split(","), dtype=int).tolist()
            #print(corr)
            variable_count += 1
            #print(corr)
            final_proxies[corr[0]] = proxy_strokes[corr[0]][corr[1]]

        if sketch_connectivity:
            #print("intersection_variables")
            for corr_var in intersection_variables_array[1]:
                if not np.isclose(corr_var.solution_value(), 1.0):
                    continue
                corr = np.array(corr_var.name().split("(")[1].split(")")[0].split(","), dtype=int).tolist()
                #print(corr)
                variable_count += 1
                #s_0, s_1, p_0, p_1 = corr
                #for inter in intersections_3d:
                #    if (inter.stroke_ids[0] == s_0 and inter.stroke_ids[1] == s_1 and inter.proxy_ids[0] == p_0 and inter.proxy_ids[1] == p_1) or \
                #            (inter.stroke_ids[1] == s_0 and inter.stroke_ids[0] == s_1 and inter.proxy_ids[1] == p_0 and inter.proxy_ids[0] == p_1):
                final_intersections.append(corr)
                #print(final_intersections)

        if line_coverage:
            for s_id in range(len(structured_line_coverage_variables)):
                if len(fixed_strokes[s_id]) > 0:
                    continue
                #solution = symm_prob.getAttr('x', structured_line_coverage_variables[s_id][0])
                for corr_var in structured_line_coverage_variables[s_id][0][1]:
                    if not np.isclose(corr_var.solution_value(), 1.0):
                        continue
                    variable_count += 1
                    #print(corr)
                    corr = np.array(corr_var.name().split("(")[1].split(")")[0].split(","), dtype=int).tolist()
                    s_i, j = corr
                    final_line_weights[s_i][0] = line_coverages[s_i][j].weight
                for corr_var in structured_line_coverage_variables[s_id][1][1]:
                    if not np.isclose(corr_var.solution_value(), 1.0):
                        continue
                    variable_count += 1
                    #print(corr)
                    corr = np.array(corr_var.name().split("(")[1].split(")")[0].split(","), dtype=int).tolist()
                    s_i, j = corr
                    final_line_weights[s_i][1] = line_coverages[s_i][j].weight

            final_line_coverages = [lc[1] - lc[0] if len(lc) == 2 else 0 for lc in final_line_weights]
            #for s_i, final_line_coverage in enumerate(final_line_coverages):
            #    print(s_i, final_line_coverage)
        return best_obj, final_correspondences, final_proxies, final_intersections, \
               final_line_coverages, final_end_intersections, solution_terms, final_full_anchored_stroke_ids, final_half_anchored_stroke_ids, final_triple_intersections
    else:
        #print("Non-optimal", symm_prob.Status)
        return -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
