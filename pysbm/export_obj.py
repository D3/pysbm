import enum
import pickle, json, os, inspect, sys
from math import acos
import matplotlib.pyplot as plt
from shapely.geometry import MultiPoint
from copy import deepcopy
from skspatial.objects import Plane
import numpy as np
import argparse
import pysbm.tools_3d as tools_3d
from pysbm.symmetry_tools.common_tools import extract_fixed_strokes
from pysbm.sketching import load_sketch, scale_center_sketch, copy_scaling
import seaborn as sns

def remove_sharp_turns(original_sketch):

    # detect sharp turns
    for s_id, s in enumerate(original_sketch.strokes):
        removed_point = True
        while removed_point:
            removed_point = False
            points = np.array([p.coords for p in original_sketch.strokes[s_id].points_list])
            sharp_points = np.zeros(len(points), dtype=bool)
            for p_id in range(len(s.points_list)-2):

                seg_one = points[p_id:p_id+2]
                seg_two = points[p_id+1:p_id+3]
                v1 = seg_one[1] - seg_one[0]
                if np.isclose(np.linalg.norm(v1), 0.0):
                    continue
                v1 /= np.linalg.norm(v1)
                v2 = seg_two[1] - seg_two[0]
                if np.isclose(np.linalg.norm(v2), 0.0):
                    continue
                v2 /= np.linalg.norm(v2)
                angle = np.rad2deg(acos(np.maximum(-1.0, np.minimum(1.0, np.dot(v1, v2)))))
                if np.abs(angle) > 90:
                    sharp_points[p_id+1] = True
            for p_id in range(len(points)-1):
                if sharp_points[p_id] and sharp_points[p_id+1]:
                    sharp_points[p_id] = False
            for p_id in reversed(range(len(points))):
                if sharp_points[p_id]:
                    del original_sketch.strokes[s_id].points_list[p_id]
                    removed_point = True

def get_symm_strokes_v2(sketch, sketch_file_name, cam, batches_results,
    clean_strokes=True):
    fixed_strokes = extract_fixed_strokes(batches_results)

    original_sketch = load_sketch(sketch_file_name)
    if not np.isclose(sketch.scale, 1.0):
        copy_scaling(original_sketch, sketch.scaling_information)
    #covered_s_ids = set()
    #for s_id, s in enumerate(sketch.strokes):
    #    for orig_s_id in sketch.strokes[s_id].original_id:
    #        original_sketch.strokes[orig_s_id].points_list = deepcopy(s.original_points)
    #        #print("orig_s_id", orig_s_id)
    #        covered_s_ids.add(orig_s_id)
    #for s_id in np.flip(list(range(len(original_sketch.strokes)))):
    #    if s_id in covered_s_ids:
    #        continue
    #    del original_sketch.strokes[s_id]

    if clean_strokes:
        remove_sharp_turns(original_sketch)
    new_fixed_strokes = [[] for i in original_sketch.strokes]
    new_pressures = [[] for i in original_sketch.strokes]
    original_coords = [[] for i in original_sketch.strokes]
    for s_id, s in enumerate(fixed_strokes):
        for orig_s_id in sketch.strokes[s_id].original_id:
            if len(s) == 0:
                new_fixed_strokes[orig_s_id] = []
                new_pressures[orig_s_id] = []
                original_coords[orig_s_id] = []
                continue
            original_s = original_sketch.strokes[orig_s_id]
            original_pts = np.array([p.coords for p in original_s.points_list])
            original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])
            lifted_points = []
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                if not np.isclose(np.linalg.norm(line_v), 0.0):
                    line_v /= np.linalg.norm(line_v)
                    lifted_points = np.array([cam.lift_point_close_to_line(
                        p, line_p, line_v) for p in original_pts])
            else:
                if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                    plane = Plane.best_fit(np.array(s))
                    lifted_points = cam.lift_polyline_to_plane(original_pts, s[0], plane.normal)
                else:
                    lifted_points = np.array([cam.lift_point_close_to_polyline(
                        p, np.array(s)) for p in original_pts])
            new_fixed_strokes[orig_s_id] = lifted_points
            new_pressures[orig_s_id] = original_pressures
            original_coords[orig_s_id] = [p.coords for p in original_sketch.strokes[orig_s_id].original_coords]

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes, new_pressures, original_coords

def get_symm_strokes(file_name, pickle_folder):
    with open(file_name, "rb") as fp:
        batches_results = json.load(fp)
    fixed_strokes = batches_results[-1]["fixed_strokes"]
    proxies = batches_results[-1]["final_proxies"]
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0:
            fixed_strokes[p_id] = p
    if "non_symmetric" in file_name:
        fixed_strokes = batches_results[-1]["with_non_symmetric_strokes"]

    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        cam = pickle.load(fp)

    sketch_file_name = os.path.join(data_folder, "sketch.json")
    #print(sketch_file_name)
    original_sketch, _, _ = sketch_wires.read_data_wires(sketch_file_name, include_dict_points=True)
    remove_sharp_turns(original_sketch)
    #print(len(original_sketch.strokes))
    new_fixed_strokes = [[] for i in original_sketch.strokes]
    new_pressures = [[] for i in original_sketch.strokes]
    display_strokes = []
    for s_id, s in enumerate(fixed_strokes):
        for orig_s_id in sketch.strokes[s_id].original_id:
            if len(s) == 0:
                new_fixed_strokes[orig_s_id] = []
                new_pressures[orig_s_id] = []
                continue
            display_strokes.append(orig_s_id)
            original_s = original_sketch.strokes[orig_s_id]
            original_pts = np.array([p.coords for p in original_s.points_list])
            original_pressures = np.array([p.get_data("pressure") for p in original_s.points_list])
            lifted_points = []
            if sketch.strokes[s_id].axis_label != 5:
                line_p = np.array(s[0])
                line_v = np.array(s[-1]) - np.array(s[0])
                if not np.isclose(np.linalg.norm(line_v), 0.0):
                    line_v /= np.linalg.norm(line_v)
                    lifted_points = np.array([cam.lift_point_close_to_line(
                        p, line_p, line_v) for p in original_pts])
            else:
                if sketch.strokes[s_id].axis_label == 5 and sketch.strokes[s_id].is_ellipse():
                    plane = Plane.best_fit(np.array(s))
                    lifted_points = cam.lift_polyline_to_plane(original_pts, s[0], plane.normal)
                else:
                    lifted_points = np.array([cam.lift_point_close_to_polyline(
                        p, np.array(s)) for p in original_pts])
            new_fixed_strokes[orig_s_id] = lifted_points
            new_pressures[orig_s_id] = original_pressures

    fixed_strokes = deepcopy(new_fixed_strokes)
    return fixed_strokes, new_pressures

def export_mesh(vertices, faces, obj_file_name):
    obj_file_txt = ""
    for p in vertices:
        obj_file_txt += "v "+str(p[0])+" "+str(p[1])+" "+str(p[2])+"\n"
    for f in faces:
        obj_file_txt += "f "+str(f[0])+" "+str(f[1])+" "+str(f[2])+"\n"
    with open(obj_file_name, "w") as fp:
        fp.write(obj_file_txt)

def export_strokes(strokes, obj_file_name):
    obj_file_txt = ""
    p_counter = 0
    for s_id, s in enumerate(strokes):
        if len(s) == 0:
            continue
        p_counter += 1
        for p_id, p in enumerate(s):
            obj_file_txt += "v "+str(p[0])+" "+str(p[1])+" "+str(p[2])+"\n"
        for p_id, p in enumerate(s[:-1]):
            obj_file_txt += "l "+str(p_counter)+" "+str(p_counter+1)+"\n"
            p_counter += 1
    with open(obj_file_name, "w") as fp:
        fp.write(obj_file_txt)

#def export_blender_data(sketch, sketch_file_name, cam, batches_result):
#    blender_folder = os.path.join(sketch.sketch_folder, "blender")
#    if not os.path.exists(blender_folder):
#        os.mkdir(blender_folder)
#    fixed_strokes, pressures = get_symm_strokes_v2(sketch, 
#        os.path.join(sketch.sketch_folder, sketch_file_name), cam, batches_result)
#    
#    # data for camera calibration
#    intersections_3d = []
#    for batch in batches_result:
#        for inter in batch["intersections"]:
#            intersections_3d.append(inter[0])
#    bbox = np.array(tools_3d.bbox_from_points(intersections_3d))
#    bbox_center = 0.5*(bbox[:3]+bbox[3:])
#    np.save(os.path.join(blender_folder, "bbox_center"), bbox_center)
#    mean_pressures = np.zeros(len(pressures))
#    for press_id, press in enumerate(pressures):
#        if len(press) > 0:
#            mean_pressures[press_id] = np.mean(press)
#    np.save(os.path.join(blender_folder, "pressures"), mean_pressures)
#    points_2d = [p for s in sketch.strokes[:10] for p in np.array(s.linestring.linestring)]
#
#    np.save(os.path.join(blender_folder, "points_2d"), np.array(points_2d))
#    points_3d = np.array([camera.lift_point(p, 1.0) for p in points_2d])
#    cam_pos = np.array(camera.cam_pos)
#    for p_id in range(len(points_3d)):
#        v = points_3d[p_id] - cam_pos
#        v /= np.linalg.norm(v)
#        points_3d[p_id] = v
#    np.save(os.path.join(blender_folder, "points_3d"), np.array(points_3d))
#
#    # reconstruction data
#    for s_id, s in enumerate(fixed_strokes):
#        if len(s) == 0:
#            continue
#        export_strokes([s], os.path.join(blender_folder, "stroke_"+str(s_id)+".obj"))
#    batches_plane_points, plane_faces = get_planes_3d(batches_result)
#    for j, plane_points in enumerate(batches_plane_points):
#        if len(plane_points) == 0:
#            continue
#        for i in range(3):
#            tmp_name = "batch_"+str(j)+"_plane_"+["x", "y", "z"][i]+".obj"
#        export_mesh(plane_points, plane_faces, os.path.join(blender_folder, tmp_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sketch_id", default=1, type=int, help="sketch_id")
    parser.add_argument("--batches_file", default="", type=str, help="sketch_id")
    args = parser.parse_args()
    sketch_id = args.sketch_id
    batches_file = args.batches_file
    batches_file = "batches_results_bootstrapped.json"
    batches_file = "batches_results_non_symmetric.json"
    batches_file = "batches_results_normal.json"
    pickle_folder = os.path.join("../data", str(sketch_id), "pickle")
    data_folder = "/".join(pickle_folder.split("/")[:-1])
    if not os.path.exists(os.path.join(data_folder, batches_file)):
        batches_file = "batches_results_bootstrapped.json"
    if not os.path.exists(os.path.join(data_folder, batches_file)):
        batches_file = "batches_results_post.json",
    if not os.path.exists(os.path.join(data_folder, batches_file)):
        batches_file = "batches_results_normal.json"

    #dist = np.load(os.path.join(data_folder, "ablations", batches_file.replace(".json", ".npy")))[0]
    #print(batches_file)
    #print(dist)

    with open(os.path.join(pickle_folder, "pre_processed_sketch.pkl"), "rb") as fp:
        sketch = pickle.load(fp)
    with open(os.path.join(pickle_folder, "camera.pkl"), "rb") as fp:
        camera = pickle.load(fp)
    with open(os.path.join(data_folder, batches_file), "r") as fp:
        batches_ref = json.load(fp)
    fixed_strokes = batches_ref[-1]["fixed_strokes"]
    proxies = batches_ref[-1]["final_proxies"]
    #fixed_strokes.append([camera.cam_pos, np.array(camera.cam_pos)+np.array(camera.view_dir)])
    for p_id, p in enumerate(proxies):
        if p is not None and len(p) > 0 and len(fixed_strokes[p_id]) == 0:
            fixed_strokes[p_id] = p
    blender_folder = os.path.join(data_folder, "blender")
    if not os.path.exists(blender_folder):
        os.mkdir(blender_folder)
    # export non_reconstructed_strokes
    non_reconstructed_stroke_ids = [s_id for s_id, s in enumerate(fixed_strokes) if len(s) == 0]
    #print("non_reconstructed_stroke_ids")
    #print(non_reconstructed_stroke_ids)
    # lift them to 3D
    original_sketch, _, _ = sketch_wires.read_data_wires(os.path.join(data_folder, "sketch.json"), include_dict_points=True)
    remove_sharp_turns(original_sketch)
    for s_id in range(len(original_sketch.strokes)):
        if len(original_sketch.strokes[s_id].points_list) < 2:
            continue
        lifted_line = [camera.lift_point(p.coords, 1.0)
                       for p in original_sketch.strokes[s_id].points_list]
        export_strokes([lifted_line], os.path.join(blender_folder, "original_stroke_"+str(s_id)+".obj"))
    for s_id in non_reconstructed_stroke_ids:
        for orig_s_id in sketch.strokes[s_id].original_id:
            if len(original_sketch.strokes[orig_s_id].points_list) < 2:
                continue
            lifted_line = [camera.lift_point(p.coords, 1.0)
                           for p in original_sketch.strokes[orig_s_id].points_list]
            export_strokes([lifted_line], os.path.join(blender_folder, "non_reconstructed_stroke_"+str(orig_s_id)+".obj"))

    fixed_strokes, pressures = get_symm_strokes(os.path.join(data_folder, batches_file), pickle_folder)
    #for s in fixed_strokes:
    #    if len(s) > 0:
    #        proj_s = np.array(camera.project_polyline(s))
    #        plt.plot(proj_s[:, 0], proj_s[:, 1])
    #plt.gca().set_xlim(0, sketch.width)
    #plt.gca().set_ylim(sketch.height, 0)
    #plt.show()
    mean_pressures = np.zeros(len(pressures))
    for press_id, press in enumerate(pressures):
        if len(press) > 0:
            mean_pressures[press_id] = np.mean(press)
    np.save(os.path.join(blender_folder, "pressures"), mean_pressures)
    #all_point
    #MultiPoint([p for s in fixed_strokes for p in s])
    points_2d = [p for s in sketch.strokes[:10] for p in np.array(s.linestring.linestring)]
    sketch_points = MultiPoint(points_2d)

    np.save(os.path.join(blender_folder, "points_2d"), np.array(points_2d))
    points_3d = np.array([camera.lift_point(p, 1.0) for p in points_2d])
    cam_pos = np.array(camera.cam_pos)
    for p_id in range(len(points_3d)):
        v = points_3d[p_id] - cam_pos
        v /= np.linalg.norm(v)
        points_3d[p_id] = v
    np.save(os.path.join(blender_folder, "points_3d"), np.array(points_3d))

    #export_strokes(fixed_strokes, os.path.join(blender_folder, "sketch.obj"))
    for s_id, s in enumerate(fixed_strokes):
        if len(s) == 0:
            continue
        export_strokes([s], os.path.join(blender_folder, "stroke_"+str(s_id)+".obj"))
    #obj_file_txt = ""
    #p_counter = 0
    #for s_id, s in enumerate(fixed_strokes):
    #    if len(s) == 0:
    #        continue
    #    p_counter += 1
    #    #obj_file_txt += "o ["+str(s_id)+"]\n"
    #    for p_id, p in enumerate(s):
    #        obj_file_txt += "v "+str(p[0])+" "+str(p[1])+" "+str(p[2])+"\n"
    #    for p_id, p in enumerate(s[:-1]):
    #        obj_file_txt += "l "+str(p_counter)+" "+str(p_counter+1)+"\n"
    #        p_counter += 1
    #print(obj_file_txt)
    #with open(os.path.join(blender_folder, "result.obj"), "w") as fp:
    #    fp.write(obj_file_txt)

    #with open(os.path.join(data_folder, batches_file), "r") as fp:
    #    batches = json.load(fp)
    #fixed_strokes = []
    #fixed_strokes = batches[-1]["fixed_strokes"]
    #proxies = batches[-1]["final_proxies"]
    #for p_id, p in enumerate(proxies):
    #    if p is not None and len(p) > 0 and len(fixed_strokes[p_id]) == 0:
    #        fixed_strokes[p_id] = p
    intersections_3d = []
    #for batches_file in ["batches_results_normal.json", "batches_results_bootstrapped.json", "batches_results_post.json", "batches_results_non_symmetric.json"]:
    for batches_file in ["batches_results_normal.json"]:
        with open(os.path.join(data_folder, batches_file), "r") as fp:
            batches_ref = json.load(fp)
        for batch in batches_ref:
            for inter in batch["intersections"]:
                intersections_3d.append(inter[0])
    for batches_file in ["batches_results_bootstrapped.json"]:
        with open(os.path.join(data_folder, batches_file), "r") as fp:
            batches_ref = json.load(fp)
        for batch in batches_ref:
            for inter in batch["intersections"]:
                intersections_3d.append(inter[0])
    for batches_file in ["batches_results_normal.json"]:
        with open(os.path.join(data_folder, batches_file), "r") as fp:
            batches_ref = json.load(fp)
    bbox = np.array(tools_3d.bbox_from_points(intersections_3d))
    bbox_center = 0.5*(bbox[:3]+bbox[3:])
    np.save(os.path.join(blender_folder, "bbox_center"), bbox_center)
    exit()
    plane_faces = [[1, 2, 3], [1, 3, 4]]
    plane_counter = 0
    #for batches_file in ["batches_results_normal.json", "batches_results_bootstrapped.json", "batches_results_post.json", "batches_results_non_symmetric.json"]:
    for batches_file in ["batches_results_normal.json"]:
        with open(os.path.join(data_folder, batches_file), "r") as fp:
            batches_ref = json.load(fp)
        for batch in batches_ref:
            for symm_plane in batch["symmetry_planes"]:
                plane_n = np.array(symm_plane["plane_normal"])
                plane_p = np.zeros(3)
                plane_p[np.argmax(np.abs(plane_n))] = -symm_plane["signed_distance"]
                #print(plane_p)
                if np.argmax(np.abs(plane_n)) == 0:
                    plane_points = [[plane_p[0], bbox[1], bbox[2]],
                                    [plane_p[0], bbox[4], bbox[2]],
                                    [plane_p[0], bbox[4], bbox[5]],
                                    [plane_p[0], bbox[1], bbox[5]]]
                elif np.argmax(np.abs(plane_n)) == 1:
                    plane_points = [[bbox[0], plane_p[1], bbox[2]],
                                    [bbox[3], plane_p[1], bbox[2]],
                                    [bbox[3], plane_p[1], bbox[5]],
                                    [bbox[0], plane_p[1], bbox[5]]]
                else:
                    plane_points = [[bbox[0], bbox[1], plane_p[2]],
                                    [bbox[3], bbox[1], plane_p[2]],
                                    [bbox[3], bbox[4], plane_p[2]],
                                    [bbox[0], bbox[4], plane_p[2]]]
                tmp_name = "plane_x_"+str(plane_counter)+".obj"
                if np.argmax(np.abs(plane_n)) == 1:
                    tmp_name = "plane_y_"+str(plane_counter)+".obj"
                elif np.argmax(np.abs(plane_n)) == 2:
                    tmp_name = "plane_z_"+str(plane_counter)+".obj"
                export_mesh(plane_points, plane_faces, os.path.join(blender_folder, tmp_name))
                plane_counter += 1
