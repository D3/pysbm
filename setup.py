from setuptools import setup

setup(
   name='pysbm',
   version='0.01',
   description='Sketch-based modeling library',
   author='Felix Hahnlein',
   author_email='felix.hahnlein@inria.fr',
   packages=['pysbm'],
   include_package_data=True,
   package_data={'': ["symmetry_tools/*", "optimization/*", "data/blender/*"]},
)
