import os, json
from time import time
import bmesh
import numpy as np
from mathutils import Matrix
from bpy_extras.object_utils import world_to_camera_view
from mathutils import Vector

reconstruct_script = """
import os, sys, json
import pysbm
import numpy as np
from pylowstroke.sketch_vanishing_points import assign_line_directions_given_vps
from pysbm.camera import Camera

def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y

def load_blender_camera(sketch_folder):

    # load camera
    cam_file_name = os.path.join(sketch_folder, "blender_cam_data.json")
    with open(cam_file_name, "r") as fp:
        file_content = json.load(fp)
    #print(file_content)
    cam = Camera(proj_mat=file_content["P"],
                 focal_dist=file_content["f"],
                 fov=file_content["fov"],
                 t=file_content["t"],
                 view_dir=file_content["view_dir"],
                 principal_point=file_content["principal_point"],
                 rot_mat=file_content["R"],
                 K=file_content["K"],
                 cam_pos=file_content["C"])
    vps = []
    for axis_i in range(3):
        line_1_3d = np.zeros([2, 3])
        line_1_3d[1][axis_i] += 1
        line_2_3d = np.zeros([2, 3])
        line_2_3d[1][axis_i] += 1
        line_2_3d[:, (axis_i+1)%3] += 1
        #print(line_1_3d)
        #print(line_2_3d)
        line_1_2d = np.array(cam.project_polyline(line_1_3d))
        line_2_2d = np.array(cam.project_polyline(line_2_3d))
        vp = np.array(line_intersection(line_1_2d, line_2_2d))
        vps.append(vp)
        #print(vp)
        #plt.plot(line_1_2d[:,0], line_1_2d[:,1])
        #plt.plot(line_2_2d[:,0], line_2_2d[:,1])
        #plt.scatter([vp[0]], [vp[1]])
        #plt.show()
    cam.vanishing_points_coords = vps
    cam.compute_inverse_matrices()
    return cam

import pysbm.verbose_decorator as verbose_decorator
#verbose_decorator.read_verbose_functions()

sys.setrecursionlimit(20000)

# PREPARE SKETCH
sketch_file_name = "view1_concept.json"
sketch = pysbm.sketching.load_sketch(os.path.join(SKETCH_FOLDER, sketch_file_name))

print("sketch folder:", sketch.sketch_folder)
pysbm.sketching.sketch_clean(sketch, with_scale=False)
#cam = pysbm.lifting.init_camera(sketch)
cam = load_blender_camera(sketch.sketch_folder)
assign_line_directions_given_vps(sketch, cam.vanishing_points_coords)
pysbm.sketching.preprocessing(sketch, cam)
#
### Visualizations
pysbm.visualization.sketch_plot(sketch)
pysbm.visualization.sketch_plot_successive(sketch, os.path.join(sketch.sketch_folder, "individual_lines"))
pysbm.visualization.plot_intersections(sketch)
pysbm.visualization.plot_acc_radius(sketch)
pysbm.visualization.plot_stroke_type(sketch)
pysbm.visualization.vanishing_points(sketch, cam)

#3D
## Selection of geometric constraints by optimization candidate symmetry correspondences
symm_candidates, corr_scores = pysbm.lifting.compute_symmetry_candidates(sketch, cam, include_more_curves=True)
batches = pysbm.lifting.compute_batches(sketch, symm_candidates)
batches_result, batches_result_1 = pysbm.lifting.optimize_symmetry_sketch_pipeline(
    sketch, cam, symm_candidates, batches, corr_scores, solver="ortools")

for batch_id in range(len(batches_result)):
    pysbm.visualization.visualize_correspondences(sketch, cam, batches_result,
        selected_batches=[batch_id], plot_planes=True)

with open(os.path.join(sketch.sketch_folder, "batches_result.json"), "r") as fp:
    batches_result = json.load(fp)
with open(os.path.join(sketch.sketch_folder, "batches_result_1.json"), "r") as fp:
    batches_result_1 = json.load(fp)

if len(batches_result_1) == 0:
    batches_result_1 = batches_result
#pysbm.visualization.visualize_polyscope(batches_result_1, cam)
pysbm.visualization.export_reconstruction_depth(sketch, sketch_file_name, cam, batches_result_1)
"""

def sketch_mesh_to_tubes(sketch_ob, mat=None, size=0.002):
    # Transform the sketch mesh into a curve object, extrude with the right thickness and set material
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = sketch_ob
    sketch_ob.select_set(True)
    bpy.ops.object.convert(target='CURVE')

    ob = bpy.context.view_layer.objects.active
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.object.data.bevel_depth = size

    # Default to a black flat material
    #if mat is None:
    #    mat = new_emissive_mat("stroke_mat", (0, 0, 0, 1))

    ob.active_material = mat

    bpy.ops.object.select_all(action='DESELECT')

axis_labels = {"Green": 0, "Red": 1, "Blue": 2}

MATERIAL_FILE_PATH = os.path.join("/Users/fhahnlei/Pictures/symmetric_sketch/materials.blend")
def load_mat(mat_name):
    path = MATERIAL_FILE_PATH + "\\Material\\"
    bpy.ops.wm.append(filename=mat_name, directory=path)
    mat = bpy.data.materials.get(mat_name)
    return mat

def load_sketch_mesh(sketch_name, sketch_obj_file, thickness=0.001, pressure=None, color=None):

    # Load sketch file
    bpy.ops.import_scene.obj(filepath=sketch_obj_file, split_mode='OFF', axis_forward="Y", axis_up="Z")
    ob = bpy.context.selected_objects[0]
    ob.name = f"{sketch_name}"
    # Apply your transform to correct axis, eg:
    #ob.rotation_euler[0] = numpy.deg2rad(90) # Correct axis orientations
    #bpy.ops.object.transform_apply() # Apply all transforms

    mat = load_mat("stroke-black")
    mat = mat.copy()
    tree = mat.node_tree
    print(tree.nodes)
    if not pressure is None:
        tree.nodes["Mix Shader"].inputs[0].default_value = pressure
    if not color is None:
        tree.nodes["Principled BSDF"].inputs[0].default_value = (color[0], color[1], color[2], 1)
    #bpy.data.materials["stroke-black"].node_tree.nodes["Principled BSDF"].inputs[0].default_value = (1, 0, 0, 1)

    sketch_mesh_to_tubes(ob, mat, size=thickness)
    #sketch_mesh_to_gpencil(ob, mat, size=thickness)
    #sketch_mesh_to_tubes(ob, None, size=thickness)

    ob.active_material = mat

    return ob

def align_vectors(a, b, weights=None, return_sensitivity=False):

    a = np.asarray(a)
    if a.ndim != 2 or a.shape[-1] != 3:
        raise ValueError("Expected in `a` to have shape (N, 3), "
                         "got {}".format(a.shape))
    b = np.asarray(b)
    if b.ndim != 2 or b.shape[-1] != 3:
        raise ValueError("Expected input `b` to have shape (N, 3), "
                         "got {}.".format(b.shape))

    if a.shape != b.shape:
        raise ValueError("Expected inputs `a` and `b` to have same shapes"
                         ", got {} and {} respectively.".format(
                            a.shape, b.shape))

    if weights is None:
        weights = np.ones(len(b))
    else:
        weights = np.asarray(weights)
        if weights.ndim != 1:
            raise ValueError("Expected `weights` to be 1 dimensional, got "
                             "shape {}.".format(weights.shape))
        if weights.shape[0] != b.shape[0]:
            raise ValueError("Expected `weights` to have number of values "
                             "equal to number of input vectors, got "
                             "{} values and {} vectors.".format(
                                weights.shape[0], b.shape[0]))
        if (weights < 0).any():
            raise ValueError("`weights` may not contain negative values")

    B = np.einsum('ji,jk->ik', weights[:, None] * a, b)
    u, s, vh = np.linalg.svd(B)

    # Correct improper rotation if necessary (as in Kabsch algorithm)
    if np.linalg.det(u @ vh) < 0:
        s[-1] = -s[-1]
        u[:, -1] = -u[:, -1]

    C = np.dot(u, vh)

    if s[1] + s[2] < 1e-16 * s[0]:
        print("Optimal rotation is not uniquely or poorly defined "
                      "for the given sets of vectors.")

    rmsd = np.sqrt(max(
        np.sum(weights * np.sum(b ** 2 + a ** 2, axis=1)) - 2 * np.sum(s),
        0))

    if return_sensitivity:
        zeta = (s[0] + s[1]) * (s[1] + s[2]) * (s[2] + s[0])
        kappa = s[0] * s[1] + s[1] * s[2] + s[2] * s[0]
        with np.errstate(divide='ignore', invalid='ignore'):
            sensitivity = np.mean(weights) / zeta * (
                    kappa * np.eye(3) + np.dot(B, B.T))
        return C, rmsd, sensitivity
    else:
        #print(C)
        return C, rmsd

def compute_inverse_matrices(r_t, k):

    r_t_inv = np.ones(shape=(4, 4))
    r_t_inv[:3, :] = r_t
    r_t_inv[3, :3] = 0.0
    r_t_inv = np.linalg.inv(r_t_inv)
    k_inv = np.linalg.inv(k)
    return r_t_inv, k_inv

def lift_point(p, lambda_val, k_inv, r_t_inv):
    u, v = p[0], p[1]

    p_cam = np.dot(k_inv, np.array([[u], [v], [1.0]]))
    p_cam *= lambda_val
    p_cam = np.expand_dims(p_cam, 0)

    p_world = np.ones(shape=(4, 1))
    p_world[:3] = p_cam
    p_world = np.dot(r_t_inv, p_world)
    p_world[:] /= p_world[3]
    p_world = p_world[:3]
    p_world = np.transpose(p_world)
    return p_world[0]

def project_point(p, proj_mat):
    hom_p = np.ones(4)
    hom_p[:3] = p
    proj_p = np.dot(proj_mat, hom_p)
    return proj_p[:2] / proj_p[2]

def project_by_object_utils(cam, point):
    scene = bpy.context.scene
    co_2d = world_to_camera_view(scene, cam, point)
    render_scale = scene.render.resolution_percentage / 100
    render_size = (
            int(scene.render.resolution_x * render_scale),
            int(scene.render.resolution_y * render_scale),
            )
    return Vector((co_2d.x * render_size[0], render_size[1] - co_2d.y * render_size[1]))
    #return Vector((co_2d.x * render_size[0], co_2d.y * render_size[1]))
    #return Vector((co_2d.x * render_size[0], co_2d.y * render_size[1]))
    #return Vector((co_2d.x, co_2d.y))

# Build intrinsic camera parameters from Blender camera data
#
# See notes on this in
# blender.stackexchange.com/questions/15102/what-is-blenders-camera-projection-matrix-model
def get_calibration_matrix_K_from_blender(camd):
    f_in_mm = camd.lens
    scene = bpy.context.scene
    resolution_x_in_px = scene.render.resolution_x
    resolution_y_in_px = scene.render.resolution_y
    scale = scene.render.resolution_percentage / 100
    sensor_width_in_mm = camd.sensor_width
    sensor_height_in_mm = camd.sensor_height
    pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
    if (camd.sensor_fit == 'VERTICAL'):
        # the sensor height is fixed (sensor fit is horizontal),
        # the sensor width is effectively changed with the pixel aspect ratio
        s_u = resolution_x_in_px * scale / sensor_width_in_mm / pixel_aspect_ratio
        s_v = resolution_y_in_px * scale / sensor_height_in_mm
    else: # 'HORIZONTAL' and 'AUTO'
        # the sensor width is fixed (sensor fit is horizontal),
        # the sensor height is effectively changed with the pixel aspect ratio
        pixel_aspect_ratio = scene.render.pixel_aspect_x / scene.render.pixel_aspect_y
        s_u = resolution_x_in_px * scale / sensor_width_in_mm
        s_v = resolution_y_in_px * scale * pixel_aspect_ratio / sensor_height_in_mm


    # Parameters of intrinsic calibration matrix K
    alpha_u = f_in_mm * s_u
    alpha_v = f_in_mm * s_v
    u_0 = resolution_x_in_px * scale / 2
    v_0 = resolution_y_in_px * scale / 2
    skew = 0 # only use rectangular pixels

    K = Matrix(
        ((alpha_u, skew,    u_0),
        (    0  , alpha_v, v_0),
        (    0  , 0,        1 )))
    return K

def get_3x4_RT_matrix_from_blender(cam):
    # bcam stands for blender camera
    R_bcam2cv = Matrix(
        ((1, 0,  0),
         (0, -1, 0),
         (0, 0, -1)))

    # Transpose since the rotation is object rotation,
    # and we want coordinate rotation
    # R_world2bcam = cam.rotation_euler.to_matrix().transposed()
    # T_world2bcam = -1*R_world2bcam * location
    #
    # Use matrix_world instead to account for all constraints
    location, rotation = cam.matrix_world.decompose()[0:2]
    R_world2bcam = rotation.to_matrix().transposed()

    # Convert camera location to translation vector used in coordinate changes
    # T_world2bcam = -1*R_world2bcam*cam.location
    # Use location from matrix_world to account for constraints:
    T_world2bcam = -1*R_world2bcam @ location
    #print("T_world2bcam")
    #print(T_world2bcam)

    # Build the coordinate transform matrix from world to computer vision camera
    # NOTE: Use * instead of @ here for older versions of Blender
    # TODO: detect Blender version
    R_world2cv = R_bcam2cv@R_world2bcam
    T_world2cv = R_bcam2cv@T_world2bcam

    # put into 3x4 matrix
    RT = Matrix((
        R_world2cv[0][:] + (T_world2cv[0],),
        R_world2cv[1][:] + (T_world2cv[1],),
        R_world2cv[2][:] + (T_world2cv[2],)
         ))
    return RT

def get_3x4_RT_matrix_from_blender_experiment(cam):
    # bcam stands for blender camera
    R_bcam2cv = Matrix(
        ((1, 0,  0),
         (0, -1, 0),
         (0, 0, -1)))
    R_bcam2cv = Matrix(
        ((1, 0,  0),
         (0, 1, 0),
         (0, 0, 1)))

    # Transpose since the rotation is object rotation,
    # and we want coordinate rotation
    # R_world2bcam = cam.rotation_euler.to_matrix().transposed()
    # T_world2bcam = -1*R_world2bcam * location
    #
    # Use matrix_world instead to account for all constraints
    location, rotation = cam.matrix_world.decompose()[0:2]
    #R_world2bcam = rotation.to_matrix().transposed()
    R_world2bcam = rotation.to_matrix()

    # Convert camera location to translation vector used in coordinate changes
    # T_world2bcam = -1*R_world2bcam*cam.location
    # Use location from matrix_world to account for constraints:
    T_world2bcam = -1*R_world2bcam @ location
    R_world2bcam = rotation.to_matrix().transposed()
    #print("T_world2bcam")
    #print(T_world2bcam)

    # Build the coordinate transform matrix from world to computer vision camera
    # NOTE: Use * instead of @ here for older versions of Blender
    # TODO: detect Blender version
    R_world2cv = R_bcam2cv@R_world2bcam
    T_world2cv = R_bcam2cv@T_world2bcam

    # put into 3x4 matrix
    RT = Matrix((
        R_world2cv[0][:] + (T_world2cv[0],),
        R_world2cv[1][:] + (T_world2cv[1],),
        R_world2cv[2][:] + (T_world2cv[2],)
         ))
    return RT

def get_3x4_P_matrix_from_blender(cam):
    K = get_calibration_matrix_K_from_blender(cam.data)
    RT = get_3x4_RT_matrix_from_blender(cam)
    return K@RT, K, RT

def setup_scene(envmap_path=None):

    bpy.context.scene.render.film_transparent = True

    # Environment lighting
    if envmap_path is not None:
        bpy.context.scene.world.use_nodes = True
        node_tex = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeTexEnvironment")
        node_tex.image = bpy.data.images.load(envmap_path)
        node_tree = bpy.context.scene.world.node_tree
        # Tweak saturation
        node_hsv = bpy.context.scene.world.node_tree.nodes.new("ShaderNodeHueSaturation")
        node_hsv.inputs[1].default_value = 0.2 # Set saturation
        node_tree.links.new(node_tex.outputs['Color'], node_hsv.inputs['Color'])
        node_tree.links.new(node_hsv.outputs['Color'], node_tree.nodes['Background'].inputs['Color'])

    bpy.ops.object.select_all(action = 'SELECT')
    bpy.data.objects.get("Camera").select_set(False)
    #bpy.data.objects.get("Camera_2").select_set(False)
    bpy.ops.object.delete()

def calibrate_camera(cam_param, folder):
    fov = cam_param["fov"]
    cam = bpy.data.objects['Camera']
    cam.data.sensor_width = 1
    cam.data.sensor_height = 1
    cam.rotation_mode = "XYZ"
    #cam.location = [-0.82328509792654525, -0.58680546433041747, -0.749133031137398]
    cam.location = cam_param["C"]
    cam_pos = np.array(cam_param["C"])
    #cam.data.lens = 885.72437566790518
    P, K, RT = get_3x4_P_matrix_from_blender(cam)
    K = np.array(cam_param["K"])
    #print("K")
    #print(K)
    #K = numpy.array([[885.72437566790518,0,356.35539412325687],[0,885.72437566790518,-276.38582435039547],[0,0,1]])

    fx, fy = K[0,0], K[1,1]
    cx, cy = K[0,2], K[1,2]
    if fx > fy:
        bpy.context.scene.render.pixel_aspect_y = fx/fy
    elif fx < fy:
        bpy.context.scene.render.pixel_aspect_x = fy/fx
    width = 512 #fov 29.0416d
    height = 512

    cam.data.lens_unit = 'FOV'
    bpy.context.scene.render.resolution_x = width
    bpy.context.scene.render.resolution_y = height
    max_resolution = max(width, height)
    cam.data.angle = 2 * np.arctan(width / (2 * K[0, 0]))
    #cam.data.angle = 2 * numpy.arctan(471.034823785905 / (2 * K[0, 0]))
    #print(numpy.rad2deg(cam.data.angle))
    #shift_x: -0.166805, shift_y: -0.946853
    #cam.data.angle = numpy.deg2rad(35.1282)
    #print(numpy.rad2deg(cam.data.angle))
    cam.data.shift_x = -(cx - width / 2.0) / max_resolution
    cam.data.shift_y = (cy - height / 2.0) / max_resolution
    bpy.context.view_layer.update()

    # find correct rotation
    r_t_inv, k_inv = compute_inverse_matrices(RT, K)
    bpy.context.view_layer.update()

    points_2d = np.load(os.path.join(folder, "points_2d.npy"))
    lifted_points_3d = np.array([lift_point(p, 1.0, k_inv, r_t_inv) for p in points_2d])
    for p_id in range(len(lifted_points_3d)):
        v = lifted_points_3d[p_id] - cam_pos
        v /= np.linalg.norm(v)
        #lifted_points_3d[p_id] = cam_pos + v
        lifted_points_3d[p_id] = v
        #lifted_points_3d[p_id] /= numpy.linalg.norm(lifted_points_3d[p_id])
        #lifted_points_3d[p_id] = cam_pos + lifted_points_3d[p_id]/numpy.linalg.norm(lifted_points_3d[p_id])
    np.save(os.path.join(folder, "lifted_points_3d.npy"), lifted_points_3d)

    points_3d = np.load(os.path.join(folder, "points_3d.npy"))
    rot_mat, rmsd = align_vectors(points_3d, lifted_points_3d)
    #print(rot_mat)
    rot_mat = Matrix(rot_mat)
    #print(rot_mat.to_euler())
    rot_mat_euler = rot_mat.to_euler()
    cam.rotation_euler.rotate(rot_mat_euler)
    #print(cam.rotation_euler)
    bpy.context.view_layer.update()
    bpy.context.view_layer.update()
    P, K, RT = get_3x4_P_matrix_from_blender(cam)
    K = np.array(cam_param["K"])
    r_t_inv, k_inv = compute_inverse_matrices(RT, K)
    return k_inv, r_t_inv, P

bl_info = {
    "name": "Add-on Template",
    "description": "",
    "author": "p2or",
    "version": (0, 0, 3),
    "blender": (2, 80, 0),
    "location": "3D View > Tools",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Development"
}

import bpy

from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )
from bpy.types import (Panel,
                       Menu,
                       Operator,
                       PropertyGroup,
                       )


# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------

class MyProperties(PropertyGroup):

    my_bool: BoolProperty(
        name="Enable or Disable",
        description="A bool property",
        default = False
        )

    my_int: IntProperty(
        name = "Int Value",
        description="A integer property",
        default = 23,
        min = 10,
        max = 100
        )

    my_float: FloatProperty(
        name = "Float Value",
        description = "A float property",
        default = 23.7,
        min = 0.01,
        max = 30.0
        )

    my_float_vector: FloatVectorProperty(
        name = "Float Vector Value",
        description="Something",
        default=(0.0, 0.0, 0.0), 
        min= 0.0, # float
        max = 0.1
    ) 

    my_string: StringProperty(
        name="User Input",
        description=":",
        default="",
        maxlen=1024,
        )

    my_path: StringProperty(
        name = "Directory",
        description="Choose a directory:",
        default="/Users/fhahnlei/Documents/wires_python/data/1",
        maxlen=1024,
        subtype='DIR_PATH'
        )
        
    my_enum: EnumProperty(
        name="Dropdown:",
        description="Apply Data to attribute.",
        items=[ ('OP1', "Option 1", ""),
                ('OP2', "Option 2", ""),
                ('OP3', "Option 3", ""),
               ]
        )

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class WM_OT_ReconstructSketch(Operator):
    bl_label = "Reconstruct Sketch"
    bl_idname = "wm.reconstruct_sketch"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        folder = mytool.my_path
        new_script = reconstruct_script.replace("SKETCH_FOLDER", "\""+folder+"\"")
        with open("/tmp/test_script.py", "w") as fp:
            fp.write(new_script)
        print(new_script)

        #print("conda init bash;conda activate wires;")#; python -c "+new_script)
        print("conda run -n pysbm python /tmp/test_script.py")
        os.system("conda run -n pysbm python /tmp/test_script.py")#+new_script)
        #os.system("<VENV_PATH_TO_PYTHON>/bin/python3 python /tmp/test_script.py")#+new_script)
        # print the values to the console
        print("Hello World")
        print("bool state:", mytool.my_bool)
        print("int value:", mytool.my_int)
        print("float value:", mytool.my_float)
        print("string value:", mytool.my_string)
        print("enum state:", mytool.my_enum)

        return {'FINISHED'}


class WM_OT_SaveDrawing(Operator):
    bl_label = "Save Drawing"
    bl_idname = "wm.save_drawing"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        meshes = bpy.data.meshes
        objects = bpy.data.objects
        cam = bpy.data.objects['Camera']
        P, K, RT = get_3x4_P_matrix_from_blender(cam)
        sketch = {"canvas":
        {"height": scene.render.resolution_y,
        "width": scene.render.resolution_x,
        "pen_width": 1.5},
        "strokes": []}
        r_t_inv, k_inv = compute_inverse_matrices(RT, K)
        pt_3d = lift_point([100, 200], 1.0, k_inv, r_t_inv)
        #print(project_by_object_utils(cam, pt_3d))
        v = bpy.data.scenes["Scene"].cursor.location
        #print("cursor_location", project_by_object_utils(cam, v))
        #print(project_point(pt_3d, P))
        #print(cam.location)
        #print(-np.array(RT)[:,:3]@np.array(RT)[:,3])

        #print(context.selected_ids)
        if len(context.selected_ids) > 1 or context.selected_ids[0].bl_rna.identifier != "Object":
                return {'FINISHED'}
        item = context.selected_ids[0]
        #print(item.type)
        if item.type != "GPENCIL":
            return {'FINISHED'}
        pencil_data = item.data
        #2) To get the GP-layers, use layer name or index.
        #print(pencil_data.layers)
        layer = pencil_data.layers.active
        #3) To get the GP-Frames, use index. But there are other properties of the layer
        #   you can access e.g., opacity, thickness, select, etc. You can add or remove the frame.
        #print(layer.frames)
        frame = layer.frames[0]
        #4) To get the GP-strokes, use index. You can add or remove a stroke, change the
        #   display-mode, line-width, material index, etc. 
        #print(len(frame.strokes))
        strokes_2d = []
        time_stamp = 0.0
        #print(project_by_object_utils(cam, Vector((0,0,0))))
        #print(project_by_object_utils(cam, Vector((-1,0,0))))
        for s_id, s in enumerate(frame.strokes):
            name = bpy.context.object.material_slots[s.material_index].name
            if not name in axis_labels.keys():
                axis_label = 5 
            else:
                axis_label = axis_labels[name]

            points_2d = []
            for p_id, p in enumerate(s.points):
                #print(p)
                #print(p.co, p.pressure, p.strength)
                #p_2d = project_point(p.co, P)
                #print(item.matrix_world)
                #p_2d = project_by_object_utils(cam, p.co+item.location)
                p_2d = project_by_object_utils(cam, item.matrix_world@p.co)
                #print(item.matrix_world@p.co)
                #print(p_2d)
                points_2d.append({"x": p_2d[0], "y": p_2d[1], "p": p.pressure, "t": time_stamp+0.05})
                time_stamp += 0.05
            #points_2d = np.array(points_2d)
            strokes_2d.append({"points": points_2d, "is_removed": False})
        #print(strokes_2d)
        sketch["strokes"] = strokes_2d
        folder = mytool.my_path
        #with open(os.path.join(folder, "sketch.json"), "rb") as fp:
        #    sketch = json.load(fp)
        #sketch["strokes_topology"] = strokes_2d
        with open(os.path.join(folder, "view1_concept.json"), "w") as fp:
            json.dump(sketch, fp)

        # Save camera
        cam_file_name = os.path.join(folder, "blender_cam_data.json")
        width = sketch["canvas"]["width"]
        #cam.data.angle = 2 * np.arctan(width / (2 * K[0, 0]))
        cam_data = {}
        cam_data["f"] = width/(2*np.tan(0.5*cam.data.angle))
        cam_data["fov"] = np.degrees(cam.data.angle)
        cam_data["principal_point"] = [K[0][2], K[1][2]]
        cam_data["R"] = np.array(RT)[:,:3].tolist()
        cam_data["t"] = np.array(RT)[:,3].tolist()
        cam_data["view_dir"] = np.array(RT)[2,:3].tolist()
        cam_data["P"] = np.array(P).tolist()
        cam_data["K"] = np.array(K).tolist()
        cam_data["C"] = np.array(cam.location).tolist()
        with open(cam_file_name, "w") as fp:
            json.dump(cam_data, fp, indent=4)

        return {'FINISHED'}

class WM_OT_LoadSketchFolder(Operator):
    bl_label = "Load Sketch Folder"
    bl_idname = "wm.load_sketch_folder"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        cam = bpy.data.objects['Camera']
        #cam.data.sensor_width = 1
        #cam.data.sensor_height = 1
        folder = mytool.my_path
        #P, K, RT = get_3x4_P_matrix_from_blender(cam)
        #print(RT)
        #file_path = os.path.join(folder, "cam_data.json")
        #with open(file_path, "r") as fp:
        #    K = np.array(json.load(fp)["cam_param"]["K"])
        #print(K)
        #P = K@RT
        #r_t_inv, k_inv = compute_inverse_matrices(RT, K)
        bpy.context.view_layer.update()

        print("Sketch folder:", mytool.my_path)

        file_path = os.path.join(folder, "reconstruction_depth.json")
        # print the values to the console
        if not os.path.exists(file_path):
            print("Depth file ", file_path, " does not exist!")
            return {'FINISHED'}
        with open(file_path, "r") as fp:
            reconstruction_depths = json.load(fp)
        if len(context.selected_ids) > 1 or context.selected_ids[0].bl_rna.identifier != "Object":
                return {'FINISHED'}
        item = context.selected_ids[0]
        if item.type != "GPENCIL":
            return {'FINISHED'}
        new_item = item.copy()
        new_item.data = item.data.copy()
        context.collection.objects.link(new_item)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = new_item
        new_item.select_set(True)
        new_item.matrix_world.zero()
        #new_item.parent_clear(type="CLEAR")
        bpy.ops.object.parent_clear(type='CLEAR')
        #item = bpy.ops.object.duplicate_move()
        #print(item)
        pencil_data = new_item.data
        layer = pencil_data.layers.active
        frame = layer.frames[0]
        new_s_id = 0
        # get reference distance
        found_ref = False
        for s in reconstruction_depths:
            for p in s:
                d_new = p
                found_ref = True
                break
            if found_ref:
                break
        if not found_ref:
            print("ERROR: no reconstruction depth found")
            return {'FINISHED'}

        # get canvas distance
        d_canvas = np.linalg.norm(np.array(cam.location) - np.array(frame.strokes[0].points[0].co))

        gpencil_id = 0
        pysbm_id = 0
        points_a = []
        points_b = []
        print("lengths")
        print(len(reconstruction_depths))
        print(len(frame.strokes))
        while gpencil_id < len(frame.strokes):
        #for s_id, s in enumerate(frame.strokes):
            s = frame.strokes[gpencil_id]
            print(len(s.points), len(reconstruction_depths[pysbm_id]))
            if len(s.points) < 2:
                gpencil_id += 1
                for p_id, p in enumerate(s.points):
                    p.co = [0,0,0]
                continue
            #new_s_id += 1
            print(len(s.points), len(reconstruction_depths[pysbm_id]))
            if len(s.points) != len(reconstruction_depths[pysbm_id]):
                gpencil_id += 1
                pysbm_id += 1
                for p_id, p in enumerate(s.points):
                    p.co = [0,0,0]
                #new_s_id += 1
                continue

            if len(reconstruction_depths[pysbm_id]) == 0:
                gpencil_id += 1
                pysbm_id += 1
                for p_id, p in enumerate(s.points):
                    p.co = [0,0,0]
                continue

            print("stroke", gpencil_id)
            for p_id, p in enumerate(s.points):
                #print(p)
                #print(p.co, reconstruction_depths[s_id][p_id])
                #print(lift_point([p_2d[0], p_2d[1]], reconstruction_depths[s_id][p_id], k_inv, r_t_inv))
                #p.co = lift_point([p_2d[0], p_2d[1]], 10.0*reconstruction_depths[pysbm_id][p_id], k_inv, r_t_inv)
                #p_2d = project_point(p.co, P)
                #p_2d_a = project_by_object_utils(cam, p.co)
                #p_2d_a = project_point(p.co, P)
                #p_2d_a = lift_point([p_2d_a[0], p_2d_a[1]], 10, k_inv, r_t_inv)
                #p_2d = reconstruction_depths[pysbm_id][p_id][0]
                ##print(p_2d_a, p_2d)
                #p.co = lift_point([p_2d[0], p_2d[1]], 10*reconstruction_depths[pysbm_id][p_id][1], k_inv, r_t_inv)
                #p.co = lift_point([p_2d[0], p_2d[1]], reconstruction_depths[pysbm_id][p_id][1], k_inv, r_t_inv)
                p.co = reconstruction_depths[pysbm_id][p_id]

                ###p_2d_b = project_by_object_utils(cam, p.co)
                #p_2d_b = project_point(p.co, P)
                #p_2d_b = lift_point([p_2d_b[0], p_2d_b[1]], 10, k_inv, r_t_inv)
                ##p.co = p_2d_b

                #points_a.append(p_2d_a-np.array(cam.location))
                #points_b.append(p_2d_b-np.array(cam.location))

                ##p.co = lift_point([p_2d[0], p_2d[1]], 15.0, k_inv, r_t_inv)
                #cam_vec = np.array(p.co) - np.array(cam.location)
                #cam_vec /= np.linalg.norm(cam_vec)
                ##p.co = np.array(cam.location) + 10.0*reconstruction_depths[new_s_id][p_id]*cam_vec
                #d1 = reconstruction_depths[pysbm_id][p_id]
                ##p.co = np.array(cam.location) + cam_vec*d1*d_canvas/d_new
                #print(d1*d_canvas/d_new)
                #p.co = np.array(cam.location) + cam_vec*d1
            gpencil_id += 1
            pysbm_id += 1
            #new_s_id += 1
        new_item.scale[0] = 1
        new_item.scale[1] = 1
        new_item.scale[2] = 1
        new_item.rotation_euler[0] = 0
        new_item.rotation_euler[1] = 0
        new_item.rotation_euler[2] = 0
        return {'FINISHED'}
        # align 3d model and gpencil drawing
        rot_mat, _ = align_vectors(points_a, points_b)
        #print(rot_mat)
        rot_mat = Matrix(rot_mat)
        print(rot_mat.to_euler())
        rot_mat_euler = rot_mat.to_euler()
        cam.rotation_euler.rotate(rot_mat_euler)
        bpy.context.view_layer.update()
        bpy.context.view_layer.update()
        P, _, RT = get_3x4_P_matrix_from_blender(cam)
        #K = np.array(cam_param["K"])
        P = K@RT
        r_t_inv, k_inv = compute_inverse_matrices(RT, K)
        gpencil_id = 0
        pysbm_id = 0
        points_a = []
        points_b = []
        while gpencil_id < len(frame.strokes):
        #for s_id, s in enumerate(frame.strokes):
            s = frame.strokes[gpencil_id]
            if len(s.points) < 2:
                gpencil_id += 1
                continue
            #new_s_id += 1
            if len(s.points) != len(reconstruction_depths[pysbm_id]):
                gpencil_id += 1
                pysbm_id += 1
                #new_s_id += 1
                continue

            if len(reconstruction_depths[pysbm_id]) == 0:
                gpencil_id += 1
                pysbm_id += 1
                continue

            for p_id, p in enumerate(s.points):
                #print(p)
                #print(p.co, reconstruction_depths[s_id][p_id])
                #print(lift_point([p_2d[0], p_2d[1]], reconstruction_depths[s_id][p_id], k_inv, r_t_inv))
                #p.co = lift_point([p_2d[0], p_2d[1]], 10.0*reconstruction_depths[pysbm_id][p_id], k_inv, r_t_inv)
                p_2d = project_point(p.co, P)
                p_2d = project_by_object_utils(cam, p.co)
                #p_2d = reconstruction_depths[pysbm_id][p_id][0]
                p.co = lift_point([p_2d[0], p_2d[1]], 10*reconstruction_depths[pysbm_id][p_id][1], k_inv, r_t_inv)

                ##p.co = lift_point([p_2d[0], p_2d[1]], 15.0, k_inv, r_t_inv)
                #cam_vec = np.array(p.co) - np.array(cam.location)
                #cam_vec /= np.linalg.norm(cam_vec)
                ##p.co = np.array(cam.location) + 10.0*reconstruction_depths[new_s_id][p_id]*cam_vec
                #d1 = reconstruction_depths[pysbm_id][p_id]
                ##p.co = np.array(cam.location) + cam_vec*d1*d_canvas/d_new
                #print(d1*d_canvas/d_new)
                #p.co = np.array(cam.location) + cam_vec*d1
            gpencil_id += 1
            pysbm_id += 1
            #new_s_id += 1
        print(rot_mat.transposed().to_euler())
        rot_mat_euler = rot_mat.transposed().to_euler()
        cam.rotation_euler.rotate(rot_mat_euler)
        bpy.context.view_layer.update()
        bpy.context.view_layer.update()

        return {'FINISHED'}

# ------------------------------------------------------------------------
#    Menus
# ------------------------------------------------------------------------

class OBJECT_MT_CustomMenu(bpy.types.Menu):
    bl_label = "Select"
    bl_idname = "OBJECT_MT_custom_menu"

    def draw(self, context):
        layout = self.layout

        # Built-in operators
        layout.operator("object.select_all", text="Select/Deselect All").action = 'TOGGLE'
        layout.operator("object.select_all", text="Inverse").action = 'INVERT'
        layout.operator("object.select_random", text="Random")

# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class OBJECT_PT_CustomPanel(Panel):
    bl_label = "PySBM"
    bl_idname = "OBJECT_PT_custom_panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "Tools"
    bl_context = "objectmode"   


    @classmethod
    def poll(self,context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool

        #layout.prop(mytool, "my_bool")
        #layout.prop(mytool, "my_enum", text="") 
        #layout.prop(mytool, "my_int")
        #layout.prop(mytool, "my_float")
        #layout.prop(mytool, "my_float_vector", text="")
        #layout.prop(mytool, "my_string")
        layout.prop(mytool, "my_path")
        layout.operator("wm.save_drawing")
        layout.operator("wm.reconstruct_sketch")
        layout.operator("wm.load_sketch_folder")
        #layout.menu(OBJECT_MT_CustomMenu.bl_idname, text="Presets", icon="SCENE")
        layout.separator()

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    MyProperties,
    WM_OT_LoadSketchFolder,
    WM_OT_ReconstructSketch,
    WM_OT_SaveDrawing,
    OBJECT_MT_CustomMenu,
    OBJECT_PT_CustomPanel
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.my_tool = PointerProperty(type=MyProperties)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.my_tool


if __name__ == "__main__":
    register()
