import pysbm, os, sys

sys.setrecursionlimit(20000)

# PREPARE SKETCH
file_name = "../data/test_mark_vacuum/view1_concept.json"
file_name = "../data/test_jan_rounded_2_2/view1_concept.json"
sketch = pysbm.sketching.load_sketch(file_name)
pysbm.visualization.sketch_plot(sketch)

pysbm.sketching.sketch_clean(sketch)
pysbm.visualization.sketch_plot(sketch)

cam = pysbm.lifting.init_camera(sketch)
pysbm.sketching.preprocessing(sketch, cam)

## Visualizations
pysbm.visualization.sketch_plot(sketch)
pysbm.visualization.sketch_plot_successive(sketch, os.path.join(sketch.sketch_folder, "individual_lines"))
pysbm.visualization.plot_intersections(sketch)
pysbm.visualization.plot_acc_radius(sketch)
pysbm.visualization.plot_stroke_type(sketch)
pysbm.visualization.vanishing_points(sketch, cam)

#3D
## Selection of geometric constraints by optimization candidate symmetry correspondences
symm_candidates, corr_scores = pysbm.lifting.compute_symmetry_candidates(sketch, cam, include_more_curves=True)
pysbm.visualization.symm_candidates_ps(sketch, symm_candidates, cam)
pysbm.visualization.plot_candidate_correspondences(sketch, symm_candidates)
batches = pysbm.lifting.compute_batches(sketch, symm_candidates, VERBOSE=False)
print("batches", batches)
pysbm.visualization.visualize_batches(sketch, batches)
batches_result, batches_result_1 = pysbm.lifting.optimize_symmetry_sketch_pipeline(
    sketch, cam, symm_candidates, batches, corr_scores, solver="ortools")

## visualize result
for batch_id in range(len(batches_result)):
    pysbm.visualization.visualize_correspondences(sketch, cam, batches_result,
        selected_batches=[batch_id], plot_planes=True)
if len(batches_result_1) == 0:
    batches_result_1 = batches_result
pysbm.visualization.visualize_polyscope(batches_result_1, cam)
pysbm.visualization.export_blender_data(sketch, file_name, cam, batches_result, batches_result_1)