import os, sys, json
import pysbm
import numpy as np
from pylowstroke.sketch_vanishing_points import assign_line_directions_given_vps
from pysbm.camera import Camera
import matplotlib.pyplot as plt

def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y

def load_blender_camera(sketch_folder):

    # load camera
    cam_file_name = os.path.join(sketch_folder, "blender_cam_data.json")
    print(sketch_folder)
    with open(cam_file_name, "r") as fp:
        file_content = json.load(fp)
    #print(file_content)
    cam = Camera(proj_mat=file_content["P"],
                 focal_dist=file_content["f"],
                 fov=file_content["fov"],
                 t=file_content["t"],
                 view_dir=file_content["view_dir"],
                 principal_point=file_content["principal_point"],
                 rot_mat=file_content["R"],
                 K=file_content["K"],
                 cam_pos=file_content["C"])
    vps = []
    for axis_i in range(3):
        line_1_3d = np.zeros([2, 3])
        line_1_3d[1][axis_i] += 1
        line_2_3d = np.zeros([2, 3])
        line_2_3d[1][axis_i] += 1
        line_2_3d[:, (axis_i+1)%3] += 1
        line_1_2d = np.array(cam.project_polyline(line_1_3d))
        line_2_2d = np.array(cam.project_polyline(line_2_3d))
        vp = np.array(line_intersection(line_1_2d, line_2_2d))
        vps.append(vp)
        #plt.plot(line_1_2d[:,0], line_1_2d[:,1])
        #plt.plot(line_2_2d[:,0], line_2_2d[:,1])
        #plt.scatter([vp[0]], [vp[1]])
        #plt.show()
    #pts = np.array([p.coords for p in sketch.strokes[0].points_list])
    #plt.plot(pts[:, 0], pts[:, 1], c="green")
    #plt.gca().invert_yaxis()
    #plt.show()
    cam.vanishing_points_coords = vps
    cam.compute_inverse_matrices()
    return cam


sys.setrecursionlimit(20000)
# PREPARE SKETCH
file_name = "../data/test_jan_rounded_2_2/view1_concept.json"
sketch = pysbm.sketching.load_sketch(file_name)
pysbm.visualization.sketch_plot(sketch)

# you don't wanna scale it if the camera is already provided
pysbm.sketching.sketch_clean(sketch, with_scale=False)
pysbm.visualization.sketch_plot(sketch)

#cam = pysbm.lifting.init_camera(sketch)
cam = load_blender_camera(sketch.sketch_folder)
assign_line_directions_given_vps(sketch, cam.vanishing_points_coords)
pysbm.sketching.preprocessing(sketch, cam)

## Visualizations
pysbm.visualization.sketch_plot(sketch)
pysbm.visualization.sketch_plot_successive(sketch, os.path.join(sketch.sketch_folder, "individual_lines"))
pysbm.visualization.plot_intersections(sketch)
pysbm.visualization.plot_acc_radius(sketch)
pysbm.visualization.plot_stroke_type(sketch)
pysbm.visualization.vanishing_points(sketch, cam)

#3D
## Selection of geometric constraints by optimization candidate symmetry correspondences
symm_candidates, corr_scores = pysbm.lifting.compute_symmetry_candidates(sketch, cam, include_more_curves=True)
pysbm.visualization.symm_candidates_ps(sketch, symm_candidates, cam)
pysbm.visualization.plot_candidate_correspondences(sketch, symm_candidates)
batches = pysbm.lifting.compute_batches(sketch, symm_candidates, VERBOSE=False)
pysbm.visualization.visualize_batches(sketch, batches)
batches_result, batches_result_1 = pysbm.lifting.optimize_symmetry_sketch_pipeline(
    sketch, cam, symm_candidates, batches, corr_scores, solver="ortools")

## visualize result
for batch_id in range(len(batches_result)):
    pysbm.visualization.visualize_correspondences(sketch, cam, batches_result,
        selected_batches=[batch_id], plot_planes=True)
if len(batches_result_1) == 0:
    batches_result_1 = batches_result
pysbm.visualization.visualize_polyscope(batches_result_1, cam)
pysbm.visualization.export_blender_data(sketch, file_name, cam, batches_result, batches_result_1)