import pysbm

# PREPARE SKETCH
file_name = "../data/house/sketch.svg"
sketch = pysbm.sketching.load_sketch(file_name)
pysbm.visualization.sketch_plot(sketch)

pysbm.sketching.sketch_clean(sketch)
cam = pysbm.lifting.init_camera(sketch)
pysbm.sketching.preprocessing(sketch, cam)

## 3D RECONSTRUCTION
symm_candidates, corr_scores = pysbm.lifting.compute_symmetry_candidates(sketch, cam)
batches = pysbm.lifting.compute_batches(sketch, symm_candidates)
batches_result, batches_result_1 = pysbm.lifting.optimize_symmetry_sketch_pipeline(
    sketch, cam, symm_candidates, batches, corr_scores)

## SHOW/EXPORT RESULT
pysbm.visualization.visualize_polyscope(batches_result_1, cam)
pysbm.visualization.export_blender_data(sketch, file_name, cam, batches_result, batches_result_1)