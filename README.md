# pySBM

Documentation available [here](https://d3.gitlabpages.inria.fr/pysbm-docs/).

If there should be any issues, don't hesitate to contact me at fhahnlei@cs.washington.edu

## Dependencies

- [pylowstroke](https://gitlab.inria.fr/D3/pylowstroke)
- Optional: [Gurobi](https://www.gurobi.com/)

## Installation

The library has been tested with <code>python==3.8</code>.
The following code creates a new virtual environment and installs both <code>pylowstroke</code> and <code>pysbm</code>.

    conda create -n pysbm python=3.8
    conda activate pysbm

    git clone https://gitlab.inria.fr/D3/pylowstroke.git
    cd pylowstroke
    pip install -r requirements.txt
    pip install .
    cd ..

    git clone https://gitlab.inria.fr/D3/pysbm.git
    cd pysbm
    pip install -r requirements.txt
    pip install .

## Test the installation

Try out the quickstart example:

    cd test
    python quickstart.py

The result will be shown and stored in the folder <code>data/house</code> as a Blender file.
